package com.src.main.comm.request.external;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
public abstract class AbstractExternalRequest implements ExternalRequest {
	private final List<Integer> sequence;
	private UUID requestId;
	private ExternalRequestType type;

	protected AbstractExternalRequest(ExternalRequestType type) {
		this.type = type;
		sequence = new ArrayList<Integer>();
		sequence.add(type.id());
	}

	protected AbstractExternalRequest(Builder<?, ?> b) {
		this(b.type);
		requestId = b.requestId;
	}

	public static abstract class Builder<E extends ExternalRequest, T extends Builder<E, T>> {
		private UUID requestId;
		private ExternalRequestType type;

		protected Builder(ExternalRequestType type) {
			this.type = type;
		}

		public abstract E build();

		public abstract T self();

		public T requestId(UUID requestId) {
			this.requestId = requestId;
			return self();
		}
	}

	public static class Deserializer extends JsonDeserializer<ExternalRequest> {
		@Override
		public ExternalRequest deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			if (!"type".equals(field)) {
				throw new RuntimeException("The first field in serialization should be : type");
			}
			p.nextToken();
			final ExternalRequest request = GcmCcsRequestType.valueOf(p.readValueAs(int.class)).create();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				request.populateFromJsonString(field, p);
			}
			return request;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeObjectField("lineage", sequence);
		g.writeNumberField("type", type().id());
		g.writeObjectField("requestId", requestId);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("type".equals(field)) {
			p.nextToken();
			type = ExternalRequestType.valueOf(p.readValueAs(int.class));
		} else if ("requestId".equals(field)) {
			p.nextToken();
			requestId = p.readValueAs(UUID.class);
		}
	}

	@Override
	public void preProcess() throws Exception {

	}

	@Override
	public void postProcess() throws Exception {

	}

	@Override
	public List<Integer> lineageSequence() {
		return sequence;
	}

	@Override
	public ExternalRequestType type() {
		return type;
	}

	@Override
	public UUID requestId() {
		return requestId;
	}

	@Override
	public String toString() {
		return serializeAsJson().orElse(super.toString());
	}
}
