package com.src.main.comm.request.external;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Abhinav.Dwivedi
 *
 */
public enum BasicCollapseKey implements CollapseKey {
	SYNC_COLLAPSE_KEY(1, "SyncData");
	private final int id;
	private final String value;

	private BasicCollapseKey(int id, String value) {
		this.id = id;
		this.value = value;
	}

	private static final Map<Integer, CollapseKey> keys = new HashMap<Integer, CollapseKey>();

	static {
		for (BasicCollapseKey key : BasicCollapseKey.values()) {
			if (keys.get(key.id) == null) {
				keys.put(key.id, key);
			} else {
				// throw new BadPracticeException("Duplicate id: " + key.id);
			}
		}
	}

	public static CollapseKey valueOf(int id) {
		return keys.get(id);
	}

	@Override
	public int id() {
		return id;
	}

	@Override
	public String value() {
		return value;
	}
}
