package com.src.main.comm.request.external;

import com.src.main.tlabs.quizzify.db.communications.ExternalCommunicationLink;

/**
 * 
 * @author Abhinav.Dwivedi
 *
 */
public interface CcsClient extends Client {
	static CcsClient instance() {
		return SmackCcsClient.instance();
	}

	Object connect() throws Exception;

	void sendDownstreamMessage(ExternalCommunicationLink link) throws Exception;
}
