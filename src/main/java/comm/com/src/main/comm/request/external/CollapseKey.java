package com.src.main.comm.request.external;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
public interface CollapseKey {
	int id();

	String value();
}
