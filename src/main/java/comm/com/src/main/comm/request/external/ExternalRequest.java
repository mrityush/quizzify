package com.src.main.comm.request.external;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.src.main.comm.response.external.ExternalResponse;
import com.src.main.tlabs.quizzify.db.communications.ExternalCommunicationLink;

import layer1.layer2.serialise.JsonSerialisable;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
public interface ExternalRequest extends JsonSerialisable {
	default Optional<String> executeOnlyLatestKey() {
		return Optional.empty();
	}

	default Optional<String> executeOnlyLatestValue() {
		return Optional.empty();
	}

	List<Integer> lineageSequence();

	void postProcess() throws Exception;

	void preProcess() throws Exception;

	ExternalResponse process(ExternalCommunicationLink link) throws Exception;

	UUID requestId();

	public ExternalRequestType type();
}
