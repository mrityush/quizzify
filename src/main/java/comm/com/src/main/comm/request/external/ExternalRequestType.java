package com.src.main.comm.request.external;

import java.util.HashMap;
import java.util.Map;

public enum ExternalRequestType {
	GOOGLE_CCS(4, "Google CCS") {
	// public GcmCcsRequest create() {
	// return new GcmCcsRequest(this);
	// }
	};
	private static final Map<Integer, ExternalRequestType> types = new HashMap<Integer, ExternalRequestType>();

	static {
		for (ExternalRequestType type : ExternalRequestType.values()) {
			if (types.get(type.id) == null) {
				types.put(type.id, type);
			} else {
				// throw new BadPracticeException("Duplicate id: " + type.id);
			}
		}
	}
	private final int id;
	private final String description;

	private ExternalRequestType(int id, String description) {
		this.id = id;
		this.description = description;
	}

	public static ExternalRequestType valueOf(int id) {
		return types.get(id);
	}

	//
	// @Override
	// public Optional<? extends ExternalRequest> create() {
	// return Optional.empty();
	// }

	public int id() {
		return id;
	}

	public String description() {
		return description;
	}
}
