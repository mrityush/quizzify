package com.src.main.comm.request.external;

import java.util.HashMap;
import java.util.Map;

import com.src.main.comm.request.external.gcm.GcmCcsSendOTPRequest;

public enum GcmCcsRequestType {
	TALK_TO_SERVER(1, "Talk to server") {
		@Override
		public GcmCcsTalkToServerRequest create() {
			return new GcmCcsTalkToServerRequest();
		}
	},
	SEND_OTP(3, "Send OTP") {
		@Override
		public GcmCcsSendOTPRequest create() {
			return new GcmCcsSendOTPRequest();
		}
	};

	public abstract ExternalRequest create();

	public static final Map<Integer, GcmCcsRequestType> types = new HashMap<Integer, GcmCcsRequestType>();

	static {
		for (GcmCcsRequestType type : GcmCcsRequestType.values()) {
			if (types.get(type.id) == null) {
				types.put(type.id, type);
			} else {
				// throw new BadPracticeException("Duplicate id: " + type.id);
			}
		}
	}
	private final int id;
	private final String description;

	private GcmCcsRequestType(int id, String description) {
		this.id = id;
		this.description = description;
	}

	public static GcmCcsRequestType valueOf(int id) {
		return types.get(id);
	}

	public int id() {
		return id;
	}

	public String description() {
		return description;
	}

}
