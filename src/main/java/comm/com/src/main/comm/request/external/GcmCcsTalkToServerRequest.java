package com.src.main.comm.request.external;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.src.main.comm.request.external.gcm.GcmCcsRequest;

/**
 * 
 * @author Abhinav.Dwivedi
 *
 */
public class GcmCcsTalkToServerRequest extends GcmCcsRequest {

	public GcmCcsTalkToServerRequest() {
		super(GcmCcsRequestType.TALK_TO_SERVER);
	}

	protected GcmCcsTalkToServerRequest(Builder b) {
		super(b);
	}

	public static class Builder extends GcmCcsRequest.Builder<GcmCcsTalkToServerRequest, Builder> {

		public Builder() {
			super(GcmCcsRequestType.TALK_TO_SERVER);
		}

		@Override
		public GcmCcsTalkToServerRequest build() {
			return new GcmCcsTalkToServerRequest(this);
		}

		@Override
		public Builder self() {
			return this;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		super.inJsonString(g);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		super.populateFromJsonString(field, p);
	}
}
