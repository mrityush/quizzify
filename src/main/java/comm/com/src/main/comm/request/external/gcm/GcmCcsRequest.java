package com.src.main.comm.request.external.gcm;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import layer1.layer2.serialise.JsonUtil;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.src.main.comm.request.external.AbstractExternalRequest;
import com.src.main.comm.request.external.BasicCollapseKey;
import com.src.main.comm.request.external.CcsClient;
import com.src.main.comm.request.external.CollapseKey;
import com.src.main.comm.request.external.ExternalRequestType;
import com.src.main.comm.request.external.GcmCcsRequestType;
import com.src.main.comm.response.external.ExternalResponse;
import com.src.main.tlabs.quizzify.db.communications.ExternalCommunicationLink;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
public abstract class GcmCcsRequest extends AbstractExternalRequest {
	private String registrationId;
	private GcmCcsRequestType gcmCcsRequestType;
	private CollapseKey collapseKey;
	private boolean delayWhileIdle;
	private long ttlInSeconds;
	private boolean deliveryReceiptRequested;

	protected GcmCcsRequest(GcmCcsRequestType gcmCcsRequestType) {
		super(ExternalRequestType.GOOGLE_CCS);
		this.gcmCcsRequestType = gcmCcsRequestType;
		// addLineage(gcmCcsRequestType);
	}

	protected GcmCcsRequest(Builder<?, ?> b) {
		super(b);
		gcmCcsRequestType = b.gcmCcsRequestType;
		// addLineage(b.gcmCcsRequestType);
		registrationId = b.registrationId;
		collapseKey = b.collapseKey;
		delayWhileIdle = b.delayWhileIdle;
		ttlInSeconds = b.ttlInSeconds;
		deliveryReceiptRequested = b.deliveryReceiptRequested;
	}

	public static abstract class Builder<E extends GcmCcsRequest, T extends Builder<E, T>> extends
			AbstractExternalRequest.Builder<E, T> {
		private String registrationId;
		private GcmCcsRequestType gcmCcsRequestType;
		private CollapseKey collapseKey;
		private boolean delayWhileIdle;
		private long ttlInSeconds;
		private boolean deliveryReceiptRequested;

		protected Builder(GcmCcsRequestType gcmCcsRequestType) {
			super(ExternalRequestType.GOOGLE_CCS);
			this.gcmCcsRequestType = gcmCcsRequestType;
		}

		public abstract T self();

		public T registrationId(String registrationId) {
			this.registrationId = registrationId;
			return self();
		}

		public T collapseKey(CollapseKey collapseKey) {
			this.collapseKey = collapseKey;
			return self();
		}

		public T delayWhileIdle(boolean delayWhileIdle) {
			this.delayWhileIdle = delayWhileIdle;
			return self();
		}

		public T ttlInSeconds(long ttlInSeconds) {
			this.ttlInSeconds = ttlInSeconds;
			return self();
		}

		public T deliveryReceiptRequested(boolean deliveryReceiptRequested) {
			this.deliveryReceiptRequested = deliveryReceiptRequested;
			return self();
		}
	}

	@Override
	public ExternalResponse process(ExternalCommunicationLink link) throws Exception {
		CcsClient.instance().sendDownstreamMessage(link);
		return null;
	}

	@Override
	public Optional<String> executeOnlyLatestKey() {
		if (collapseKey().isPresent()) {
			return Optional.of(registrationId + ":" + collapseKey.value());
		}
		return Optional.empty();
	}

	@Override
	public Optional<String> executeOnlyLatestValue() {
		return Optional.of(requestId().toString());
	}

	public String createJsonMessage() {
		Map<String, Object> attributes = new LinkedHashMap<String, Object>();
		attributes.put("to", registrationId);
		if (collapseKey != null) {
			attributes.put("collapse_key", collapseKey.value());
		}
		if (ttlInSeconds > 0) {
			attributes.put("time_to_live", String.valueOf(ttlInSeconds));
		}
		if (delayWhileIdle) {
			attributes.put("delay_while_idle", delayWhileIdle);
		}
		if (deliveryReceiptRequested) {
			attributes.put("delivery_receipt_requested", deliveryReceiptRequested);
		}
		attributes.put("message_id", requestId().toString());
		Map<String, Object> dataAttributes = new HashMap<String, Object>();
		// Making clone to remove unnecessary attributes to free space for
		// payload. Total 4Kb data can be sent.
		GcmCcsRequest clone = this;// TEMporary solueion
		// GcmCcsRequest clone = (GcmCcsRequest) JsonUtil.clone(this, new
		// TypeReference<ExternalRequest>() {
		// }).get();
		clone.registrationId = null;
		dataAttributes.put("ccsRequest", clone);
		attributes.put("data", dataAttributes);
		return JsonUtil.toJson(attributes).get();
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		super.inJsonString(g);
		g.writeStringField("registrationId", registrationId);
		g.writeNumberField("gcmCcsRequestType", gcmCcsRequestType.id());
		g.writeNumberField("collapseKey", collapseKey != null ? collapseKey.id() : -1);
		g.writeNumberField("ttlInSeconds", ttlInSeconds);
		g.writeBooleanField("delayWhileIdle", delayWhileIdle);
		g.writeBooleanField("deliveryReceiptRequested", deliveryReceiptRequested);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("registrationId".equals(field)) {
			p.nextToken();
			registrationId = p.readValueAs(String.class);
		} else if ("gcmCcsRequestType".equals(field)) {
			p.nextToken();
			gcmCcsRequestType = GcmCcsRequestType.valueOf(p.readValueAs(int.class));
		} else if ("collapseKey".equals(field)) {
			p.nextToken();
			collapseKey = BasicCollapseKey.valueOf(p.readValueAs(int.class));
		} else if ("ttlInSeconds".equals(field)) {
			p.nextToken();
			ttlInSeconds = p.readValueAs(long.class);
		} else if ("delayWhileIdle".equals(field)) {
			p.nextToken();
			delayWhileIdle = p.readValueAs(boolean.class);
		} else if ("deliveryReceiptRequested".equals(field)) {
			p.nextToken();
			deliveryReceiptRequested = p.readValueAs(boolean.class);
		} else {
			super.populateFromJsonString(field, p);
		}
	}

	public GcmCcsRequestType gcmCcsRequestType() {
		return gcmCcsRequestType;
	}

	public String registrationId() {
		return registrationId;
	}

	public Optional<CollapseKey> collapseKey() {
		return Optional.ofNullable(collapseKey);
	}
}
