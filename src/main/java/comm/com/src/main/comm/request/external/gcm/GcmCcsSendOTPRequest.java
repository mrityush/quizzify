package com.src.main.comm.request.external.gcm;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.src.main.comm.request.external.GcmCcsRequestType;

/**
 * 
 * @author Abhinav.Dwivedi
 * @author mrityunjya.shukla
 *
 */
public class GcmCcsSendOTPRequest extends GcmCcsRequest {
	private String otp;

	public GcmCcsSendOTPRequest() {
		super(GcmCcsRequestType.SEND_OTP);
	}

	protected GcmCcsSendOTPRequest(Builder b) {
		super(b);
		otp = b.otp;
	}

	public static class Builder extends GcmCcsRequest.Builder<GcmCcsSendOTPRequest, Builder> {
		private String otp;

		public Builder() {
			super(GcmCcsRequestType.SEND_OTP);
		}

		@Override
		public GcmCcsSendOTPRequest build() {
			return new GcmCcsSendOTPRequest(this);
		}

		public Builder otp(String otp) {
			this.otp = otp;
			return self();
		}

		@Override
		public Builder self() {
			return this;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		super.inJsonString(g);
		g.writeStringField("otp", otp);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("otp".equals(field)) {
			p.nextToken();
			otp = p.readValueAs(String.class);
		} else {
			super.populateFromJsonString(field, p);
		}
	}
}
