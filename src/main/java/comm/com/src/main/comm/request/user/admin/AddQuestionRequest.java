package com.src.main.comm.request.user.admin;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.src.main.comm.response.user.admin.AddQuestionResponse;
import com.src.main.tlabs.quizzify.db.questionnaires.Question;
import com.src.main.tlabs.quizzify.dbops.manager.ServiceManager;

import layer1.layer2.comm.request.AbstractRequest;
import layer1.layer2.comm.request.Request;
import layer1.layer2.comm.request.RequestType;
import layer1.layer2.comm.response.Response;
import layer1.layer2.resource.Result;

public class AddQuestionRequest extends AbstractRequest {

	private Question question;

	public AddQuestionRequest(RequestType type) {
		super(type);
	}

	@Override
	public void authenticate(String... authParams) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeObjectField("question", question);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("question".equals(field)) {
			p.nextToken();
			question = p.readValueAs(new TypeReference<Request>() {
			});
		}
	}

	@Override
	public void postProcess() {
		// TODO Auto-generated method stub

	}

	@Override
	public void preProcess() {
		// TODO Auto-generated method stub

	}

	@Override
	public Response process() {
		boolean success = ServiceManager.questionnairesService().addQuestion(question);
		String message = (success) ? "Success in adding Question" : "Failure in adding Question";
		Result result = new Result(success, message);
		AddQuestionResponse response = new AddQuestionResponse.Builder().result(result).build();
		return response;
	}

	public Question question() {
		return question;
	}

	public void question(Question question) {
		this.question = question;
	}

	@Override
	public Boolean validate() {
		// TODO Auto-generated method stub
		return null;
	}

}
