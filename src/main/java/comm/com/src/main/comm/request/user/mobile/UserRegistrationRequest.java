package com.src.main.comm.request.user.mobile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.src.main.comm.response.user.mobile.UserRegistrationResponse;
import com.src.main.dtos.users.UsersDeviceDTO;
import com.src.main.tlabs.quizzify.dao.manager.DaoManager;
import com.src.main.tlabs.quizzify.db.statusfields.UserStatus;
import com.src.main.tlabs.quizzify.db.users.User;
import com.src.main.tlabs.quizzify.dbops.manager.ServiceManager;

import layer1.layer2.comm.request.AbstractRequest;
import layer1.layer2.comm.request.RequestType;
import layer1.layer2.comm.response.Response;
import layer1.layer2.comm.response.ResponseError;
import layer1.layer2.generics.Authentication;
import layer1.layer2.generics.CryptoUtil;
import layer1.layer2.resource.Result;
import layer1.layer2.serialise.JsonUtil;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
@Authentication(required = false)
public class UserRegistrationRequest extends AbstractRequest {
	private static final Logger logger = LoggerFactory.getLogger(UserRegistrationRequest.class);
	private String email;
	private UsersDeviceDTO userDeviceDto;

	public UserRegistrationRequest() {
	}

	public UserRegistrationRequest(RequestType type) {
		super(type);
	}

	@Override
	public void authenticate(String... authParams) {// throws
													// AuthenticationException {
		// checkApiVersion();
		if (!CryptoUtil.matchesAppPublicKey(authParams[0])) {
			// throw AuthenticationException.UNAUTHORIZED_SOURCE;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		super.inJsonString(g);
		g.writeObjectField("userDeviceDto", userDeviceDto);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("userDeviceDto".equals(field)) {
			p.nextToken();
			userDeviceDto = p.readValueAs(new TypeReference<UsersDeviceDTO>() {
			});
		} else if ("email".equals(field)) {
			p.nextToken();
			email = p.readValueAs(String.class);
		} else {
			super.populateFromJsonString(field, p);
		}
	}

	@Override
	public Response process() {
		User user = DaoManager.userDao().getUserByEmail(email);
		LocalDateTime now = LocalDateTime.now();
		boolean success = true;
		if (user != null) {
			success = success && ServiceManager.usersService().generateAndSendOtp(user,
					userDeviceDto);
		} else {
			user = new User.Builder().email(email).createdBy(User.SYSTEM_USER_ID).creationTime(now)
					.lastModifiedBy(User.SYSTEM_USER_ID).lastModificationTime(now).id(0).status(
							UserStatus.ACTIVE).type(1).userUuid(UUID.randomUUID()).build();
			success = success && DaoManager.userDao().addUser(user);
		}
		String responseMsg = null;
		ResponseError error = ResponseError.NO_ERROR;
		UserRegistrationResponse.Builder responseBuilder = new UserRegistrationResponse.Builder();

		Result result = new Result(success, responseMsg);
		result.addError(error);
		responseBuilder.result(result);
		String responseJson = JsonUtil.toJson(responseBuilder.build()).get();
		logger.debug("Response String : {}", responseJson);
		return responseBuilder.build();
	}

	@Override
	public Boolean validate() {
		// TODO add validity conditions
		return true;
	}

}
