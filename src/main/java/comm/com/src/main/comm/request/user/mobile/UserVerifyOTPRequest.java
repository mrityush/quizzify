package com.src.main.comm.request.user.mobile;

import java.io.IOException;

import layer1.layer2.comm.request.AbstractRequest;
import layer1.layer2.comm.request.RequestType;
import layer1.layer2.comm.response.Response;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.src.main.tlabs.quizzify.db.users.UserDevice;

public class UserVerifyOTPRequest extends AbstractRequest {
	private String otp;
	private UserDevice userDevice;

	public UserVerifyOTPRequest(RequestType requestType) {
		super(requestType);
	}

	public UserVerifyOTPRequest() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		super.inJsonString(g);
		g.writeObjectField("userDevice", userDevice);
		g.writeStringField("otp", otp);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("otp".equals(field)) {
			p.nextToken();
			otp = p.readValueAs(String.class);
		} else if ("userDevice".equals(field)) {
			p.nextToken();
			userDevice = p.readValueAs(new TypeReference<UserDevice>() {
			});
		} else {
			super.populateFromJsonString(field, p);
		}
	}

	@Override
	public void authenticate(String... authParams) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public Response process() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean validate() {
		// TODO Auto-generated method stub
		return null;
	}

}
