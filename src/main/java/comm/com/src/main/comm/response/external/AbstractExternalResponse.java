package com.src.main.comm.response.external;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * 
 * @author Abhinav.Dwivedi
 *
 */
public abstract class AbstractExternalResponse implements ExternalResponse {
	private UUID requestId;
	private ExternalResponseType type;
	private final List<Integer> sequence;

	protected AbstractExternalResponse(ExternalResponseType type) {
		this.type = type;
		sequence = new ArrayList<Integer>();
		sequence.add(type.id());
	}

	protected AbstractExternalResponse(Builder<?, ?> b) {
		this(b.type);
		requestId = b.requestId;
	}

	public static abstract class Builder<E extends ExternalResponse, T extends Builder<E, T>> {
		private UUID requestId;
		private ExternalResponseType type;

		protected Builder(ExternalResponseType type) {
			this.type = type;
		}

		public abstract E build();

		public abstract T self();

		public T requestId(UUID requestId) {
			this.requestId = requestId;
			return self();
		}
	}

	public static class ExternalResponseDeserializer extends JsonDeserializer<ExternalResponse> {
		@Override
		public ExternalResponse deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			if (!"lineage".equals(field)) {
				throw new RuntimeException("The first field in serialization should be : lineage");
			}
			p.nextToken();
			// final ExternalResponse response =
			// ExternalResponseFactory.construct(
			// p.readValueAs(new TypeReference<List<Integer>>() {
			// })).get();
			final ExternalResponse response = null;
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				response.populateFromJsonString(field, p);
			}
			return response;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeObjectField("lineage", sequence);
		g.writeNumberField("type", type().id());
		g.writeObjectField("requestId", requestId);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("type".equals(field)) {
			p.nextToken();
			type = ExternalResponseType.valueOf(p.readValueAs(int.class));
		} else if ("requestId".equals(field)) {
			p.nextToken();
			requestId = p.readValueAs(UUID.class);
		}
	}

	@Override
	public List<Integer> lineageSequence() {
		return sequence;
	}

	@Override
	public ExternalResponseType type() {
		return type;
	}

	@Override
	public UUID requestId() {
		return requestId;
	}

	@Override
	public String toString() {
		return serializeAsJson().orElse(super.toString());
	}
}
