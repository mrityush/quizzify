package com.src.main.comm.response.external;

import java.util.List;
import java.util.UUID;

import layer1.layer2.serialise.JsonSerialisable;

/**
 * 
 * @author Abhinav.Dwivedi
 *
 */
public interface ExternalResponse extends JsonSerialisable {
	UUID requestId();

	ExternalResponseType type();

	List<Integer> lineageSequence();

	void process();
}
