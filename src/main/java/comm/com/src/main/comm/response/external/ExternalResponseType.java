package com.src.main.comm.response.external;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * 
 * @author Abhinav.Dwivedi
 *
 */
public enum ExternalResponseType {
	GOOGLE_PLACES(1, "Google Places") {
	},
	FOURSQUARE(2, "Foursquare") {
	},
	GOOGLE_CCS(4, "Google CCS") {
		public Optional<GoogleCloudMessagingCcsResponse> create() {
			return Optional.of(new GoogleCloudMessagingCcsResponse());
		}
	};
	private static final Map<Integer, ExternalResponseType> types = new HashMap<Integer, ExternalResponseType>();

	static {
		for (ExternalResponseType type : ExternalResponseType.values()) {
			if (types.get(type.id) == null) {
				types.put(type.id, type);
			} else {
				// throw new BadPracticeException("Duplicate id: " + type.id);
			}
		}
	}
	private final int id;
	private final String description;

	private ExternalResponseType(int id, String description) {
		this.id = id;
		this.description = description;
	}

	public static ExternalResponseType valueOf(int id) {
		return types.get(id);
	}

	public Optional<? extends ExternalResponse> create() {
		return Optional.empty();
	}

	public int id() {
		return id;
	}

	public String description() {
		return description;
	}
}
