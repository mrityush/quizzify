package com.src.main.comm.response.external;

/**
 * 
 * @author Abhinav.Dwivedi
 *
 */
public class GoogleCloudMessagingCcsResponse extends AbstractExternalResponse {
	public GoogleCloudMessagingCcsResponse() {
		super(ExternalResponseType.GOOGLE_CCS);
	}

	@Override
	public void process() {

	}
}
