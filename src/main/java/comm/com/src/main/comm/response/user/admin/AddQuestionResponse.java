package com.src.main.comm.response.user.admin;

import java.io.IOException;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;

import layer1.layer2.comm.response.Response;
import layer1.layer2.resource.Result;

public class AddQuestionResponse implements Response {

	public static class Builder {
		private Result result;

		public AddQuestionResponse build() {
			return new AddQuestionResponse(this);
		}

		public Builder result(Result result) {
			this.result = result;
			return this;
		}

	}

	private Result result;

	public AddQuestionResponse(Builder builder) {
		result = builder.result;
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeObjectField("result", result);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void process() {

	}

	@Override
	public Optional<String> replyInJson() {
		return serializeAsJson();
	}

	@Override
	public Result result() {
		return result;
	}

}
