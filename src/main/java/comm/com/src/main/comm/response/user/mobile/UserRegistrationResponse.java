package com.src.main.comm.response.user.mobile;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import layer1.layer2.comm.response.AbstractResponse;
import layer1.layer2.serialise.JacksonUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.src.main.tlabs.quizzify.db.users.User;

public class UserRegistrationResponse extends AbstractResponse {
	private static final Logger logger = LoggerFactory.getLogger(UserRegistrationResponse.class);
	private User user;

	protected UserRegistrationResponse(Builder b) {
		super(b);
		user = b.user;
	}

	public UserRegistrationResponse() {
		// TODO Auto-generated constructor stub
	}

	public static class Builder extends AbstractResponse.Builder<UserRegistrationResponse, Builder> {
		private User user;

		@Override
		public UserRegistrationResponse build() {
			return new UserRegistrationResponse(this);
		}

		@Override
		public Builder self() {
			return this;
		}

		public Builder userDTO(User user) {
			this.user = user;
			return this;
		}
	}

	@Override
	public void process() {
	}

	@Override
	public Optional<String> replyInJson() {
		try {
			ObjectNode responseNode = (ObjectNode) JacksonUtil.DEFAULT.mapper().readTree(serializeAsJson().get());
			responseNode.remove(Arrays.asList("type", "restResponseType", "internalRestResponseType", "userId",
					"mobileRestResponseType"));
			JsonNode userNode = responseNode.get("user");
			if (userNode instanceof ObjectNode) {
				((ObjectNode) userNode).remove(Arrays.asList("uuid", "createdBy", "creationTime", "lastModifiedBy"));
			}
			return Optional.of(JacksonUtil.DEFAULT.mapper().writeValueAsString(responseNode));
		} catch (Exception e) {
			logger.error("Error replying in Json", e);
		}
		return Optional.empty();
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		super.inJsonString(g);
		g.writeObjectField("user", user);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("user".equals(field)) {
			p.nextToken();
			user = p.readValueAs(new TypeReference<User>() {
			});
		} else {
			super.populateFromJsonString(field, p);
		}
	}

	public User userDTO() {
		return user;
	}
}
