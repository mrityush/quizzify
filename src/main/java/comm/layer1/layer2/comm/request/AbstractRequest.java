package layer1.layer2.comm.request;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import layer1.layer2.config.SystemConfig;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
public abstract class AbstractRequest implements Request {
	protected static List<String> supportedVersions;
	protected UUID userUuid;
	protected String token;
	protected String versionId;
	private RequestType type;

	protected AbstractRequest() {

	}

	public AbstractRequest(RequestType type2) {
		type = type2;
	}

	public static boolean initialize() {
		String[] tokens = SystemConfig.supportedVersions().split(",");
		supportedVersions = Arrays.asList(tokens);
		return true;
	}

	protected void checkApiVersion() {// throws AuthenticationException {
		if (!supportedVersions.contains(versionId)) {
			// throw AuthenticationException.UNSUPPORTED_SOURCE; // TODO do we
			// have
			// to keep this
			// ?
		}
	}

	public void authenticate(String... authParams) throws Exception {// AuthenticationException
																		// earlier
		// checkApiVersion();
		token = authParams[0];
		// Authentication auth =
		// this.getClass().getAnnotation(Authentication.class);
		// if (auth == null || auth.required()) {
		// if (userUuid() == null
		// || !Authenticator.mobileUserRestAuthenticator().authenticate(
		// new MobileUserRestAuthenticationData(userUuid(), token))) {
		// //throw AuthenticationException.INVALID_CREDENTIALS;
		// }
		// }
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeObjectField("userUuid", userUuid);
		g.writeNumberField("type", type.id());
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("userUuid".equals(field)) {
			p.nextToken();
			userUuid = p.readValueAs(UUID.class);
		} else if ("type".equals(field)) {
			p.nextToken();
			type = RequestType.valueOf(p.readValueAs(Integer.class));
		}
	}

	public static class Deserializer extends JsonDeserializer<Request> {
		@Override
		public Request deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			if (!"type".equals(field)) {
				throw new RuntimeException("The first field in serialization should be : type");
			}
			p.nextToken();
			RequestType type = RequestType.valueOf(p.readValueAs(int.class));
			final Request request = type.create();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				request.populateFromJsonString(field, p);
			}
			return request;
		}
	}

	@Override
	public String toString() {
		return serializeAsJson().orElse(super.toString());
	}

	@Override
	public void preProcess() {

	}

	@Override
	public void postProcess() {

	}

	public RequestType type() {
		return type;
	}

	public UUID userUuid() {
		return userUuid;
	}

	public String token() {
		return token;
	}
}
