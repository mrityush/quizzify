package layer1.layer2.comm.request;

import layer1.layer2.comm.response.Response;
import layer1.layer2.serialise.JsonSerialisable;

public interface Request extends JsonSerialisable {
	void authenticate(String... authParams) throws Exception;

	void postProcess();

	void preProcess();

	Response process();

	Boolean validate();
}
