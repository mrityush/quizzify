package layer1.layer2.comm.request;

import com.src.main.comm.request.user.mobile.UserRegistrationRequest;
import com.src.main.comm.request.user.mobile.UserVerifyOTPRequest;

public abstract class RequestHandler {
	private static final UserRegistrationRequest USER_REGISTRATION_REQUEST = new UserRegistrationRequest();
	private static final UserVerifyOTPRequest USER_VERIFY_OTP_REQUEST = new UserVerifyOTPRequest();

	public static UserRegistrationRequest userRegistrationRequestHandler() {
		return USER_REGISTRATION_REQUEST;
	}

	public static UserVerifyOTPRequest userVerifyOtpRequestHandler() {
		return USER_VERIFY_OTP_REQUEST;
	}
}
