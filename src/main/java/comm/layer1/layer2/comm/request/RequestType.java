package layer1.layer2.comm.request;

import java.util.HashMap;
import java.util.Map;

import com.src.main.comm.request.user.admin.AddQuestionRequest;
import com.src.main.comm.request.user.mobile.UserRegistrationRequest;
import com.src.main.comm.request.user.mobile.UserVerifyOTPRequest;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
public enum RequestType {
	USER_REGISTRATION(101, "User Registration") {

		@Override
		public Request create() {
			return new UserRegistrationRequest(this);
		}
	},
	USER_VERIFICATION(102, "Message Process") {

		@Override
		public Request create() {
			return new UserVerifyOTPRequest(this);
		}
	},
	ADD_QUESTION(103, "Add Question") {

		@Override
		public Request create() {
			return new AddQuestionRequest(this);
		}
	};

	private static final Map<Integer, RequestType> types = new HashMap<Integer, RequestType>();

	static {
		for (RequestType type : RequestType.values()) {
			if (types.get(type.id) == null) {
				types.put(type.id, type);
			} else {
				// throw new BadPracticeException("Duplicate id: " + type.id);
			}
		}
	}
	private final int id;
	private final String description;

	public abstract Request create();

	private RequestType(int id, String description) {
		this.id = id;
		this.description = description;
	}

	public static RequestType valueOf(int id) {
		return types.get(id);
	}

	public int id() {
		return id;
	}

	public String description() {
		return description;
	}

}
