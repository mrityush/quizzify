package layer1.layer2.comm.response;

import java.io.IOException;

import layer1.layer2.resource.Result;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/***
 * 
 * @author mrityunjya.shukla
 *
 */
public abstract class AbstractResponse implements Response {
	protected Result result;

	protected AbstractResponse(Builder<?, ?> b) {
		result = b.result;
	}

	public AbstractResponse() {

	}

	public static abstract class Builder<E extends Response, T extends Builder<E, T>> {
		private Result result;

		public abstract E build();

		public abstract T self();

		public T result(Result result) {
			this.result = result;
			return self();
		}
	}

	public static class Deserializer extends JsonDeserializer<Response> {
		@Override
		public Response deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			// if (!"type".equals(field)) {
			// throw new
			// RuntimeException("The first field in serialization should be : type");
			// }
			p.nextToken();
			ResponseType type = ResponseType.valueOf(p.readValueAs(int.class));
			final Response response = type.create();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				response.populateFromJsonString(field, p);
			}
			return response;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeObjectField("result", result);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("result".equals(field)) {
			p.nextToken();
			result = p.readValueAs(Result.class);
		}
	}

	@Override
	public Result result() {
		return result;
	}

	@Override
	public String toString() {
		return serializeAsJson().orElse(super.toString());
	}
}
