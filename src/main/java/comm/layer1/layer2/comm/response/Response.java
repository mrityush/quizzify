package layer1.layer2.comm.response;

import java.util.Optional;

import layer1.layer2.resource.Result;
import layer1.layer2.serialise.JsonSerialisable;

public interface Response extends JsonSerialisable {
	void process();

	Optional<String> replyInJson();

	Result result();
}
