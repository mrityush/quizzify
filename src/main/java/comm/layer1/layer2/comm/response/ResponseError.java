package layer1.layer2.comm.response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import layer1.layer2.serialise.JsonSerialisable;

/**
 * 
 * @author mj
 *
 */
public enum ResponseError implements JsonSerialisable {
	INTERNAL_ERROR(2, "Internal Error."), NO_ERROR(4, "No Error"), PARSING_ERROR(3,
			"Parsing Error."), USER_REGISTRATION_DUPLICATE(1, "User already registered.");
	public static class Deserializer extends JsonDeserializer<ResponseError> {
		@Override
		public ResponseError deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			if (!"code".equals(field)) {
				throw new RuntimeException("The first field in serialization should be : code");
			}
			p.nextToken();
			final ResponseError error = ResponseError.valueOf(p.readValueAs(int.class));
			while (p.nextToken() != JsonToken.END_OBJECT) {
			}
			return error;
		}
	}

	private static final Map<Integer, ResponseError> errors = new HashMap<Integer, ResponseError>();

	static {
		for (ResponseError error : ResponseError.values()) {
			if (errors.get(error.code) == null) {
				errors.put(error.code, error);
			} else {
				// throw new BadPracticeException("Duplicate id: " +
				// error.code);
			}
		}
	}

	public static ResponseError valueOf(int id) {
		return errors.get(id);
	}

	private final int code;
	private final String description;

	private ResponseError(int code, String description) {
		this.code = code;
		this.description = description;
	}

	public int code() {
		return code;
	}

	public String description() {
		return description;
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeNumberField("code", code);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		// TODO Auto-generated method stub

	}
}
