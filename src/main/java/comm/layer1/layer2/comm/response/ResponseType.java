package layer1.layer2.comm.response;

import java.util.HashMap;
import java.util.Map;

import com.src.main.comm.response.user.mobile.UserRegistrationResponse;
import com.src.main.comm.response.user.mobile.UserVerifyOTPResponse;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
public enum ResponseType {
	USER_REGISTRATION(101, "User Registration") {

		@Override
		public Response create() {
			return new UserRegistrationResponse();
		}
	},
	USER_VERIFICATION(102, "Message Process") {

		@Override
		public Response create() {
			return new UserVerifyOTPResponse();
		}
	};

	private static final Map<Integer, ResponseType> types = new HashMap<Integer, ResponseType>();

	static {
		for (ResponseType type : ResponseType.values()) {
			if (types.get(type.id) == null) {
				types.put(type.id, type);
			} else {
				// throw new BadPracticeException("Duplicate id: " + type.id);
			}
		}
	}
	private final int id;
	private final String description;

	private ResponseType(int id, String description) {
		this.id = id;
		this.description = description;
	}

	public abstract Response create();

	public static ResponseType valueOf(int id) {
		return types.get(id);
	}

	public int id() {
		return id;
	}

	public String description() {
		return description;
	}

}
