package layer1.layer2.resource;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
public class AuthenticationException extends Exception {
	public static final AuthenticationException INVALID_CREDENTIALS = new AuthenticationException(
			"Invalid Credentials.");
	public static final AuthenticationException UNAUTHORIZED_SOURCE = new AuthenticationException(
			"Unauthorized Source.");
	public static final AuthenticationException UNSUPPORTED_SOURCE = new AuthenticationException("Unsupported Source.");
	private static final long serialVersionUID = 1L;

	private AuthenticationException(String message) {
		super(message);
	}
}
