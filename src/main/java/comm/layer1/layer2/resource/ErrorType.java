package layer1.layer2.resource;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAllowedException;
import javax.ws.rs.NotAuthorizedException;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
public enum ErrorType {
	BAD_REQUEST(new BadRequestException()), INTERNAL_SERVER_ERROR(new InternalServerErrorException()), WRONG_CREDENTIALS(
			new NotAuthorizedException("Authorization credentials Invalid/Unavailable")), NOT_SUPPORTED_ANY_MORE(
			new NotAllowedException("REST API level not supported anymore."));
	private final RuntimeException exception;

	private ErrorType(RuntimeException exception) {
		this.exception = exception;
	}

	public RuntimeException exception() {
		return exception;
	}
}