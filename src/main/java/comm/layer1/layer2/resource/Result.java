package layer1.layer2.resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import layer1.layer2.comm.response.ResponseError;
import layer1.layer2.serialise.JsonSerialisable;

public class Result implements JsonSerialisable {

	public static class Deserializer extends JsonDeserializer<Result> {

		@Override
		public Result deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			p.nextToken();
			final Result result = new Result();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				result.populateFromJsonString(field, p);
			}
			return result;
		}

	}

	private List<ResponseError> errors;
	private String message;
	private boolean success;

	public Result() {

	}

	public Result(boolean success, String message) {
		this.message = message;
		this.success = success;
	}

	public boolean addError(ResponseError e) {
		if (errors == null) {
			errors = new ArrayList<ResponseError>();
		}
		return errors.add(e);
	}

	public List<ResponseError> errors() {
		return errors;
	}

	public boolean hasNoErrors() {
		boolean noError = success;
		noError = noError && (errors == null || errors.stream().allMatch((
				e) -> e == ResponseError.NO_ERROR));
		return noError;
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeBooleanField("success", success);
		g.writeStringField("message", message);
		g.writeObjectField("errors", errors);
	}

	public boolean isSuccess() {
		return success;
	}

	public String message() {
		return message;
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if (field.equals("success")) {
			success = p.readValueAs(Boolean.class);
		} else if (field.equals("message")) {
			message = p.readValueAs(String.class);
		} else if ("errors".equals(field)) {
			p.nextToken();
			errors = p.readValueAs(new TypeReference<List<ResponseError>>() {
			});
		}

	}

	@Override
	public String toString() {
		return serializeAsJson().orElse(super.toString());
	}

}
