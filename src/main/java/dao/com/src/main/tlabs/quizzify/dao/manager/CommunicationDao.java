package com.src.main.tlabs.quizzify.dao.manager;

import java.util.List;

import org.hibernate.Session;

import com.src.main.tlabs.quizzify.db.communications.CommunicationLink;
import com.src.main.tlabs.quizzify.db.communications.ExternalCommunicationLink;
import com.src.main.tlabs.quizzify.dbops.manager.DatabaseOps;

public interface CommunicationDao extends DatabaseOps {
	public boolean addCommunicationLink(CommunicationLink communicationLink);

	public boolean addCommunicationLink(CommunicationLink communicationLink, Session session);

	public boolean addExternalCommunicationLink(ExternalCommunicationLink communicationLink);

	public boolean addExternalCommunicationLink(ExternalCommunicationLink communicationLink, Session session);

	public List<CommunicationLink> getCommunicationLinkById(long id);

	public List<ExternalCommunicationLink> getExternalCommunicationLinkById(long id);

	public ExternalCommunicationLink getExternalCommunicationLinkByRequestId(String messageId);

	public boolean hardDeleteCommunicationLink(CommunicationLink commLink);

	public boolean hardDeleteCommunicationLink(CommunicationLink commLink, Session session);

	public boolean hardDeleteExternalCommunicationLink(ExternalCommunicationLink commLink);

	public boolean hardDeleteExternalCommunicationLink(ExternalCommunicationLink commLink, Session session);

	public boolean softDeleteCommunicationLink(CommunicationLink commLink);

	public boolean softDeleteCommunicationLink(CommunicationLink commLink, Session session);

	public boolean softDeleteExternalCommunicationLink(ExternalCommunicationLink commLink);

	public boolean softDeleteExternalCommunicationLink(ExternalCommunicationLink commLink, Session session);

	public boolean updateCommunicationLink(CommunicationLink commLink);

	public boolean updateCommunicationLink(CommunicationLink commLink, Session session);

	public boolean updateExternalCommunicationLink(ExternalCommunicationLink commLink);

	public boolean updateExternalCommunicationLink(ExternalCommunicationLink commLink, Session session);
}
