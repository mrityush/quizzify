package com.src.main.tlabs.quizzify.dao.manager;

import com.src.main.tlabs.quizzify.dao.objects.CommunicationDefaultDao;
import com.src.main.tlabs.quizzify.dao.objects.EmailsDefaultDao;
import com.src.main.tlabs.quizzify.dao.objects.QuestionnaireDefaultDao;
import com.src.main.tlabs.quizzify.dao.objects.RewardDefaultDao;
import com.src.main.tlabs.quizzify.dao.objects.ShipmentDefaultDao;
import com.src.main.tlabs.quizzify.dao.objects.SponsorDefaultDao;
import com.src.main.tlabs.quizzify.dao.objects.UserDefaultDao;

public class DaoManager {
	private static CommunicationDao communicationDao = new CommunicationDefaultDao();
	private static EmailsDao emailsDao = new EmailsDefaultDao();
	private static QuestionnaireDao questionnaireDao = new QuestionnaireDefaultDao();
	private static RewardDao rewardDao = new RewardDefaultDao();
	private static ShipmentDao shipmentDao = new ShipmentDefaultDao();
	private static SponsorDao sponsorDao = new SponsorDefaultDao();
	private static UserDao userDao = new UserDefaultDao();

	public static CommunicationDao communicationDao() {
		return communicationDao;
	}

	public static EmailsDao emailsDao() {
		return emailsDao;
	}

	public static QuestionnaireDao questionnaireDao() {
		return questionnaireDao;
	}

	public static RewardDao rewardDao() {
		return rewardDao;
	}

	public static ShipmentDao shipmentDao() {
		return shipmentDao;
	}

	public static SponsorDao sponsorDao() {
		return sponsorDao;
	}

	public static UserDao userDao() {
		return userDao;
	}

}
