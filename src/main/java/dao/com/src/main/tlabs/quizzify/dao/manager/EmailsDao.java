package com.src.main.tlabs.quizzify.dao.manager;

import java.util.List;

import org.hibernate.Session;

import com.src.main.tlabs.quizzify.db.emails.Email;
import com.src.main.tlabs.quizzify.db.emails.Template;
import com.src.main.tlabs.quizzify.dbops.manager.DatabaseOps;

public interface EmailsDao extends DatabaseOps {
	public boolean addEmail(Email email);

	public boolean addEmail(Email email, Session session);

	public boolean addTemplate(Template template);

	public boolean addTemplate(Template template, Session session);

	public List<Email> getEmailById(long id);

	public List<Template> getTemplateById(long id);

	public boolean hardDeleteEmail(Email email);

	public boolean hardDeleteEmail(Email email, Session session);

	public boolean hardDeleteTemplate(Template template);

	public boolean hardDeleteTemplate(Template template, Session session);

	public boolean softDeleteEmail(Email email);

	public boolean softDeleteEmail(Email email, Session session);

	public boolean softDeleteTemplate(Template template);

	public boolean softDeleteTemplate(Template template, Session session);

	public boolean updateEmail(Email email);

	public boolean updateEmail(Email email, Session session);

	public boolean updateTemplate(Template template);

	public boolean updateTemplate(Template template, Session session);
}
