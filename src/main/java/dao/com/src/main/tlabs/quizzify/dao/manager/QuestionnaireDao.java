package com.src.main.tlabs.quizzify.dao.manager;

import java.util.List;

import org.hibernate.Session;

import com.src.main.tlabs.quizzify.db.questionnaires.AnswerSet;
import com.src.main.tlabs.quizzify.db.questionnaires.Category;
import com.src.main.tlabs.quizzify.db.questionnaires.Question;
import com.src.main.tlabs.quizzify.db.questionnaires.QuizSet;
import com.src.main.tlabs.quizzify.db.questionnaires.Quiz;
import com.src.main.tlabs.quizzify.db.questionnaires.Set;
import com.src.main.tlabs.quizzify.dbops.manager.DatabaseOps;

public interface QuestionnaireDao extends DatabaseOps {
	public boolean addAnswerSet(AnswerSet answerSet);

	public boolean addAnswerSet(AnswerSet answerSet, Session session);

	public boolean addCategory(Category category);

	public boolean addCategory(Category category, Session session);

	public boolean addQuestion(Question question);

	public boolean addQuestion(Question question, Session session);

	public boolean addQuiz(Quiz quiz);

	public boolean addQuiz(Quiz quiz, Session session);

	public boolean addQuizSet(QuizSet quizSet);

	public boolean addQuizSet(QuizSet quizSet, Session session);

	public boolean addSet(Set set);

	public boolean addSet(Set set, Session session);

	public List<AnswerSet> getAnswerSetById(long id);

	public List<Category> getCategoryById(long id);

	public List<Question> getQuestionById(long id);

	public List<Quiz> getQuizById(long id);

	public List<QuizSet> getQuizSetById(long id);

	public List<Set> getSetById(long id);

	public boolean hardDeleteAnswerSet(AnswerSet answerSet);

	public boolean hardDeleteAnswerSet(AnswerSet answerSet, Session session);

	public boolean hardDeleteCategory(Category category);

	public boolean hardDeleteCategory(Category category, Session session);

	public boolean hardDeleteQuestion(Question question);

	public boolean hardDeleteQuestion(Question question, Session session);

	public boolean hardDeleteQuiz(Quiz quiz);

	public boolean hardDeleteQuiz(Quiz quiz, Session session);

	public boolean hardDeleteQuizSet(QuizSet quizSet);

	public boolean hardDeleteQuizSet(QuizSet quizSet, Session session);

	public boolean hardDeleteSet(Set set);

	public boolean hardDeleteSet(Set set, Session session);

	public boolean softDeleteAnswerSet(AnswerSet answerSet);

	public boolean softDeleteAnswerSet(AnswerSet answerSet, Session session);

	public boolean softDeleteCategory(Category category);

	public boolean softDeleteCategory(Category category, Session session);

	public boolean softDeleteQuestion(Question question);

	public boolean softDeleteQuestion(Question question, Session session);

	public boolean softDeleteQuiz(Quiz quiz);

	public boolean softDeleteQuiz(Quiz quiz, Session session);

	public boolean softDeleteQuizSet(QuizSet quizSet);

	public boolean softDeleteQuizSet(QuizSet quizSet, Session session);

	public boolean softDeleteSet(Set set);

	public boolean softDeleteSet(Set set, Session session);

	public boolean updateAnswerSet(AnswerSet answerSet);

	public boolean updateAnswerSet(AnswerSet answerSet, Session session);

	public boolean updateCategory(Category category);

	public boolean updateCategory(Category category, Session session);

	public boolean updateQuestion(Question question);

	public boolean updateQuestion(Question question, Session session);

	public boolean updateQuiz(Quiz quiz);

	public boolean updateQuiz(Quiz quiz, Session session);

	public boolean updateQuizSet(QuizSet quizSet);

	public boolean updateQuizSet(QuizSet quizSet, Session session);

	public boolean updateSet(Set set);

	public boolean updateSet(Set set, Session session);

}
