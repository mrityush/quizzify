package com.src.main.tlabs.quizzify.dao.manager;

import java.util.List;

import org.hibernate.Session;

import com.src.main.tlabs.quizzify.db.rewards.Reward;
import com.src.main.tlabs.quizzify.db.rewards.RewardsStat;
import com.src.main.tlabs.quizzify.dbops.manager.DatabaseOps;

public interface RewardDao extends DatabaseOps {
	public boolean addReward(Reward reward);

	public boolean addReward(Reward reward, Session session);

	public boolean addRewardsStat(RewardsStat rewardStat);

	public boolean addRewardsStat(RewardsStat rewardStat, Session session);

	public List<Reward> getRewardById(long id);

	public List<RewardsStat> getRewardsStatById(long id);

	public boolean hardDeleteReward(Reward reward);

	public boolean hardDeleteReward(Reward reward, Session session);

	public boolean hardDeleteRewardsStat(RewardsStat rewardStat);

	public boolean hardDeleteRewardsStat(RewardsStat rewardStat, Session session);

	public boolean softDeleteReward(Reward reward);

	public boolean softDeleteReward(Reward reward, Session session);

	public boolean softDeleteRewardsStat(RewardsStat rewardStat);

	public boolean softDeleteRewardsStat(RewardsStat rewardStat, Session session);

	public boolean updateReward(Reward reward);

	public boolean updateReward(Reward reward, Session session);

	public boolean updateRewardsStat(RewardsStat rewardStat);

	public boolean updateRewardsStat(RewardsStat rewardStat, Session session);
}
