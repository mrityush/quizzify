package com.src.main.tlabs.quizzify.dao.manager;

import java.util.List;

import org.hibernate.Session;

import com.src.main.tlabs.quizzify.db.shipments.Courier;
import com.src.main.tlabs.quizzify.db.shipments.Shipment;
import com.src.main.tlabs.quizzify.dbops.manager.DatabaseOps;

public interface ShipmentDao extends DatabaseOps {
	public boolean addCourier(Courier courier);

	public boolean addCourier(Courier courier, Session session);

	public boolean addShipment(Shipment shipment);

	public boolean addShipment(Shipment shipment, Session session);

	public List<Courier> getCourierById(long id);

	public List<Shipment> getShipmentById(long id);

	public boolean hardDeleteCourier(Courier courier);

	public boolean hardDeleteCourier(Courier courier, Session session);

	public boolean hardDeleteShipment(Shipment shipment);

	public boolean hardDeleteShipment(Shipment shipment, Session session);

	public boolean softDeleteCourier(Courier courier);

	public boolean softDeleteCourier(Courier courier, Session session);

	public boolean softDeleteShipment(Shipment shipment);

	public boolean softDeleteShipment(Shipment shipment, Session session);

	public boolean updateCourier(Courier courier);

	public boolean updateCourier(Courier courier, Session session);

	public boolean updateShipment(Shipment shipment);

	public boolean updateShipment(Shipment shipment, Session session);
}
