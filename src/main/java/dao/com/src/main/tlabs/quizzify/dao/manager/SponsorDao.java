package com.src.main.tlabs.quizzify.dao.manager;

import java.util.List;

import org.hibernate.Session;

import com.src.main.tlabs.quizzify.db.sponsors.Sponsor;
import com.src.main.tlabs.quizzify.db.sponsors.SponsorsStat;
import com.src.main.tlabs.quizzify.dbops.manager.DatabaseOps;

public interface SponsorDao extends DatabaseOps {
	public boolean addSponsor(Sponsor sponsor);

	public boolean addSponsor(Sponsor sponsor, Session session);

	public boolean addSponsorsStat(SponsorsStat sponsorStat);

	public boolean addSponsorsStat(SponsorsStat sponsorStat, Session session);

	public List<Sponsor> getSponsorById(long id);

	public List<SponsorsStat> getSponsorsStatById(long id);

	public boolean hardDeleteSponsor(Sponsor sponsor);

	public boolean hardDeleteSponsor(Sponsor sponsor, Session session);

	public boolean hardDeleteSponsorsStat(SponsorsStat sponsorStat);

	public boolean hardDeleteSponsorsStat(SponsorsStat sponsorStat, Session session);

	public boolean softDeleteSponsor(Sponsor sponsor);

	public boolean softDeleteSponsor(Sponsor sponsor, Session session);

	public boolean softDeleteSponsorsStat(SponsorsStat sponsorStat);

	public boolean softDeleteSponsorsStat(SponsorsStat sponsorStat, Session session);

	public boolean updateSponsor(Sponsor sponsor);

	public boolean updateSponsor(Sponsor sponsor, Session session);

	public boolean updateSponsorsStat(SponsorsStat sponsorStat);

	public boolean updateSponsorsStat(SponsorsStat sponsorStat, Session session);
}
