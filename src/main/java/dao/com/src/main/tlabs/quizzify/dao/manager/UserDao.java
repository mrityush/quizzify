package com.src.main.tlabs.quizzify.dao.manager;

import java.util.List;

import org.hibernate.Session;

import com.src.main.tlabs.quizzify.db.statusfields.UserDeviceStatus;
import com.src.main.tlabs.quizzify.db.users.User;
import com.src.main.tlabs.quizzify.db.users.UserDevice;
import com.src.main.tlabs.quizzify.db.users.UserOTP;
import com.src.main.tlabs.quizzify.db.users.UserProfile;
import com.src.main.tlabs.quizzify.db.users.UserQuestionProfile;
import com.src.main.tlabs.quizzify.db.users.UserQuizPerformance;
import com.src.main.tlabs.quizzify.db.users.UserQuizProfile;
import com.src.main.tlabs.quizzify.dbops.manager.DatabaseOps;

public interface UserDao extends DatabaseOps {
	public boolean addUser(User email);

	public boolean addUser(User email, Session session);

	public boolean addUsersDevice(UserDevice email);

	public boolean addUsersDevice(UserDevice email, Session session);

	public boolean addUsersOtp(UserOTP usersOtp);

	public boolean addUsersOtp(UserOTP usersOtp, Session session);

	public boolean addUsersProfile(UserProfile email);

	public boolean addUsersProfile(UserProfile email, Session session);

	public boolean addUsersQuestionProfile(UserQuestionProfile email);

	public boolean addUsersQuestionProfile(UserQuestionProfile email, Session session);

	public boolean addUsersQuizPerformance(UserQuizPerformance email);

	public boolean addUsersQuizPerformance(UserQuizPerformance email, Session session);

	public boolean addUsersQuizProfile(UserQuizProfile email);

	public boolean addUsersQuizProfile(UserQuizProfile email, Session session);

	public User getUserByEmail(String email);

	public List<User> getUserById(long id);

	public List<UserDevice> getUserDevicesForUser(long userId, UserDeviceStatus active);

	public UserOTP getUserOTPByUserId(long userId);

	public List<UserDevice> getUsersDeviceById(long id);

	public List<UserProfile> getUsersProfileById(long id);

	public List<UserQuestionProfile> getUsersQuestionProfileById(long id);

	public List<UserQuizPerformance> getUsersQuizPerformanceById(long id);

	public List<UserQuizProfile> getUsersQuizProfileById(long id);

	public boolean hardDeleteUser(User email);

	public boolean hardDeleteUser(User email, Session session);

	public boolean hardDeleteUsersDevice(UserDevice email);

	public boolean hardDeleteUsersDevice(UserDevice email, Session session);

	public boolean hardDeleteUsersProfile(UserProfile email);

	public boolean hardDeleteUsersProfile(UserProfile email, Session session);

	public boolean hardDeleteUsersQuestionProfile(UserQuestionProfile email);

	public boolean hardDeleteUsersQuestionProfile(UserQuestionProfile email, Session session);

	public boolean hardDeleteUsersQuizPerformance(UserQuizPerformance email);

	public boolean hardDeleteUsersQuizPerformance(UserQuizPerformance email, Session session);

	public boolean hardDeleteUsersQuizProfile(UserQuizProfile email);

	public boolean hardDeleteUsersQuizProfile(UserQuizProfile email, Session session);

	public boolean softDeleteUser(User email);

	public boolean softDeleteUser(User email, Session session);

	public boolean softDeleteUsersDevice(UserDevice email);

	public boolean softDeleteUsersDevice(UserDevice email, Session session);

	public boolean softDeleteUsersProfile(UserProfile email);

	public boolean softDeleteUsersProfile(UserProfile email, Session session);

	public boolean softDeleteUsersQuestionProfile(UserQuestionProfile email);

	public boolean softDeleteUsersQuestionProfile(UserQuestionProfile email, Session session);

	public boolean softDeleteUsersQuizPerformance(UserQuizPerformance email);

	public boolean softDeleteUsersQuizPerformance(UserQuizPerformance email, Session session);

	public boolean softDeleteUsersQuizProfile(UserQuizProfile email);

	public boolean softDeleteUsersQuizProfile(UserQuizProfile email, Session session);

	public boolean updateUser(User email);

	public boolean updateUser(User email, Session session);

	public boolean updateUsersDevice(UserDevice email);

	public boolean updateUsersDevice(UserDevice email, Session session);

	public boolean updateUsersOtp(UserOTP usersOtp);

	public boolean updateUsersOtp(UserOTP usersOtp, Session session);

	public boolean updateUsersProfile(UserProfile email);

	public boolean updateUsersProfile(UserProfile email, Session session);

	public boolean updateUsersQuestionProfile(UserQuestionProfile email);

	public boolean updateUsersQuestionProfile(UserQuestionProfile email, Session session);

	public boolean updateUsersQuizPerformance(UserQuizPerformance email);

	public boolean updateUsersQuizPerformance(UserQuizPerformance email, Session session);

	public boolean updateUsersQuizProfile(UserQuizProfile email);

	public boolean updateUsersQuizProfile(UserQuizProfile email, Session session);

}
