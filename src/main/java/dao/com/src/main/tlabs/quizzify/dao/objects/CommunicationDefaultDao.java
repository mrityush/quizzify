package com.src.main.tlabs.quizzify.dao.objects;

import java.util.ArrayList;
import java.util.List;

import layer1.layer2.hibernate.HibernateUtil;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.src.main.tlabs.quizzify.dao.manager.CommunicationDao;
import com.src.main.tlabs.quizzify.db.communications.CommunicationLink;
import com.src.main.tlabs.quizzify.db.communications.ExternalCommunicationLink;
import com.src.main.tlabs.quizzify.db.statusfields.CommunicationLinkStatus;

//@SuppressWarnings("unchecked")
public class CommunicationDefaultDao implements CommunicationDao {
	private static Logger logger = LoggerFactory.getLogger(CommunicationDefaultDao.class);

	@Override
	public boolean addCommunicationLink(CommunicationLink communicationLink) {
		return addCommunicationLink(communicationLink, null);
	}

	@Override
	public boolean addCommunicationLink(CommunicationLink communicationLink, Session session) {
		logger.debug("Adding Communication Link : {}", communicationLink);
		Session localSession = (session == null) ? HibernateUtil.getCommunicationsSessionFactory().openSession()
				: session;
		boolean success = false;
		success = addItem(communicationLink, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding Communication Link : {}", success, communicationLink);
		return success;
	}

	@Override
	public boolean addExternalCommunicationLink(ExternalCommunicationLink communicationLink) {
		return addExternalCommunicationLink(communicationLink, null);
	}

	@Override
	public boolean addExternalCommunicationLink(ExternalCommunicationLink communicationLink, Session session) {
		logger.debug("Adding External Communication Link : {}", communicationLink);
		Session localSession = (session == null) ? HibernateUtil.getCommunicationsSessionFactory().openSession()
				: session;
		boolean success = false;
		success = addItem(communicationLink, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding External Communication Link : {}", success, communicationLink);
		return success;
	}

	@Override
	public List<CommunicationLink> getCommunicationLinkById(long id) {
		List<CommunicationLink> commLinks = new ArrayList<CommunicationLink>();
		Session session = HibernateUtil.getCommunicationsSessionFactory().openSession();
		commLinks = getItemsListById(CommunicationLink.class, id, session);
		return commLinks;
	}

	@Override
	public List<ExternalCommunicationLink> getExternalCommunicationLinkById(long id) {
		List<ExternalCommunicationLink> commLinks = new ArrayList<ExternalCommunicationLink>();
		Session session = HibernateUtil.getCommunicationsSessionFactory().openSession();
		commLinks = getItemsListById(ExternalCommunicationLink.class, id, session);
		return commLinks;
	}

	@Override
	public ExternalCommunicationLink getExternalCommunicationLinkByRequestId(String messageId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hardDeleteCommunicationLink(CommunicationLink commLink) {
		return hardDeleteCommunicationLink(commLink, null);
	}

	@Override
	public boolean hardDeleteCommunicationLink(CommunicationLink commLink, Session session) {
		logger.debug("Deleting Communication Link : {}", commLink);
		Session localSession = (session == null) ? HibernateUtil.getCommunicationsSessionFactory().openSession()
				: session;
		boolean success = false;
		success = deleteItem(commLink, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting Communication Link : {}", success, commLink);
		return success;
	}

	@Override
	public boolean hardDeleteExternalCommunicationLink(ExternalCommunicationLink commLink) {
		return hardDeleteExternalCommunicationLink(commLink, null);
	}

	@Override
	public boolean hardDeleteExternalCommunicationLink(ExternalCommunicationLink commLink, Session session) {
		logger.debug("Deleting External Communication Link : {}", commLink);
		Session localSession = (session == null) ? HibernateUtil.getCommunicationsSessionFactory().openSession()
				: session;
		boolean success = false;
		success = deleteItem(commLink, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting External Communication Link : {}", success, commLink);
		return success;
	}

	@Override
	public boolean softDeleteCommunicationLink(CommunicationLink commLink) {
		return softDeleteCommunicationLink(commLink, null);
	}

	@Override
	public boolean softDeleteCommunicationLink(CommunicationLink commLink, Session session) {
		logger.debug("Soft Deleting  Communication Link : {}", commLink);
		Session localSession = (session == null) ? HibernateUtil.getCommunicationsSessionFactory().openSession()
				: session;
		boolean success = false;
		commLink.setStatus(CommunicationLinkStatus.INACTIVE);
		success = updateItem(commLink, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  Communication Link : {}", success, commLink);
		return success;
	}

	@Override
	public boolean softDeleteExternalCommunicationLink(ExternalCommunicationLink commLink) {
		return softDeleteExternalCommunicationLink(commLink, null);
	}

	@Override
	public boolean softDeleteExternalCommunicationLink(ExternalCommunicationLink commLink, Session session) {
		logger.debug("Soft Deleting External Communication Link : {}", commLink);
		Session localSession = (session == null) ? HibernateUtil.getCommunicationsSessionFactory().openSession()
				: session;
		boolean success = false;
		commLink.setStatus(CommunicationLinkStatus.INACTIVE);
		success = updateItem(commLink, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft deleting External Communication Link : {}", success, commLink);
		return success;
	}

	@Override
	public boolean updateCommunicationLink(CommunicationLink commLink) {
		return updateCommunicationLink(commLink, null);
	}

	@Override
	public boolean updateCommunicationLink(CommunicationLink commLink, Session session) {
		logger.debug("Updating Communication Link : {}", commLink);
		Session localSession = (session == null) ? HibernateUtil.getCommunicationsSessionFactory().openSession()
				: session;
		boolean success = false;
		success = updateItem(commLink, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in Updating Communication Link : {}", success, commLink);
		return success;
	}

	@Override
	public boolean updateExternalCommunicationLink(ExternalCommunicationLink commLink) {
		return updateExternalCommunicationLink(commLink, null);
	}

	@Override
	public boolean updateExternalCommunicationLink(ExternalCommunicationLink commLink, Session session) {
		logger.debug("Updating External Communication Link : {}", commLink);
		Session localSession = (session == null) ? HibernateUtil.getCommunicationsSessionFactory().openSession()
				: session;
		boolean success = false;
		success = updateItem(commLink, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in Updating External Communication Link : {}", success, commLink);
		return success;
	}
}
