package com.src.main.tlabs.quizzify.dao.objects;

import java.util.ArrayList;
import java.util.List;

import layer1.layer2.hibernate.HibernateUtil;

import org.hibernate.Session;

import com.src.main.tlabs.quizzify.dao.manager.EmailsDao;
import com.src.main.tlabs.quizzify.db.emails.Email;
import com.src.main.tlabs.quizzify.db.emails.Template;
import com.src.main.tlabs.quizzify.db.statusfields.EmailStatus;
import com.src.main.tlabs.quizzify.db.statusfields.TemplateStatus;

public class EmailsDefaultDao implements EmailsDao {

	@Override
	public boolean addEmail(Email email) {
		return addEmail(email, null);
	}

	@Override
	public boolean addEmail(Email email, Session session) {
		logger.debug("Adding Email : {}", email);
		Session localSession = (session == null) ? HibernateUtil.getEmailsSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(email, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding Email : {}", success, email);
		return success;
	}

	@Override
	public boolean addTemplate(Template template) {
		return addTemplate(template, null);
	}

	@Override
	public boolean addTemplate(Template template, Session session) {
		logger.debug("Adding Template : {}", template);
		Session localSession = (session == null) ? HibernateUtil.getEmailsSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(template, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding Template : {}", success, template);
		return success;
	}

	@Override
	public List<Email> getEmailById(long id) {
		List<Email> emails = new ArrayList<Email>();
		Session session = HibernateUtil.getEmailsSessionFactory().openSession();
		emails = getItemsListById(Email.class, id, session);
		return emails;
	}

	@Override
	public List<Template> getTemplateById(long id) {
		List<Template> templates = new ArrayList<Template>();
		Session session = HibernateUtil.getEmailsSessionFactory().openSession();
		templates = getItemsListById(Template.class, id, session);
		return templates;
	}

	@Override
	public boolean hardDeleteEmail(Email email) {
		return hardDeleteEmail(email, null);
	}

	@Override
	public boolean hardDeleteEmail(Email email, Session session) {
		logger.debug("Deleting Email : {}", email);
		Session localSession = (session == null) ? HibernateUtil.getEmailsSessionFactory().openSession() : session;
		boolean success = false;
		success = deleteItem(email, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting Email : {}", success, email);
		return success;
	}

	@Override
	public boolean hardDeleteTemplate(Template template) {
		return hardDeleteTemplate(template, null);
	}

	@Override
	public boolean hardDeleteTemplate(Template template, Session session) {
		logger.debug("Deleting Template : {}", template);
		Session localSession = (session == null) ? HibernateUtil.getEmailsSessionFactory().openSession() : session;
		boolean success = false;
		success = deleteItem(template, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting Template : {}", success, template);
		return success;
	}

	@Override
	public boolean softDeleteEmail(Email email) {
		return softDeleteEmail(email, null);
	}

	@Override
	public boolean softDeleteEmail(Email email, Session session) {
		logger.debug("Soft Deleting  Email : {}", email);
		Session localSession = (session == null) ? HibernateUtil.getCommunicationsSessionFactory().openSession()
				: session;
		boolean success = false;
		email.setStatus(EmailStatus.INACTIVE);
		success = updateItem(email, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  Email : {}", success, email);
		return success;
	}

	@Override
	public boolean softDeleteTemplate(Template template) {
		return softDeleteTemplate(template, null);
	}

	@Override
	public boolean softDeleteTemplate(Template template, Session session) {
		logger.debug("Soft Deleting  Template : {}", template);
		Session localSession = (session == null) ? HibernateUtil.getCommunicationsSessionFactory().openSession()
				: session;
		boolean success = false;
		template.setStatus(TemplateStatus.INACTIVE);
		success = updateItem(template, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  Template : {}", success, template);
		return success;
	}

	@Override
	public boolean updateEmail(Email email) {
		return updateEmail(email, null);
	}

	@Override
	public boolean updateEmail(Email email, Session session) {
		logger.debug("Updating  Email : {}", email);
		Session localSession = (session == null) ? HibernateUtil.getCommunicationsSessionFactory().openSession()
				: session;
		boolean success = false;
		success = updateItem(email, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in updating  Email : {}", success, email);
		return success;
	}

	@Override
	public boolean updateTemplate(Template template) {
		return updateTemplate(template, null);
	}

	@Override
	public boolean updateTemplate(Template template, Session session) {
		logger.debug("Updating  Template : {}", template);
		Session localSession = (session == null) ? HibernateUtil.getCommunicationsSessionFactory().openSession()
				: session;
		boolean success = false;
		success = updateItem(template, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in updating Template : {}", success, template);
		return success;
	}

}
