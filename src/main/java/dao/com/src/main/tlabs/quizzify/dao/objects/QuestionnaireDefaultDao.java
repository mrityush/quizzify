package com.src.main.tlabs.quizzify.dao.objects;

import java.util.ArrayList;
import java.util.List;

import layer1.layer2.hibernate.HibernateUtil;

import org.hibernate.Session;

import com.src.main.tlabs.quizzify.dao.manager.QuestionnaireDao;
import com.src.main.tlabs.quizzify.db.questionnaires.AnswerSet;
import com.src.main.tlabs.quizzify.db.questionnaires.Category;
import com.src.main.tlabs.quizzify.db.questionnaires.Question;
import com.src.main.tlabs.quizzify.db.questionnaires.QuizSet;
import com.src.main.tlabs.quizzify.db.questionnaires.Quiz;
import com.src.main.tlabs.quizzify.db.questionnaires.Set;
import com.src.main.tlabs.quizzify.db.statusfields.AnswerStatus;
import com.src.main.tlabs.quizzify.db.statusfields.CategoryStatus;
import com.src.main.tlabs.quizzify.db.statusfields.QuizStatus;

public class QuestionnaireDefaultDao implements QuestionnaireDao {

	@Override
	public boolean addAnswerSet(AnswerSet answerSet) {
		return addAnswerSet(answerSet, null);
	}

	@Override
	public boolean addAnswerSet(AnswerSet answerSet, Session session) {
		logger.debug("Adding Answer Set : {}", answerSet);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = addItem(answerSet, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding Answer Set : {}", success, answerSet);
		return success;
	}

	@Override
	public boolean addCategory(Category category) {
		return addCategory(category, null);
	}

	@Override
	public boolean addCategory(Category category, Session session) {
		logger.debug("Adding Category : {}", category);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = addItem(category, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding Category : {}", success, category);
		return success;
	}

	@Override
	public boolean addQuestion(Question question) {
		return addQuestion(question, null);
	}

	@Override
	public boolean addQuestion(Question question, Session session) {
		logger.debug("Adding Question : {}", question);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = addItem(question, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding Question : {}", success, question);
		return success;
	}

	@Override
	public boolean addQuiz(Quiz quiz) {
		return addQuiz(quiz, null);
	}

	@Override
	public boolean addQuiz(Quiz quiz, Session session) {
		logger.debug("Adding Quiz : {}", quiz);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = addItem(quiz, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding Quiz : {}", success, quiz);
		return success;
	}

	@Override
	public boolean addQuizSet(QuizSet quizSet) {
		return addQuizSet(quizSet, null);
	}

	@Override
	public boolean addQuizSet(QuizSet quizSet, Session session) {
		logger.debug("Adding QuizSet : {}", quizSet);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = addItem(quizSet, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding QuizSet : {}", success, quizSet);
		return success;
	}

	@Override
	public boolean addSet(Set set) {
		return addSet(set, null);
	}

	@Override
	public boolean addSet(Set set, Session session) {
		logger.debug("Adding Set : {}", set);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = addItem(set, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding Set : {}", success, set);
		return success;
	}

	@Override
	public List<AnswerSet> getAnswerSetById(long id) {
		List<AnswerSet> answerSets = new ArrayList<AnswerSet>();
		Session session = HibernateUtil.getQuestionnairesSessionFactory().openSession();
		answerSets = getItemsListById(AnswerSet.class, id, session);
		return answerSets;
	}

	@Override
	public List<Category> getCategoryById(long id) {
		List<Category> categories = new ArrayList<Category>();
		Session session = HibernateUtil.getQuestionnairesSessionFactory().openSession();
		categories = getItemsListById(Category.class, id, session);
		return categories;
	}

	@Override
	public List<Question> getQuestionById(long id) {
		List<Question> questions = new ArrayList<Question>();
		Session session = HibernateUtil.getQuestionnairesSessionFactory().openSession();
		questions = getItemsListById(Question.class, id, session);
		return questions;
	}

	@Override
	public List<Quiz> getQuizById(long id) {
		List<Quiz> quizzes = new ArrayList<Quiz>();
		Session session = HibernateUtil.getQuestionnairesSessionFactory().openSession();
		quizzes = getItemsListById(Quiz.class, id, session);
		return quizzes;
	}

	@Override
	public List<QuizSet> getQuizSetById(long id) {
		List<QuizSet> quizSets = new ArrayList<QuizSet>();
		Session session = HibernateUtil.getQuestionnairesSessionFactory().openSession();
		quizSets = getItemsListById(QuizSet.class, id, session);
		return quizSets;
	}

	@Override
	public List<Set> getSetById(long id) {
		List<Set> sets = new ArrayList<Set>();
		Session session = HibernateUtil.getQuestionnairesSessionFactory().openSession();
		sets = getItemsListById(Set.class, id, session);
		return sets;
	}

	@Override
	public boolean hardDeleteAnswerSet(AnswerSet answerSet) {
		return hardDeleteAnswerSet(answerSet, null);
	}

	@Override
	public boolean hardDeleteAnswerSet(AnswerSet answerSet, Session session) {
		logger.debug("Deleting Answer Set : {}", answerSet);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = deleteItem(answerSet, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting Answer Set : {}", success, answerSet);
		return success;
	}

	@Override
	public boolean hardDeleteCategory(Category category) {
		return hardDeleteCategory(category, null);
	}

	@Override
	public boolean hardDeleteCategory(Category category, Session session) {
		logger.debug("Deleting category : {}", category);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = deleteItem(category, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting category : {}", success, category);
		return success;
	}

	@Override
	public boolean hardDeleteQuestion(Question question) {
		return hardDeleteQuestion(question, null);
	}

	@Override
	public boolean hardDeleteQuestion(Question question, Session session) {
		logger.debug("Deleting question : {}", question);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = deleteItem(question, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting question : {}", success, question);
		return success;
	}

	@Override
	public boolean hardDeleteQuiz(Quiz quiz) {
		return hardDeleteQuiz(quiz, null);
	}

	@Override
	public boolean hardDeleteQuiz(Quiz quiz, Session session) {
		logger.debug("Deleting quiz : {}", quiz);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = deleteItem(quiz, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting quiz : {}", success, quiz);
		return success;
	}

	@Override
	public boolean hardDeleteQuizSet(QuizSet quizSet) {
		return hardDeleteQuizSet(quizSet, null);
	}

	@Override
	public boolean hardDeleteQuizSet(QuizSet quizSet, Session session) {
		logger.debug("Deleting quizSet : {}", quizSet);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = deleteItem(quizSet, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting quizSet : {}", success, quizSet);
		return success;
	}

	@Override
	public boolean hardDeleteSet(Set set) {
		return hardDeleteSet(set, null);
	}

	@Override
	public boolean hardDeleteSet(Set set, Session session) {
		logger.debug("Deleting set : {}", set);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = deleteItem(set, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting set : {}", success, set);
		return success;
	}

	@Override
	public boolean softDeleteAnswerSet(AnswerSet answerSet) {
		return softDeleteAnswerSet(answerSet, null);
	}

	@Override
	public boolean softDeleteAnswerSet(AnswerSet answerSet, Session session) {
		logger.debug("Soft Deleting  Answer Set : {}", answerSet);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		answerSet.setStatus(AnswerStatus.INACTIVE);
		success = updateItem(answerSet, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  Answer Set : {}", success, answerSet);
		return success;
	}

	@Override
	public boolean softDeleteCategory(Category category) {
		return softDeleteCategory(category, null);
	}

	@Override
	public boolean softDeleteCategory(Category category, Session session) {
		logger.debug("Soft Deleting  category : {}", category);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		category.setStatus(CategoryStatus.INACTIVE);
		success = updateItem(category, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  category : {}", success, category);
		return success;
	}

	@Override
	public boolean softDeleteQuestion(Question question) {
		return softDeleteQuestion(question, null);
	}

	@Override
	public boolean softDeleteQuestion(Question question, Session session) {
		logger.debug("Soft Deleting  question : {}", question);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		question.setStatus(CategoryStatus.INACTIVE);
		success = updateItem(question, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  question : {}", success, question);
		return success;
	}

	@Override
	public boolean softDeleteQuiz(Quiz quiz) {
		return softDeleteQuiz(quiz, null);
	}

	@Override
	public boolean softDeleteQuiz(Quiz quiz, Session session) {
		logger.debug("Soft Deleting  quiz : {}", quiz);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		quiz.setStatus(QuizStatus.INACTIVE);
		success = updateItem(quiz, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  quiz : {}", success, quiz);
		return success;
	}

	@Override
	public boolean softDeleteQuizSet(QuizSet quizSet) {
		return softDeleteQuizSet(quizSet, null);
	}

	@Override
	public boolean softDeleteQuizSet(QuizSet quizSet, Session session) {
		logger.debug("Soft Deleting  quizSet : {}", quizSet);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		quizSet.setStatus(QuizStatus.INACTIVE);
		success = updateItem(quizSet, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  quizSet : {}", success, quizSet);
		return success;
	}

	@Override
	public boolean softDeleteSet(Set set) {
		return softDeleteSet(set, null);
	}

	@Override
	public boolean softDeleteSet(Set set, Session session) {
		logger.debug("Soft Deleting  set : {}", set);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		set.setStatus(QuizStatus.INACTIVE);
		success = updateItem(set, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  set : {}", success, set);
		return success;
	}

	@Override
	public boolean updateAnswerSet(AnswerSet answerSet) {
		return updateAnswerSet(answerSet, null);
	}

	@Override
	public boolean updateAnswerSet(AnswerSet answerSet, Session session) {
		logger.debug("Updating  answerSet : {}", answerSet);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = updateItem(answerSet, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in updating  answerSet : {}", success, answerSet);
		return success;
	}

	@Override
	public boolean updateCategory(Category category) {
		return updateCategory(category, null);
	}

	@Override
	public boolean updateCategory(Category category, Session session) {
		logger.debug("Updating  category : {}", category);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = updateItem(category, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in updating  category : {}", success, category);
		return success;
	}

	@Override
	public boolean updateQuestion(Question question) {
		return updateQuestion(question, null);
	}

	@Override
	public boolean updateQuestion(Question question, Session session) {
		logger.debug("Updating  question : {}", question);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = updateItem(question, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in updating  question : {}", success, question);
		return success;
	}

	@Override
	public boolean updateQuiz(Quiz quiz) {
		return updateQuiz(quiz, null);
	}

	@Override
	public boolean updateQuiz(Quiz quiz, Session session) {
		logger.debug("Updating  quiz : {}", quiz);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = updateItem(quiz, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in updating  quiz : {}", success, quiz);
		return success;
	}

	@Override
	public boolean updateQuizSet(QuizSet quizSet) {
		return updateQuizSet(quizSet, null);
	}

	@Override
	public boolean updateQuizSet(QuizSet quizSet, Session session) {
		logger.debug("Updating  quizSet : {}", quizSet);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = updateItem(quizSet, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in updating  quizSet : {}", success, quizSet);
		return success;
	}

	@Override
	public boolean updateSet(Set set) {
		return updateSet(set, null);
	}

	@Override
	public boolean updateSet(Set set, Session session) {
		logger.debug("Updating  set : {}", set);
		Session localSession = (session == null) ? HibernateUtil.getQuestionnairesSessionFactory().openSession()
				: session;
		boolean success = false;
		success = updateItem(set, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in updating  set : {}", success, set);
		return success;
	}

}
