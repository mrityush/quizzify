package com.src.main.tlabs.quizzify.dao.objects;

import java.util.ArrayList;
import java.util.List;

import layer1.layer2.hibernate.HibernateUtil;

import org.hibernate.Session;

import com.src.main.tlabs.quizzify.dao.manager.RewardDao;
import com.src.main.tlabs.quizzify.db.rewards.Reward;
import com.src.main.tlabs.quizzify.db.rewards.RewardsStat;
import com.src.main.tlabs.quizzify.db.statusfields.RewardStatus;

public class RewardDefaultDao implements RewardDao {

	@Override
	public boolean addReward(Reward reward) {
		return addReward(reward, null);
	}

	@Override
	public boolean addReward(Reward reward, Session session) {
		logger.debug("Adding reward : {}", reward);
		Session localSession = (session == null) ? HibernateUtil.getRewardsSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(reward, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding reward : {}", success, reward);
		return success;
	}

	@Override
	public boolean addRewardsStat(RewardsStat rewardStat) {
		return addRewardsStat(rewardStat, null);
	}

	@Override
	public boolean addRewardsStat(RewardsStat rewardStat, Session session) {
		logger.debug("Adding rewardStat : {}", rewardStat);
		Session localSession = (session == null) ? HibernateUtil.getRewardsSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(rewardStat, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding rewardStat : {}", success, rewardStat);
		return success;
	}

	@Override
	public List<Reward> getRewardById(long id) {
		List<Reward> rewards = new ArrayList<Reward>();
		Session session = HibernateUtil.getRewardsSessionFactory().openSession();
		rewards = getItemsListById(Reward.class, id, session);
		return rewards;
	}

	@Override
	public List<RewardsStat> getRewardsStatById(long id) {
		List<RewardsStat> rewardsStats = new ArrayList<RewardsStat>();
		Session session = HibernateUtil.getRewardsSessionFactory().openSession();
		rewardsStats = getItemsListById(RewardsStat.class, id, session);
		return rewardsStats;
	}

	@Override
	public boolean hardDeleteReward(Reward reward) {
		return hardDeleteReward(reward, null);
	}

	@Override
	public boolean hardDeleteReward(Reward reward, Session session) {
		logger.debug("Deleting hard reward : {}", reward);
		Session localSession = (session == null) ? HibernateUtil.getRewardsSessionFactory().openSession() : session;
		boolean success = false;
		success = deleteItem(reward, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in hard deleting reward : {}", success, reward);
		return success;
	}

	@Override
	public boolean hardDeleteRewardsStat(RewardsStat rewardStat) {
		return hardDeleteRewardsStat(rewardStat, null);
	}

	@Override
	public boolean hardDeleteRewardsStat(RewardsStat rewardStat, Session session) {
		logger.debug("Deleting hard reward Stat : {}", rewardStat);
		Session localSession = (session == null) ? HibernateUtil.getRewardsSessionFactory().openSession() : session;
		boolean success = false;
		success = deleteItem(rewardStat, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in hard deleting reward Stat: {}", success, rewardStat);
		return success;
	}

	@Override
	public boolean softDeleteReward(Reward reward) {
		return softDeleteReward(reward, null);
	}

	@Override
	public boolean softDeleteReward(Reward reward, Session session) {
		logger.debug("Soft Deleting  reward : {}", reward);
		Session localSession = (session == null) ? HibernateUtil.getRewardsSessionFactory().openSession() : session;
		boolean success = false;
		reward.setStatus(RewardStatus.INACTIVE);
		success = updateItem(reward, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  reward : {}", success, reward);
		return success;
	}

	@Override
	public boolean softDeleteRewardsStat(RewardsStat rewardStat) {
		return softDeleteRewardsStat(rewardStat, null);
	}

	@Override
	public boolean softDeleteRewardsStat(RewardsStat rewardStat, Session session) {
		logger.debug("Soft Deleting  reward Stat : {}", rewardStat);
		Session localSession = (session == null) ? HibernateUtil.getRewardsSessionFactory().openSession() : session;
		boolean success = false;
		rewardStat.setStatus(RewardStatus.INACTIVE);
		success = updateItem(rewardStat, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  reward Stat: {}", success, rewardStat);
		return success;
	}

	@Override
	public boolean updateReward(Reward reward) {
		return updateReward(reward, null);
	}

	@Override
	public boolean updateReward(Reward reward, Session session) {
		logger.debug("Updating reward : {}", reward);
		Session localSession = (session == null) ? HibernateUtil.getRewardsSessionFactory().openSession() : session;
		boolean success = false;
		success = updateItem(reward, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in Updating reward : {}", success, reward);
		return success;
	}

	@Override
	public boolean updateRewardsStat(RewardsStat rewardStat) {
		return updateRewardsStat(rewardStat, null);
	}

	@Override
	public boolean updateRewardsStat(RewardsStat rewardStat, Session session) {
		logger.debug("Updating reward Stat: {}", rewardStat);
		Session localSession = (session == null) ? HibernateUtil.getRewardsSessionFactory().openSession() : session;
		boolean success = false;
		success = updateItem(rewardStat, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in Updating reward Stat: {}", success, rewardStat);
		return success;
	}

}
