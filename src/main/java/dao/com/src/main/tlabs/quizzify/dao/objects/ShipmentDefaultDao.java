package com.src.main.tlabs.quizzify.dao.objects;

import java.util.ArrayList;
import java.util.List;

import layer1.layer2.hibernate.HibernateUtil;

import org.hibernate.Session;

import com.src.main.tlabs.quizzify.dao.manager.ShipmentDao;
import com.src.main.tlabs.quizzify.db.shipments.Courier;
import com.src.main.tlabs.quizzify.db.shipments.Shipment;
import com.src.main.tlabs.quizzify.db.statusfields.CourierStatus;
import com.src.main.tlabs.quizzify.db.statusfields.ShipmentStatus;

public class ShipmentDefaultDao implements ShipmentDao {

	@Override
	public boolean addCourier(Courier courier) {
		return addCourier(courier, null);
	}

	@Override
	public boolean addCourier(Courier courier, Session session) {
		logger.debug("Adding Courier : {}", courier);
		Session localSession = (session == null) ? HibernateUtil.getShipmentsSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(courier, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding Courier : {}", success, courier);
		return success;
	}

	@Override
	public boolean addShipment(Shipment shipment) {
		return addShipment(shipment, null);
	}

	@Override
	public boolean addShipment(Shipment shipment, Session session) {
		logger.debug("Adding shipment : {}", shipment);
		Session localSession = (session == null) ? HibernateUtil.getShipmentsSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(shipment, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding shipment : {}", success, shipment);
		return success;
	}

	@Override
	public List<Courier> getCourierById(long id) {
		List<Courier> couriers = new ArrayList<Courier>();
		Session session = HibernateUtil.getShipmentsSessionFactory().openSession();
		couriers = getItemsListById(Courier.class, id, session);
		return couriers;
	}

	@Override
	public List<Shipment> getShipmentById(long id) {
		List<Shipment> shipments = new ArrayList<Shipment>();
		Session session = HibernateUtil.getShipmentsSessionFactory().openSession();
		shipments = getItemsListById(Shipment.class, id, session);
		return shipments;
	}

	@Override
	public boolean hardDeleteCourier(Courier courier) {
		return hardDeleteCourier(courier, null);
	}

	@Override
	public boolean hardDeleteCourier(Courier courier, Session session) {
		logger.debug("Deleting courier : {}", courier);
		Session localSession = (session == null) ? HibernateUtil.getShipmentsSessionFactory().openSession() : session;
		boolean success = false;
		success = deleteItem(courier, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting courier : {}", success, courier);
		return success;
	}

	@Override
	public boolean hardDeleteShipment(Shipment shipment) {
		return hardDeleteShipment(shipment, null);
	}

	@Override
	public boolean hardDeleteShipment(Shipment shipment, Session session) {
		logger.debug("Deleting shipment : {}", shipment);
		Session localSession = (session == null) ? HibernateUtil.getShipmentsSessionFactory().openSession() : session;
		boolean success = false;
		success = deleteItem(shipment, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting shipment : {}", success, shipment);
		return success;
	}

	@Override
	public boolean softDeleteCourier(Courier courier) {
		return softDeleteCourier(courier, null);
	}

	@Override
	public boolean softDeleteCourier(Courier courier, Session session) {
		logger.debug("Soft Deleting  courier : {}", courier);
		Session localSession = (session == null) ? HibernateUtil.getShipmentsSessionFactory().openSession() : session;
		boolean success = false;
		courier.setStatus(CourierStatus.INACTIVE);
		success = updateItem(courier, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  courier : {}", success, courier);
		return success;
	}

	@Override
	public boolean softDeleteShipment(Shipment shipment) {
		return softDeleteShipment(shipment, null);
	}

	@Override
	public boolean softDeleteShipment(Shipment shipment, Session session) {
		logger.debug("Soft Deleting  shipment : {}", shipment);
		Session localSession = (session == null) ? HibernateUtil.getShipmentsSessionFactory().openSession() : session;
		boolean success = false;
		shipment.setStatus(ShipmentStatus.INACTIVE);
		success = updateItem(shipment, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  courier : {}", success, shipment);
		return success;
	}

	@Override
	public boolean updateCourier(Courier courier) {
		return updateCourier(courier, null);
	}

	@Override
	public boolean updateCourier(Courier courier, Session session) {
		logger.debug("Updating  courier : {}", courier);
		Session localSession = (session == null) ? HibernateUtil.getShipmentsSessionFactory().openSession() : session;
		boolean success = false;
		success = updateItem(courier, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in updating courier : {}", success, courier);
		return success;
	}

	@Override
	public boolean updateShipment(Shipment shipment) {
		return updateShipment(shipment, null);
	}

	@Override
	public boolean updateShipment(Shipment shipment, Session session) {
		logger.debug("Updating  shipment : {}", shipment);
		Session localSession = (session == null) ? HibernateUtil.getShipmentsSessionFactory().openSession() : session;
		boolean success = false;
		success = updateItem(shipment, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in updating  courier : {}", success, shipment);
		return success;
	}

}
