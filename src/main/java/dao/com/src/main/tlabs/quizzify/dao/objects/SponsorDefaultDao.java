package com.src.main.tlabs.quizzify.dao.objects;

import java.util.ArrayList;
import java.util.List;

import layer1.layer2.hibernate.HibernateUtil;

import org.hibernate.Session;

import com.src.main.tlabs.quizzify.dao.manager.SponsorDao;
import com.src.main.tlabs.quizzify.db.sponsors.Sponsor;
import com.src.main.tlabs.quizzify.db.sponsors.SponsorsStat;
import com.src.main.tlabs.quizzify.db.statusfields.SponsorStatus;

public class SponsorDefaultDao implements SponsorDao {

	@Override
	public boolean addSponsor(Sponsor sponsor) {
		return addSponsor(sponsor, null);
	}

	@Override
	public boolean addSponsor(Sponsor sponsor, Session session) {
		logger.debug("Adding sponsor : {}", sponsor);
		Session localSession = (session == null) ? HibernateUtil.getSponsorsSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(sponsor, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding sponsor : {}", success, sponsor);
		return success;
	}

	@Override
	public boolean addSponsorsStat(SponsorsStat sponsorStat) {
		return addSponsorsStat(sponsorStat, null);
	}

	@Override
	public boolean addSponsorsStat(SponsorsStat sponsorStat, Session session) {
		logger.debug("Adding sponsor stat: {}", sponsorStat);
		Session localSession = (session == null) ? HibernateUtil.getSponsorsSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(sponsorStat, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding sponsor stat: {}", success, sponsorStat);
		return success;
	}

	@Override
	public List<Sponsor> getSponsorById(long id) {
		List<Sponsor> sponsors = new ArrayList<Sponsor>();
		Session session = HibernateUtil.getSponsorsSessionFactory().openSession();
		sponsors = getItemsListById(Sponsor.class, id, session);
		return sponsors;
	}

	@Override
	public List<SponsorsStat> getSponsorsStatById(long id) {
		List<SponsorsStat> sponsorsStats = new ArrayList<SponsorsStat>();
		Session session = HibernateUtil.getSponsorsSessionFactory().openSession();
		sponsorsStats = getItemsListById(SponsorsStat.class, id, session);
		return sponsorsStats;
	}

	@Override
	public boolean hardDeleteSponsor(Sponsor sponsor) {
		return hardDeleteSponsor(sponsor, null);
	}

	@Override
	public boolean hardDeleteSponsor(Sponsor sponsor, Session session) {
		logger.debug("Deleting sponsor : {}", sponsor);
		Session localSession = (session == null) ? HibernateUtil.getSponsorsSessionFactory().openSession() : session;
		boolean success = false;
		success = deleteItem(sponsor, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting sponsor : {}", success, sponsor);
		return success;
	}

	@Override
	public boolean hardDeleteSponsorsStat(SponsorsStat sponsorStat) {
		return hardDeleteSponsorsStat(sponsorStat, null);
	}

	@Override
	public boolean hardDeleteSponsorsStat(SponsorsStat sponsorStat, Session session) {
		logger.debug("Deleting sponsorStat : {}", sponsorStat);
		Session localSession = (session == null) ? HibernateUtil.getSponsorsSessionFactory().openSession() : session;
		boolean success = false;
		success = deleteItem(sponsorStat, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting sponsorStat : {}", success, sponsorStat);
		return success;
	}

	@Override
	public boolean softDeleteSponsor(Sponsor sponsor) {
		return softDeleteSponsor(sponsor, null);
	}

	@Override
	public boolean softDeleteSponsor(Sponsor sponsor, Session session) {
		logger.debug("Soft Deleting  sponsor : {}", sponsor);
		Session localSession = (session == null) ? HibernateUtil.getSponsorsSessionFactory().openSession() : session;
		boolean success = false;
		sponsor.setStatus(SponsorStatus.INACTIVE);
		success = updateItem(sponsor, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  sponsor : {}", success, sponsor);
		return success;
	}

	@Override
	public boolean softDeleteSponsorsStat(SponsorsStat sponsorStat) {
		return softDeleteSponsorsStat(sponsorStat, null);
	}

	@Override
	public boolean softDeleteSponsorsStat(SponsorsStat sponsorStat, Session session) {
		logger.debug("Soft Deleting  sponsorStat : {}", sponsorStat);
		Session localSession = (session == null) ? HibernateUtil.getSponsorsSessionFactory().openSession() : session;
		boolean success = false;
		sponsorStat.setStatus(SponsorStatus.INACTIVE);
		success = updateItem(sponsorStat, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  sponsorStat : {}", success, sponsorStat);
		return success;
	}

	@Override
	public boolean updateSponsor(Sponsor sponsor) {
		return updateSponsor(sponsor, null);
	}

	@Override
	public boolean updateSponsor(Sponsor sponsor, Session session) {
		logger.debug("Soft Deleting  sponsor : {}", sponsor);
		Session localSession = (session == null) ? HibernateUtil.getSponsorsSessionFactory().openSession() : session;
		boolean success = false;
		success = updateItem(sponsor, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  sponsor : {}", success, sponsor);
		return success;
	}

	@Override
	public boolean updateSponsorsStat(SponsorsStat sponsorStat) {
		return updateSponsorsStat(sponsorStat, null);
	}

	@Override
	public boolean updateSponsorsStat(SponsorsStat sponsorStat, Session session) {
		logger.debug("Soft Deleting  sponsorStat : {}", sponsorStat);
		Session localSession = (session == null) ? HibernateUtil.getSponsorsSessionFactory().openSession() : session;
		boolean success = false;
		success = updateItem(sponsorStat, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  sponsorStat : {}", success, sponsorStat);
		return success;
	}

}
