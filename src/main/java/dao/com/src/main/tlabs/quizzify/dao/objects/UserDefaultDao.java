package com.src.main.tlabs.quizzify.dao.objects;

import java.util.ArrayList;
import java.util.List;

import layer1.layer2.hibernate.HibernateUtil;

import org.hibernate.Session;

import com.src.main.tlabs.quizzify.dao.manager.UserDao;
import com.src.main.tlabs.quizzify.db.statusfields.UserDeviceStatus;
import com.src.main.tlabs.quizzify.db.statusfields.UserStatus;
import com.src.main.tlabs.quizzify.db.users.User;
import com.src.main.tlabs.quizzify.db.users.UserDevice;
import com.src.main.tlabs.quizzify.db.users.UserOTP;
import com.src.main.tlabs.quizzify.db.users.UserProfile;
import com.src.main.tlabs.quizzify.db.users.UserQuestionProfile;
import com.src.main.tlabs.quizzify.db.users.UserQuizPerformance;
import com.src.main.tlabs.quizzify.db.users.UserQuizProfile;

public class UserDefaultDao implements UserDao {

	@Override
	public boolean addUser(User user) {
		return addUser(user, null);
	}

	@Override
	public boolean addUser(User user, Session session) {
		logger.debug("Adding user : {}", user);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(user, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding user : {}", success, user);
		return success;
	}

	@Override
	public boolean addUsersDevice(UserDevice userDevice) {
		return addUsersDevice(userDevice, null);
	}

	@Override
	public boolean addUsersDevice(UserDevice userDevice, Session session) {
		logger.debug("Adding userDevice : {}", userDevice);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(userDevice, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding userDevice : {}", success, userDevice);
		return success;
	}

	@Override
	public boolean addUsersOtp(UserOTP profile) {
		return addUsersOtp(profile, null);
	}

	@Override
	public boolean addUsersOtp(UserOTP profile, Session session) {
		logger.debug("Adding user profile : {}", profile);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(profile, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding user profile : {}", success, profile);
		return success;
	}

	@Override
	public boolean addUsersProfile(UserProfile profile) {
		return addUsersProfile(profile, null);
	}

	@Override
	public boolean addUsersProfile(UserProfile profile, Session session) {
		logger.debug("Adding user profile : {}", profile);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(profile, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding user profile : {}", success, profile);
		return success;
	}

	@Override
	public boolean addUsersQuestionProfile(UserQuestionProfile questionProfile) {
		return addUsersQuestionProfile(questionProfile, null);
	}

	@Override
	public boolean addUsersQuestionProfile(UserQuestionProfile questionProfile, Session session) {
		logger.debug("Adding user questionProfile : {}", questionProfile);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(questionProfile, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding user questionProfile : {}", success, questionProfile);
		return success;
	}

	@Override
	public boolean addUsersQuizPerformance(UserQuizPerformance performance) {
		return addUsersQuizPerformance(performance, null);
	}

	@Override
	public boolean addUsersQuizPerformance(UserQuizPerformance performance, Session session) {
		logger.debug("Adding user quiz performance : {}", performance);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(performance, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding user quiz performance : {}", success, performance);
		return success;
	}

	@Override
	public boolean addUsersQuizProfile(UserQuizProfile quizProfile) {
		return addUsersQuizProfile(quizProfile, null);
	}

	@Override
	public boolean addUsersQuizProfile(UserQuizProfile quizProfile, Session session) {
		logger.debug("Adding user quiz Profile : {}", quizProfile);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = addItem(quizProfile, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in adding user quiz Profile : {}", success, quizProfile);
		return success;
	}

	@Override
	public User getUserByEmail(String email) {
		List<User> users = new ArrayList<User>();
		Session session = HibernateUtil.getUsersSessionFactory().openSession();
		users = getExactMatchingItemByProperty(User.class, "email", email, session);
		if (users.size() > 0) {
			return users.get(0);
		}
		return null;
	}

	@Override
	public List<User> getUserById(long id) {
		List<User> users = new ArrayList<User>();
		Session session = HibernateUtil.getUsersSessionFactory().openSession();
		users = getItemsListById(User.class, id, session);
		return users;
	}

	@Override
	public List<UserDevice> getUserDevicesForUser(long userId, UserDeviceStatus active) {
		List<UserDevice> userDevices = new ArrayList<UserDevice>();
		Session session = HibernateUtil.getUsersSessionFactory().openSession();
		userDevices = getNumberExactMatchFetchResults(UserDevice.class, "userId", userId, session);
		return userDevices;
	}

	@Override
	public UserOTP getUserOTPByUserId(long userId) {
		List<UserOTP> users = new ArrayList<UserOTP>();
		Session session = HibernateUtil.getUsersSessionFactory().openSession();
		users = getNumberExactMatchFetchResults(UserOTP.class, "userId", userId, session);
		if (users.size() > 0) {
			return users.get(0);
		}
		return null;
	}

	@Override
	public List<UserDevice> getUsersDeviceById(long id) {
		List<UserDevice> usersDevice = new ArrayList<UserDevice>();
		Session session = HibernateUtil.getUsersSessionFactory().openSession();
		usersDevice = getItemsListById(UserDevice.class, id, session);
		return usersDevice;
	}

	@Override
	public List<UserProfile> getUsersProfileById(long id) {
		List<UserProfile> usersProfile = new ArrayList<UserProfile>();
		Session session = HibernateUtil.getUsersSessionFactory().openSession();
		usersProfile = getItemsListById(UserProfile.class, id, session);
		return usersProfile;
	}

	@Override
	public List<UserQuestionProfile> getUsersQuestionProfileById(long id) {
		List<UserQuestionProfile> usersQuestionProfile = new ArrayList<UserQuestionProfile>();
		Session session = HibernateUtil.getUsersSessionFactory().openSession();
		usersQuestionProfile = getItemsListById(UserQuestionProfile.class, id, session);
		return usersQuestionProfile;
	}

	@Override
	public List<UserQuizPerformance> getUsersQuizPerformanceById(long id) {
		List<UserQuizPerformance> usersQuizPerformance = new ArrayList<UserQuizPerformance>();
		Session session = HibernateUtil.getUsersSessionFactory().openSession();
		usersQuizPerformance = getItemsListById(UserQuizPerformance.class, id, session);
		return usersQuizPerformance;
	}

	@Override
	public List<UserQuizProfile> getUsersQuizProfileById(long id) {
		List<UserQuizProfile> usersQuizProfile = new ArrayList<UserQuizProfile>();
		Session session = HibernateUtil.getUsersSessionFactory().openSession();
		usersQuizProfile = getItemsListById(UserQuizProfile.class, id, session);
		return usersQuizProfile;
	}

	@Override
	public boolean hardDeleteUser(User user) {
		return hardDeleteUser(user, null);
	}

	@Override
	public boolean hardDeleteUser(User user, Session session) {
		logger.debug("Deleting User : {}", user);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = deleteItem(user, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting User : {}", success, user);
		return success;
	}

	@Override
	public boolean hardDeleteUsersDevice(UserDevice userDevice) {
		return hardDeleteUsersDevice(userDevice, null);
	}

	@Override
	public boolean hardDeleteUsersDevice(UserDevice userDevice, Session session) {
		logger.debug("Deleting userDevice : {}", userDevice);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = deleteItem(userDevice, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting userDevice : {}", success, userDevice);
		return success;
	}

	@Override
	public boolean hardDeleteUsersProfile(UserProfile userProfile) {
		return hardDeleteUsersProfile(userProfile, null);
	}

	@Override
	public boolean hardDeleteUsersProfile(UserProfile userProfile, Session session) {
		logger.debug("Deleting userProfile : {}", userProfile);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = deleteItem(userProfile, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting userDevice : {}", success, userProfile);
		return success;
	}

	@Override
	public boolean hardDeleteUsersQuestionProfile(UserQuestionProfile userQuestionProfile) {
		return hardDeleteUsersQuestionProfile(userQuestionProfile, null);
	}

	@Override
	public boolean hardDeleteUsersQuestionProfile(UserQuestionProfile userQuestionProfile, Session session) {
		logger.debug("Deleting userQuestionProfile : {}", userQuestionProfile);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = deleteItem(userQuestionProfile, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting userQuestionProfile : {}", success, userQuestionProfile);
		return success;
	}

	@Override
	public boolean hardDeleteUsersQuizPerformance(UserQuizPerformance usersQuizPerformance) {
		return hardDeleteUsersQuizPerformance(usersQuizPerformance, null);
	}

	@Override
	public boolean hardDeleteUsersQuizPerformance(UserQuizPerformance usersQuizPerformance, Session session) {
		logger.debug("Deleting usersQuizPerformance : {}", usersQuizPerformance);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = deleteItem(usersQuizPerformance, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting usersQuizPerformance : {}", success, usersQuizPerformance);
		return success;
	}

	@Override
	public boolean hardDeleteUsersQuizProfile(UserQuizProfile usersQuizProfile) {
		return hardDeleteUsersQuizProfile(usersQuizProfile, null);
	}

	@Override
	public boolean hardDeleteUsersQuizProfile(UserQuizProfile usersQuizProfile, Session session) {
		logger.debug("Deleting usersQuizProfile : {}", usersQuizProfile);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = deleteItem(usersQuizProfile, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in deleting usersQuizProfile : {}", success, usersQuizProfile);
		return success;
	}

	@Override
	public boolean softDeleteUser(User user) {
		return softDeleteUser(user, null);
	}

	@Override
	public boolean softDeleteUser(User user, Session session) {
		logger.debug("Soft Deleting  user : {}", user);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		user.setStatus(UserStatus.INACTIVE);
		success = updateItem(user, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  user : {}", success, user);
		return success;
	}

	@Override
	public boolean softDeleteUsersDevice(UserDevice userDevice) {
		return softDeleteUsersDevice(userDevice, null);
	}

	@Override
	public boolean softDeleteUsersDevice(UserDevice userDevice, Session session) {
		logger.debug("Soft Deleting  userDevice : {}", userDevice);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		userDevice.setStatus(UserStatus.INACTIVE);
		success = updateItem(userDevice, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  userDevice : {}", success, userDevice);
		return success;
	}

	@Override
	public boolean softDeleteUsersProfile(UserProfile profile) {
		return softDeleteUsersProfile(profile, null);
	}

	@Override
	public boolean softDeleteUsersProfile(UserProfile profile, Session session) {
		logger.debug("Soft Deleting  user profile : {}", profile);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		profile.setStatus(UserStatus.INACTIVE);
		success = updateItem(profile, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  user profile : {}", success, profile);
		return success;
	}

	@Override
	public boolean softDeleteUsersQuestionProfile(UserQuestionProfile questionProfile) {
		return softDeleteUsersQuestionProfile(questionProfile, null);
	}

	@Override
	public boolean softDeleteUsersQuestionProfile(UserQuestionProfile questionProfile, Session session) {
		logger.debug("Soft Deleting  user questionProfile : {}", questionProfile);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		questionProfile.setStatus(UserStatus.INACTIVE);
		success = updateItem(questionProfile, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  user questionProfile : {}", success, questionProfile);
		return success;
	}

	@Override
	public boolean softDeleteUsersQuizPerformance(UserQuizPerformance performance) {
		return softDeleteUsersQuizPerformance(performance, null);
	}

	@Override
	public boolean softDeleteUsersQuizPerformance(UserQuizPerformance performance, Session session) {
		logger.debug("Soft Deleting  user performance : {}", performance);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		performance.setStatus(UserStatus.INACTIVE);
		success = updateItem(performance, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  user performance : {}", success, performance);
		return success;
	}

	@Override
	public boolean softDeleteUsersQuizProfile(UserQuizProfile quizProfile) {
		return softDeleteUsersQuizProfile(quizProfile, null);
	}

	@Override
	public boolean softDeleteUsersQuizProfile(UserQuizProfile quizProfile, Session session) {
		logger.debug("Soft Deleting  user quizProfile : {}", quizProfile);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		quizProfile.setStatus(UserStatus.INACTIVE);
		success = updateItem(quizProfile, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in soft Deleting  user quizProfile : {}", success, quizProfile);
		return success;
	}

	@Override
	public boolean updateUser(User user) {
		return updateUser(user, null);
	}

	@Override
	public boolean updateUser(User user, Session session) {
		logger.debug("Updating User : {}", user);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = updateItem(user, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in Updating User : {}", success, user);
		return success;
	}

	@Override
	public boolean updateUsersDevice(UserDevice userDevice) {
		return updateUsersDevice(userDevice, null);
	}

	@Override
	public boolean updateUsersDevice(UserDevice userDevice, Session session) {
		logger.debug("Updating userDevice : {}", userDevice);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = updateItem(userDevice, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in Updating userDevice : {}", success, userDevice);
		return success;
	}

	@Override
	public boolean updateUsersOtp(UserOTP usersOtp) {
		return updateUsersOtp(usersOtp, null);
	}

	@Override
	public boolean updateUsersOtp(UserOTP usersOtp, Session session) {
		logger.debug("Updating user usersOtp : {}", usersOtp);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = updateItem(usersOtp, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in Updating user quizProfile : {}", success, usersOtp);
		return success;
	}

	@Override
	public boolean updateUsersProfile(UserProfile profile) {
		return updateUsersProfile(profile, null);
	}

	@Override
	public boolean updateUsersProfile(UserProfile profile, Session session) {
		logger.debug("Updating user profile : {}", profile);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = updateItem(profile, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in Updating user profile : {}", success, profile);
		return success;
	}

	@Override
	public boolean updateUsersQuestionProfile(UserQuestionProfile questionProfile) {
		return updateUsersQuestionProfile(questionProfile, null);
	}

	@Override
	public boolean updateUsersQuestionProfile(UserQuestionProfile questionProfile, Session session) {
		logger.debug("Updating user questionProfile : {}", questionProfile);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = updateItem(questionProfile, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in Updating user questionProfile : {}", success, questionProfile);
		return success;
	}

	@Override
	public boolean updateUsersQuizPerformance(UserQuizPerformance performance) {
		return updateUsersQuizPerformance(performance, null);
	}

	@Override
	public boolean updateUsersQuizPerformance(UserQuizPerformance performance, Session session) {
		logger.debug("Updating user performance : {}", performance);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = updateItem(performance, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in Updating user performance : {}", success, performance);
		return success;
	}

	@Override
	public boolean updateUsersQuizProfile(UserQuizProfile quizProfile) {
		return updateUsersQuizProfile(quizProfile, null);
	}

	@Override
	public boolean updateUsersQuizProfile(UserQuizProfile quizProfile, Session session) {
		logger.debug("Updating user quizProfile : {}", quizProfile);
		Session localSession = (session == null) ? HibernateUtil.getUsersSessionFactory().openSession() : session;
		boolean success = false;
		success = updateItem(quizProfile, localSession);
		if (localSession != session) {
			localSession.close();
		}
		logger.debug("Success: {} in Updating user quizProfile : {}", success, quizProfile);
		return success;
	}

}
