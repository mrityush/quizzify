package com.src.main.tlabs.quizzify.db.communications;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import layer1.layer2.comm.request.Request;
import layer1.layer2.comm.response.Response;
import layer1.layer2.generics.QuizzifyEntity;
import layer1.layer2.generics.Status;
import layer1.layer2.link.AbstractCommunicationLink;
import layer1.layer2.resource.ErrorType;
import layer1.layer2.serialise.JacksonUtil;
import layer1.layer2.serialise.JsonUtil;

import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.src.main.tlabs.quizzify.db.statusfields.CommunicationLinkStatus;

@Entity
@Table(name = "communication_links", uniqueConstraints = @UniqueConstraint(columnNames = { "id", "request" }), indexes = { @Index(columnList = "id,type", name = "comm_links_type_id_idx") })
public class CommunicationLink extends AbstractCommunicationLink implements QuizzifyEntity {
	private static Logger logger = LoggerFactory.getLogger(CommunicationLink.class);

	public CommunicationLink(Builder b) {
		super(b);
		id = b.id;
		type = b.type;
		status = b.status;
		requestJson = b.requestJson;
		responseJson = b.responseJson;
		creationTime = b.creationTime;
		createdBy = b.createdBy;
		lastModificationTime = b.lastModificationTime;
		lastModifiedBy = b.lastModifiedBy;
	}

	public CommunicationLink() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "type", nullable = false)
	private int type;

	@Column(name = "status", nullable = false)
	private CommunicationLinkStatus status;

	@Column(name = "request")
	private String requestJson;

	@Column(name = "response")
	private String responseJson;

	@Column(name = "created_by", nullable = false)
	private long createdBy;

	@Column(name = "creation_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime creationTime;

	@Column(name = "last_modified_by", nullable = false)
	private long lastModifiedBy;

	@Column(name = "last_modification_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime lastModificationTime;

	public static class Builder extends AbstractCommunicationLink.Builder<CommunicationLink, Builder> {
		private long id;
		private int type;
		private CommunicationLinkStatus status;
		private String requestJson;
		private String responseJson;
		private long createdBy;
		private LocalDateTime creationTime;
		private long lastModifiedBy;
		private LocalDateTime lastModificationTime;

		public CommunicationLink build() {
			return new CommunicationLink(this);
		}

		public Builder id(long id) {
			this.id = id;
			return self();
		}

		public Builder type(int type) {
			this.type = type;
			return self();
		}

		public Builder status(CommunicationLinkStatus status) {
			this.status = status;
			return self();
		}

		public Builder response(Response response) {
			this.response = response;
			this.responseJson = JsonUtil.toJson(response).orElse(null);
			return self();
		}

		public Builder request(Request request) {
			this.request = request;
			this.requestJson = JsonUtil.toJson(request).orElse(null);
			return self();
		}

		public Builder lastModifiedBy(long lastModifiedBy) {
			this.lastModifiedBy = lastModifiedBy;
			return self();
		}

		public Builder lastModificationTime(LocalDateTime lastModificationTime) {
			this.lastModificationTime = lastModificationTime;
			return self();
		}

		public Builder createdBy(long createdBy) {
			this.createdBy = createdBy;
			return self();
		}

		public Builder creationTime(LocalDateTime creationTime) {
			this.creationTime = creationTime;
			return self();
		}

		@Override
		public Builder self() {
			return this;
		}
	}

	public long id() {
		return id;
	}

	public LocalDateTime creationTime() {
		return creationTime;
	}

	public long createdBy() {
		return createdBy;
	}

	public LocalDateTime lastModificationTime() {
		return lastModificationTime;
	}

	public long lastModifiedBy() {
		return lastModifiedBy;
	}

	public int type() {
		return type;
	}

	public static class Deserializer extends JsonDeserializer<CommunicationLink> {
		@Override
		public CommunicationLink deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			p.nextToken();
			final CommunicationLink communicationLinks = new CommunicationLink();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				communicationLinks.populateFromJsonString(field, p);
			}
			return communicationLinks;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeNumberField("id", id);
		g.writeNumberField("type", type);
		g.writeNumberField("status", status.id());
		g.writeStringField("request", requestJson);
		g.writeStringField("response", responseJson);
		g.writeNumberField("createdBy", createdBy);
		g.writeObjectField("creationTime", creationTime);
		g.writeNumberField("lastModifiedBy", lastModifiedBy);
		g.writeObjectField("lastModificationTime", lastModificationTime);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("id".equals(field)) {
			p.nextToken();
			id = p.readValueAs(long.class);
		} else if ("type".equals(field)) {
			p.nextToken();
			type = p.readValueAs(int.class);
		} else if ("request".equals(field)) {
			p.nextToken();
			requestJson = p.readValueAs(String.class);
		} else if ("status".equals(field)) {
			p.nextToken();
			status = CommunicationLinkStatus.valueOf(p.readValueAs(int.class));
		} else if ("response".equals(field)) {
			p.nextToken();
			responseJson = p.readValueAs(String.class);
		} else if ("createdBy".equals(field)) {
			p.nextToken();
			createdBy = p.readValueAs(long.class);
		} else if ("creationTime".equals(field)) {
			p.nextToken();
			creationTime = p.readValueAs(LocalDateTime.class);
		} else if ("lastModifiedBy".equals(field)) {
			p.nextToken();
			lastModifiedBy = p.readValueAs(long.class);
		} else if ("lastModificationTime".equals(field)) {
			p.nextToken();
			lastModificationTime = p.readValueAs(LocalDateTime.class);
		}
	}

	@Override
	public Status status() {
		return status;
	}

	@Override
	public void listen(String input) {
		try {
			long start = System.currentTimeMillis();
			request = JacksonUtil.DEFAULT.mapper().readValue(input, new TypeReference<Request>() {
			});
			requestJson = input;
			long end = System.currentTimeMillis();
			if (end - start > 100) {
				logger.warn("JSON Serialization-De-Serialization: {} ms taken to create request of class ({}) ", end
						- start, requestJson.getClass().getSimpleName());
			}
		} catch (IOException e) {
			logger.error("Error parsing input: ({})", input, e);
		}
	}

	@Override
	public String reply() {
		long start = System.currentTimeMillis();
		String reply = response.replyInJson().orElseThrow(() -> ErrorType.INTERNAL_SERVER_ERROR.exception());
		long end = System.currentTimeMillis();
		if (end - start > 100) {
			logger.warn("JSON Serialization-De-Serialization: {} ms taken to create response for class ({}) ", end
					- start, requestJson.getClass().getSimpleName());
		}
		logger.debug("Sending Response: ({})", reply);
		return reply;
	}

	@Override
	public Request request() {
		return request;
	}

	@Override
	public Response response() {
		return JsonUtil.fromJson(responseJson, new TypeReference<Response>() {
		}).get();
	}

	@Override
	public void setStatus(Status status) {
		this.status = (CommunicationLinkStatus) status;
	}

}