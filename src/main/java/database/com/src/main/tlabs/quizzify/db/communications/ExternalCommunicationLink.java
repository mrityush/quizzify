package com.src.main.tlabs.quizzify.db.communications;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import layer1.layer2.generics.QuizzifyEntity;
import layer1.layer2.generics.Status;
import layer1.layer2.serialise.JsonUtil;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.src.main.tlabs.quizzify.db.statusfields.CommunicationLinkStatus;

@Entity
@Table(name = "external_communication_links", uniqueConstraints = @UniqueConstraint(columnNames = { "id", "request_id" }), indexes = {
		@Index(columnList = "request_id", name = "idx_external_communications_request_id"),
		@Index(columnList = "user_id", name = "idx_external_communications_user") })
public class ExternalCommunicationLink implements QuizzifyEntity {
	public ExternalCommunicationLink(Builder b) {
		id = b.id;
		requestId = b.requestId;
		userId = b.userId;
		lastRetryTime = b.lastRetryTime;
		executionTime = b.executionTime;
		maxAttempts = b.maxAttempts;
		attempts = b.attempts;
		responseText = b.responseText;
		status = b.status;
		request = b.request;
		response = b.response;
		creationTime = b.creationTime;
		createdBy = b.createdBy;
		lastModificationTime = b.lastModificationTime;
		lastModifiedBy = b.lastModifiedBy;
	}

	public ExternalCommunicationLink() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "request_id", nullable = false)
	private String requestId;

	@Column(name = "user_id")
	private long userId;

	@Column(name = "status")
	private CommunicationLinkStatus status;

	@Column(name = "max_attempts")
	private int maxAttempts;

	@Column(name = "attempts")
	private int attempts;

	@Column(name = "execution_time")
	private LocalDateTime executionTime;

	@Column(name = "last_retry_time")
	private LocalDateTime lastRetryTime;

	@Column(name = "request")
	@Type(type="text")
	private String request;

	@Column(name = "response")
	@Type(type="text")
	private String response;

	@Column(name = "response_text")
	private String responseText;

	@Column(name = "created_by", nullable = false)
	private long createdBy;

	@Column(name = "creation_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime creationTime;

	@Column(name = "last_modified_by", nullable = false)
	private long lastModifiedBy;

	@Column(name = "last_modification_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime lastModificationTime;

	public static class Builder {
		private long id;
		private String requestId;
		private long userId;
		private CommunicationLinkStatus status;
		private int maxAttempts;
		private int attempts;
		private LocalDateTime executionTime;
		private LocalDateTime lastRetryTime;
		private String request;
		private String response;
		private String responseText;
		private long createdBy;
		private LocalDateTime creationTime;
		private long lastModifiedBy;
		private LocalDateTime lastModificationTime;

		public ExternalCommunicationLink build() {
			return new ExternalCommunicationLink(this);
		}

		public Builder id(long id) {
			this.id = id;
			return this;
		}

		public Builder requestId(String requestId) {
			this.requestId = requestId;
			return this;
		}

		public Builder userId(long userId) {
			this.userId = userId;
			return this;
		}

		public Builder status(CommunicationLinkStatus status) {
			this.status = status;
			return this;
		}

		public Builder maxAttempts(int maxAttempts) {
			this.maxAttempts = maxAttempts;
			return this;
		}

		public Builder attempts(int attempts) {
			this.attempts = attempts;
			return this;
		}

		public Builder executionTime(LocalDateTime executionTime) {
			this.executionTime = executionTime;
			return this;
		}

		public Builder lastRetryTime(LocalDateTime lastRetryTime) {
			this.lastRetryTime = lastRetryTime;
			return this;
		}

		public Builder response(String response) {
			this.response = response;
			return this;
		}

		public Builder request(String request) {
			this.request = request;
			return this;
		}

		public Builder responseText(String responseText) {
			this.responseText = responseText;
			return this;
		}

		public Builder lastModifiedBy(long lastModifiedBy) {
			this.lastModifiedBy = lastModifiedBy;
			return this;
		}

		public Builder lastModificationTime(LocalDateTime lastModificationTime) {
			this.lastModificationTime = lastModificationTime;
			return this;
		}

		public Builder createdBy(long createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public Builder creationTime(LocalDateTime creationTime) {
			this.creationTime = creationTime;
			return this;
		}
	}

	public long id() {
		return id;
	}

	public String requestId() {
		return requestId;
	}

	public long userId() {
		return userId;
	}

	public LocalDateTime executionTime() {
		return executionTime;
	}

	public LocalDateTime lastRetryTime() {
		return lastRetryTime;
	}

	public LocalDateTime creationTime() {
		return creationTime;
	}

	public long createdBy() {
		return createdBy;
	}

	public LocalDateTime lastModificationTime() {
		return lastModificationTime;
	}

	public long lastModifiedBy() {
		return lastModifiedBy;
	}

	public String request() {
		return request;
	}

	public String response() {
		return response;
	}

	public String responseText() {
		return responseText;
	}

	public static class Deserializer extends JsonDeserializer<ExternalCommunicationLink> {
		@Override
		public ExternalCommunicationLink deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			p.nextToken();
			final ExternalCommunicationLink externalCommunicationLinks = new ExternalCommunicationLink();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				externalCommunicationLinks.populateFromJsonString(field, p);
			}
			return externalCommunicationLinks;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeNumberField("id", id);
		g.writeNumberField("userId", userId);
		g.writeNumberField("maxAttempts", maxAttempts);
		g.writeNumberField("attempts", attempts);
		g.writeNumberField("status", status.id());
		g.writeStringField("requestId", requestId);
		g.writeStringField("request", JsonUtil.toJson(request).orElse(null));
		g.writeStringField("response", JsonUtil.toJson(response).orElse(null));
		g.writeStringField("responseText", responseText);
		g.writeNumberField("createdBy", createdBy);
		g.writeObjectField("creationTime", creationTime);
		g.writeObjectField("executionTime", executionTime);
		g.writeObjectField("lastRetryTime", lastRetryTime);
		g.writeNumberField("lastModifiedBy", lastModifiedBy);
		g.writeObjectField("lastModificationTime", lastModificationTime);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("id".equals(field)) {
			p.nextToken();
			id = p.readValueAs(long.class);
		} else if ("attempts".equals(field)) {
			p.nextToken();
			attempts = p.readValueAs(int.class);
		} else if ("maxAttempts".equals(field)) {
			p.nextToken();
			maxAttempts = p.readValueAs(int.class);
		} else if ("status".equals(field)) {
			p.nextToken();
			status = CommunicationLinkStatus.valueOf(p.readValueAs(int.class));
		} else if ("requestId".equals(field)) {
			p.nextToken();
			requestId = p.readValueAs(String.class);
		} else if ("request".equals(field)) {
			p.nextToken();
			request = p.readValueAs(String.class);
		} else if ("responseText".equals(field)) {
			p.nextToken();
			responseText = p.readValueAs(String.class);
		} else if ("response".equals(field)) {
			p.nextToken();
			response = p.readValueAs(String.class);
		} else if ("createdBy".equals(field)) {
			p.nextToken();
			createdBy = p.readValueAs(long.class);
		} else if ("lastRetryTime".equals(field)) {
			p.nextToken();
			lastRetryTime = p.readValueAs(LocalDateTime.class);
		} else if ("executionTime".equals(field)) {
			p.nextToken();
			executionTime = p.readValueAs(LocalDateTime.class);
		} else if ("creationTime".equals(field)) {
			p.nextToken();
			creationTime = p.readValueAs(LocalDateTime.class);
		} else if ("lastModifiedBy".equals(field)) {
			p.nextToken();
			lastModifiedBy = p.readValueAs(long.class);
		} else if ("lastModificationTime".equals(field)) {
			p.nextToken();
			lastModificationTime = p.readValueAs(LocalDateTime.class);
		}
	}

	@Override
	public Status status() {
		return status;
	}

	@Override
	public void setStatus(Status status) {
		this.status = (CommunicationLinkStatus) status;
	}
}