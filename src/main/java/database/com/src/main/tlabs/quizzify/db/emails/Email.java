package com.src.main.tlabs.quizzify.db.emails;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import layer1.layer2.generics.QuizzifyEntity;
import layer1.layer2.generics.Status;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.src.main.tlabs.quizzify.db.statusfields.EmailStatus;

@Entity
@Table(name = "emails", uniqueConstraints = @UniqueConstraint(columnNames = { "user_id", "template_id",
		"value_variable" }), indexes = { @Index(columnList = "user_id", name = "idx_emails_user") })
public class Email implements QuizzifyEntity {
	public Email(Builder b) {
		id = b.id;
		userId = b.userId;
		templateId = b.templateId;
		valueVariable = b.valueVariable;
		status = b.status;
		creationTime = b.creationTime;
		createdBy = b.createdBy;
		lastModificationTime = b.lastModificationTime;
		lastModificationTimes = b.lastModificationTimes;
		lastModifiedBy = b.lastModifiedBy;
	}

	public Email() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "user_id")
	private long userId;

	@Column(name = "template_id", nullable = false)
	private long templateId;

	@Column(name = "value_variable")
	private String valueVariable;

	@Column(name = "status")
	private EmailStatus status;

	@Column(name = "creation_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime creationTime;

	@Column(name = "created_by", nullable = false)
	private long createdBy;

	@Transient
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime lastModificationTimes;

	@Column(name = "last_modification_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime lastModificationTime;

	@Column(name = "last_modified_by", nullable = false)
	private long lastModifiedBy;

	public static class Builder {
		private long id;
		private long userId;
		private long templateId;
		private EmailStatus status;
		private String valueVariable;

		private long createdBy;
		private LocalDateTime creationTime;
		private long lastModifiedBy;
		private LocalDateTime lastModificationTime;
		private DateTime lastModificationTimes;

		public Email build() {
			return new Email(this);
		}

		public Builder id(long id) {
			this.id = id;
			return this;
		}

		public Builder templateId(long templateId) {
			this.templateId = templateId;
			return this;
		}

		public Builder userId(long userId) {
			this.userId = userId;
			return this;
		}

		public Builder status(EmailStatus status) {
			this.status = status;
			return this;
		}

		public Builder valueVariable(String valueVariable) {
			this.valueVariable = valueVariable;
			return this;
		}

		public Builder lastModifiedBy(long lastModifiedBy) {
			this.lastModifiedBy = lastModifiedBy;
			return this;
		}

		public Builder lastModificationTime(LocalDateTime lastModificationTime) {
			this.lastModificationTime = lastModificationTime;
			return this;
		}

		public Builder lastModificationTimes(DateTime lastModificationTimes) {
			this.lastModificationTimes = lastModificationTimes;
			return this;
		}

		public Builder createdBy(long createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public Builder creationTime(LocalDateTime creationTime) {
			this.creationTime = creationTime;
			return this;
		}
	}

	public long id() {
		return id;
	}

	public long templateId() {
		return templateId;
	}

	public long userId() {
		return userId;
	}

	public LocalDateTime creationTime() {
		return creationTime;
	}

	public long createdBy() {
		return createdBy;
	}

	public LocalDateTime lastModificationTime() {
		return lastModificationTime;
	}

	public long lastModifiedBy() {
		return lastModifiedBy;
	}

	public static class Deserializer extends JsonDeserializer<Email> {
		@Override
		public Email deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			p.nextToken();
			final Email emails = new Email();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				emails.populateFromJsonString(field, p);
			}
			return emails;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeNumberField("id", id);
		g.writeNumberField("userId", userId);
		g.writeStringField("valueVariable", valueVariable);
		g.writeNumberField("templateId", templateId);
		g.writeNumberField("createdBy", createdBy);
		g.writeObjectField("creationTime", creationTime);
		g.writeNumberField("lastModifiedBy", lastModifiedBy);
		g.writeObjectField("lastModificationTime", lastModificationTime);
		g.writeObjectField("lastModificationTimes", lastModificationTimes);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("id".equals(field)) {
			p.nextToken();
			id = p.readValueAs(long.class);
		} else if ("valueVariable".equals(field)) {
			p.nextToken();
			valueVariable = p.readValueAs(String.class);
		} else if ("status".equals(field)) {
			p.nextToken();
			status = EmailStatus.valueOf(p.readValueAs(int.class));
		} else if ("templateId".equals(field)) {
			p.nextToken();
			templateId = p.readValueAs(long.class);
		} else if ("createdBy".equals(field)) {
			p.nextToken();
			createdBy = p.readValueAs(long.class);
		} else if ("creationTime".equals(field)) {
			p.nextToken();
			creationTime = p.readValueAs(LocalDateTime.class);
		} else if ("lastModifiedBy".equals(field)) {
			p.nextToken();
			lastModifiedBy = p.readValueAs(long.class);
		} else if ("lastModificationTime".equals(field)) {
			p.nextToken();
			lastModificationTime = p.readValueAs(LocalDateTime.class);
		}
	}

	@Override
	public Status status() {
		return status;
	}

	@Override
	public void setStatus(Status status) {
		this.status = (EmailStatus) status;
	}

}