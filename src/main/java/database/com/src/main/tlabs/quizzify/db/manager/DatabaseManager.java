package com.src.main.tlabs.quizzify.db.manager;

import java.util.HashMap;
import java.util.Map;

import layer1.layer2.hibernate.HibernateConfig;

public enum DatabaseManager {
	USERS(1, "users") {

		@Override
		public void configureXml() {
			this.configXml = HibernateConfig.USERS_CONFIG_FILE;
		}
	},
	QUESTIONNAIRES(2, "questionnaires") {

		@Override
		public void configureXml() {
			this.configXml = HibernateConfig.QUESTIONNAIRES_CONFIG_FILE;
		}

	},
	REWARDS(3, "rewards") {

		@Override
		public void configureXml() {
			this.configXml = HibernateConfig.REWARDS_CONFIG_FILE;
		}

	},
	COMMUNICATIONS(4, "communications") {

		@Override
		public void configureXml() {
			this.configXml = HibernateConfig.COMMUNICATIONS_CONFIG_FILE;
		}

	},
	SPONSORS(5, "sponsors") {

		@Override
		public void configureXml() {
			this.configXml = HibernateConfig.SPONSORS_CONFIG_FILE;
		}

	},
	SHIPMENTS(6, "shipments") {

		@Override
		public void configureXml() {
			this.configXml = HibernateConfig.SHIPMENTS_CONFIG_FILE;
		}

	},
	EMAILS(7, "emails") {

		@Override
		public void configureXml() {
			this.configXml = HibernateConfig.EMAILS_CONFIG_FILE;
		}

	};
	private int id;
	private String description;
	protected String configXml;

	private DatabaseManager(int id, String description) {
		this.id = id;
		this.description = description;
		configureXml();
	}

	public abstract void configureXml();

	private static final Map<Integer, DatabaseManager> types = new HashMap<Integer, DatabaseManager>();

	static {
		for (DatabaseManager databaseManager : DatabaseManager.values()) {
			if (types.get(databaseManager.id) == null) {
				types.put(databaseManager.id, databaseManager);
			} else {
				// Duplicate ID Error
			}
		}
	}

	public int id() {
		return id;
	}

	public String description() {
		return description;
	}

	public String configXml() {
		return configXml;
	}

}
