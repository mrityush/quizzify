package com.src.main.tlabs.quizzify.db.questionnaires;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import layer1.layer2.generics.QuizzifyEntity;
import layer1.layer2.generics.Status;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.src.main.tlabs.quizzify.db.statusfields.QuestionStatus;

@Entity
@Table(name = "questions", uniqueConstraints = @UniqueConstraint(columnNames = { "question" }))
public class Question implements QuizzifyEntity {

	public Question(Builder b) {
		id = b.id;
		question = b.question;
		status = b.status;
		encryptedAnswer = b.encryptedAnswer;
		creationTime = b.creationTime;
		createdBy = b.createdBy;
		lastModificationTime = b.lastModificationTime;
		lastModifiedBy = b.lastModifiedBy;
	}

	public Question() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "question", nullable = false)
	private String question;

	@Column(name = "encrypted_answer", nullable = false)
	private String encryptedAnswer;

	@Column(name = "status")
	private QuestionStatus status;

	@Column(name = "created_by", nullable = false)
	private long createdBy;

	@Column(name = "creation_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime creationTime;

	@Column(name = "last_modified_by", nullable = false)
	private long lastModifiedBy;

	@Column(name = "last_modification_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime lastModificationTime;

	public static class Builder {

		private long id;
		private String question;
		private QuestionStatus status;
		private String encryptedAnswer;
		private long createdBy;
		private LocalDateTime creationTime;
		private long lastModifiedBy;
		private LocalDateTime lastModificationTime;

		public Question build() {
			return new Question(this);
		}

		public Builder id(long id) {
			this.id = id;
			return this;
		}

		public Builder question(String question) {
			this.question = question;
			return this;
		}

		public Builder encryptedAnswer(String encryptedAnswer) {
			this.encryptedAnswer = encryptedAnswer;
			return this;
		}

		public Builder status(QuestionStatus status) {
			this.status = status;
			return this;
		}

		public Builder lastModifiedBy(long lastModifiedBy) {
			this.lastModifiedBy = lastModifiedBy;
			return this;
		}

		public Builder lastModificationTime(LocalDateTime lastModificationTime) {
			this.lastModificationTime = lastModificationTime;
			return this;
		}

		public Builder createdBy(long createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public Builder creationTime(LocalDateTime creationTime) {
			this.creationTime = creationTime;
			return this;
		}
	}

	public long id() {
		return id;
	}

	public String question() {
		return question;
	}

	public LocalDateTime creationTime() {
		return creationTime;
	}

	public long createdBy() {
		return createdBy;
	}

	public LocalDateTime lastModificationTime() {
		return lastModificationTime;
	}

	public long lastModifiedBy() {
		return lastModifiedBy;
	}

	public String encryptedAnswer() {
		return encryptedAnswer;
	}

	public static class Deserializer extends JsonDeserializer<Question> {
		@Override
		public Question deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			p.nextToken();
			final Question questions = new Question();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				questions.populateFromJsonString(field, p);
			}
			return questions;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeNumberField("id", id);
		g.writeNumberField("status", status.id());
		g.writeStringField("question", question);
		g.writeStringField("encryptedAnswer", encryptedAnswer);
		g.writeNumberField("createdBy", createdBy);
		g.writeObjectField("creationTime", creationTime);
		g.writeNumberField("lastModifiedBy", lastModifiedBy);
		g.writeObjectField("lastModificationTime", lastModificationTime);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("id".equals(field)) {
			p.nextToken();
			id = p.readValueAs(long.class);
		} else if ("status".equals(field)) {
			p.nextToken();
			status = QuestionStatus.valueOf(p.readValueAs(int.class));
		} else if ("question".equals(field)) {
			p.nextToken();
			question = p.readValueAs(String.class);
		} else if ("encryptedAnswer".equals(field)) {
			p.nextToken();
			encryptedAnswer = p.readValueAs(String.class);
		} else if ("createdBy".equals(field)) {
			p.nextToken();
			createdBy = p.readValueAs(long.class);
		} else if ("creationTime".equals(field)) {
			p.nextToken();
			creationTime = p.readValueAs(LocalDateTime.class);
		} else if ("lastModifiedBy".equals(field)) {
			p.nextToken();
			lastModifiedBy = p.readValueAs(long.class);
		} else if ("lastModificationTime".equals(field)) {
			p.nextToken();
			lastModificationTime = p.readValueAs(LocalDateTime.class);
		}
	}

	@Override
	public Status status() {
		return status;
	}

	@Override
	public void setStatus(Status status) {
		this.status = (QuestionStatus) status;
	}
}