package com.src.main.tlabs.quizzify.db.questionnaires;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import layer1.layer2.generics.QuizzifyEntity;
import layer1.layer2.generics.Status;
import layer1.layer2.serialise.JsonUtil;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.src.main.tlabs.quizzify.db.statusfields.QuizStatus;

@Entity
@Table(name = "quizzes", uniqueConstraints = @UniqueConstraint(columnNames = { "quiz_name", "launch_time",
		"question_ids" }), indexes = { @Index(columnList = "launch_time,duration,category_id", name = "idx_launch_time_duration_category") })
public class Quiz implements QuizzifyEntity {

	public Quiz(Builder b) {
		id = b.id;
		questionIds = b.questionIds;
		questionIdsJson = b.questionIdsJson;
		categoryId = b.categoryId;
		launchTime = b.launchTime;
		duration = b.duration;
		status = b.status;
		quizName = b.quizName;
		creationTime = b.creationTime;
		createdBy = b.createdBy;
		lastModificationTime = b.lastModificationTime;
		lastModifiedBy = b.lastModifiedBy;
	}

	public Quiz() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "quiz_name", nullable = false)
	private String quizName;

	@Column(name = "question_ids", nullable = false)
	private String questionIdsJson;

	@Transient
	private List<Long> questionIds;

	@Column(name = "category_id")
	private long categoryId;

	@Column(name = "status")
	private QuizStatus status;

	@Column(name = "duration")
	private int duration;

	@Column(name = "launch_time")
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime launchTime;

	@Column(name = "created_by", nullable = false)
	private long createdBy;

	@Column(name = "creation_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime creationTime;

	@Column(name = "last_modified_by", nullable = false)
	private long lastModifiedBy;

	@Column(name = "last_modification_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime lastModificationTime;

	public static class Builder {

		public String questionIdsJson;
		private long id;
		private List<Long> questionIds;
		private String quizName;
		private long categoryId;
		private LocalDateTime launchTime;
		private QuizStatus status;
		private int duration;
		private long createdBy;
		private LocalDateTime creationTime;
		private long lastModifiedBy;
		private LocalDateTime lastModificationTime;

		public Quiz build() {
			return new Quiz(this);
		}

		public Builder id(long id) {
			this.id = id;
			return this;
		}

		public Builder questionIds(List<Long> questionIds) {
			this.questionIds = questionIds;
			this.questionIdsJson = JsonUtil.toJson(questionIds).get();
			return this;
		}

		public Builder categoryId(long categoryId) {
			this.categoryId = categoryId;
			return this;
		}

		public Builder status(QuizStatus status) {
			this.status = status;
			return this;
		}

		public Builder duration(int duration) {
			this.duration = duration;
			return this;
		}

		public Builder executionTime(LocalDateTime executionTime) {
			this.launchTime = executionTime;
			return this;
		}

		public Builder quizName(String quizName) {
			this.quizName = quizName;
			return this;
		}

		public Builder lastModifiedBy(long lastModifiedBy) {
			this.lastModifiedBy = lastModifiedBy;
			return this;
		}

		public Builder lastModificationTime(LocalDateTime lastModificationTime) {
			this.lastModificationTime = lastModificationTime;
			return this;
		}

		public Builder createdBy(long createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public Builder creationTime(LocalDateTime creationTime) {
			this.creationTime = creationTime;
			return this;
		}
	}

	public long id() {
		return id;
	}

	public List<Long> questionIds() {
		return questionIds;
	}

	public long userId() {
		return categoryId;
	}

	public LocalDateTime launchTime() {
		return launchTime;
	}

	public LocalDateTime creationTime() {
		return creationTime;
	}

	public long createdBy() {
		return createdBy;
	}

	public LocalDateTime lastModificationTime() {
		return lastModificationTime;
	}

	public long lastModifiedBy() {
		return lastModifiedBy;
	}

	public String request() {
		return quizName;
	}

	public static class Deserializer extends JsonDeserializer<Quiz> {
		@Override
		public Quiz deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			p.nextToken();
			final Quiz communicationLinks = new Quiz();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				communicationLinks.populateFromJsonString(field, p);
			}
			return communicationLinks;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeNumberField("id", id);
		g.writeNumberField("categoryId", categoryId);
		g.writeNumberField("duration", duration);
		g.writeNumberField("status", status.id());
		g.writeStringField("questionIds", JsonUtil.toJson(questionIds).get());
		g.writeNumberField("createdBy", createdBy);
		g.writeObjectField("creationTime", creationTime);
		g.writeObjectField("launchTime", launchTime);
		g.writeNumberField("lastModifiedBy", lastModifiedBy);
		g.writeObjectField("lastModificationTime", lastModificationTime);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("id".equals(field)) {
			p.nextToken();
			id = p.readValueAs(long.class);
		} else if ("duration".equals(field)) {
			p.nextToken();
			duration = p.readValueAs(int.class);
		} else if ("status".equals(field)) {
			p.nextToken();
			status = QuizStatus.valueOf(p.readValueAs(int.class));
		} else if ("questionIds".equals(field)) {
			p.nextToken();
			questionIds = p.readValueAs(new TypeReference<List<Long>>() {
			});
		} else if ("quizName".equals(field)) {
			p.nextToken();
			quizName = p.readValueAs(String.class);
		} else if ("createdBy".equals(field)) {
			p.nextToken();
			createdBy = p.readValueAs(long.class);
		} else if ("executionTime".equals(field)) {
			p.nextToken();
			launchTime = p.readValueAs(LocalDateTime.class);
		} else if ("creationTime".equals(field)) {
			p.nextToken();
			creationTime = p.readValueAs(LocalDateTime.class);
		} else if ("lastModifiedBy".equals(field)) {
			p.nextToken();
			lastModifiedBy = p.readValueAs(long.class);
		} else if ("lastModificationTime".equals(field)) {
			p.nextToken();
			lastModificationTime = p.readValueAs(LocalDateTime.class);
		}
	}

	@Override
	public Status status() {
		return status;
	}

	@Override
	public void setStatus(Status status) {
		this.status = (QuizStatus) status;
	}
}