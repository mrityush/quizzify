package com.src.main.tlabs.quizzify.db.questionnaires;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.src.main.tlabs.quizzify.db.statusfields.QuizStatus;

import layer1.layer2.generics.QuizzifyEntity;
import layer1.layer2.generics.Status;
import layer1.layer2.serialise.JsonUtil;

@Entity
@Table(name = "quiz_sets", uniqueConstraints = @UniqueConstraint(columnNames = { "quiz_id", "set_id" }) , indexes = {
		@Index(columnList = "quiz_id,set_id", name = "idx_quiz_id_set_id") })
public class QuizSet implements QuizzifyEntity {

	public QuizSet(Builder b) {
		id = b.id;
		questionSequenceJson = b.questionSequenceJson;
		questionSequence = b.questionSequence;
		quizId = b.quizId;
		setId = b.setId;
		status = b.status;
		creationTime = b.creationTime;
		createdBy = b.createdBy;
		lastModificationTime = b.lastModificationTime;
		lastModifiedBy = b.lastModifiedBy;
	}

	public QuizSet() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "quiz_id")
	private long quizId;

	@Column(name = "q_sequence")
	private String questionSequenceJson;

	@Transient
	private List<Integer> questionSequence;

	@Column(name = "status")
	private QuizStatus status;

	@Column(name = "set_id")
	private int setId;

	@Column(name = "created_by", nullable = false)
	private long createdBy;

	@Column(name = "creation_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime creationTime;

	@Column(name = "last_modified_by", nullable = false)
	private long lastModifiedBy;

	@Column(name = "last_modification_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime lastModificationTime;

	public static class Builder {

		private long id;
		private long quizId;
		private int setId;
		private QuizStatus status;
		private long createdBy;
		private LocalDateTime creationTime;
		private long lastModifiedBy;
		private LocalDateTime lastModificationTime;
		private List<Integer> questionSequence;
		private String questionSequenceJson;

		public QuizSet build() {
			return new QuizSet(this);
		}

		public Builder id(long id) {
			this.id = id;
			return this;
		}

		public Builder quizId(long quizId) {
			this.quizId = quizId;
			return this;
		}

		public Builder status(QuizStatus status) {
			this.status = status;
			return this;
		}

		public Builder setId(int setId) {
			this.setId = setId;
			return this;
		}

		public Builder questionSequence(List<Integer> questionSequence) {
			this.questionSequence = questionSequence;
			this.questionSequenceJson = JsonUtil.toJson(questionSequence).get();
			return this;
		}

		public Builder qSequence(String qSequence) {
			this.questionSequenceJson = qSequence;
			return this;
		}

		public Builder lastModifiedBy(long lastModifiedBy) {
			this.lastModifiedBy = lastModifiedBy;
			return this;
		}

		public Builder lastModificationTime(LocalDateTime lastModificationTime) {
			this.lastModificationTime = lastModificationTime;
			return this;
		}

		public Builder createdBy(long createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public Builder creationTime(LocalDateTime creationTime) {
			this.creationTime = creationTime;
			return this;
		}

	}

	public long id() {
		return id;
	}

	public long quizId() {
		return quizId;
	}

	public LocalDateTime creationTime() {
		return creationTime;
	}

	public long createdBy() {
		return createdBy;
	}

	public List<Integer> questionSequence() {
		return questionSequence;
	}

	public String qSequence() {
		return questionSequenceJson;
	}

	public LocalDateTime lastModificationTime() {
		return lastModificationTime;
	}

	public long lastModifiedBy() {
		return lastModifiedBy;
	}

	public static class Deserializer extends JsonDeserializer<QuizSet> {
		@Override
		public QuizSet deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			p.nextToken();
			final QuizSet quizSets = new QuizSet();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				quizSets.populateFromJsonString(field, p);
			}
			return quizSets;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeNumberField("id", id);
		g.writeNumberField("quizId", quizId);
		g.writeNumberField("setId", setId);
		g.writeNumberField("status", status.id());
		g.writeStringField("questionSequence", JsonUtil.toJson(questionSequence).get());
		g.writeNumberField("createdBy", createdBy);
		g.writeObjectField("creationTime", creationTime);
		g.writeNumberField("lastModifiedBy", lastModifiedBy);
		g.writeObjectField("lastModificationTime", lastModificationTime);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("id".equals(field)) {
			p.nextToken();
			id = p.readValueAs(long.class);
		} else if ("setId".equals(field)) {
			p.nextToken();
			setId = p.readValueAs(int.class);
		} else if ("quizId".equals(field)) {
			p.nextToken();
			quizId = p.readValueAs(long.class);
		} else if ("questionSequence".equals(field)) {
			p.nextToken();
			questionSequence = p.readValueAs(new TypeReference<List<Integer>>() {
			});
		} else if ("status".equals(field)) {
			p.nextToken();
			status = QuizStatus.valueOf(p.readValueAs(int.class));
		} else if ("createdBy".equals(field)) {
			p.nextToken();
			createdBy = p.readValueAs(long.class);
		} else if ("creationTime".equals(field)) {
			p.nextToken();
			creationTime = p.readValueAs(LocalDateTime.class);
		} else if ("lastModifiedBy".equals(field)) {
			p.nextToken();
			lastModifiedBy = p.readValueAs(long.class);
		} else if ("lastModificationTime".equals(field)) {
			p.nextToken();
			lastModificationTime = p.readValueAs(LocalDateTime.class);
		}
	}

	@Override
	public Status status() {
		return status;
	}

	@Override
	public void setStatus(Status status) {
		this.status = (QuizStatus) status;
	}
}