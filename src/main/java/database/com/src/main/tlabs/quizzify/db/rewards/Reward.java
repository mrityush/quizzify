package com.src.main.tlabs.quizzify.db.rewards;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import layer1.layer2.generics.QuizzifyEntity;
import layer1.layer2.generics.Status;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.src.main.tlabs.quizzify.db.statusfields.RewardStatus;

@Entity
@Table(name = "rewards", uniqueConstraints = @UniqueConstraint(columnNames = { "item", "price" }), indexes = { @Index(columnList = "price", name = "idx_price") })
public class Reward implements QuizzifyEntity {

	public Reward(Builder b) {
		id = b.id;
		price = b.price;
		status = b.status;
		item = b.item;
		creationTime = b.creationTime;
		createdBy = b.createdBy;
		lastModificationTime = b.lastModificationTime;
		lastModifiedBy = b.lastModifiedBy;
	}

	public Reward() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "item", nullable = false)
	private String item;

	@Column(name = "price")
	private long price;

	@Column(name = "status")
	private RewardStatus status;

	@Column(name = "created_by", nullable = false)
	private long createdBy;

	@Column(name = "creation_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime creationTime;

	@Column(name = "last_modified_by", nullable = false)
	private long lastModifiedBy;

	@Column(name = "last_modification_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime lastModificationTime;

	public static class Builder {
		// id bigint
		// item varchar(255)
		// price double
		// status smallint
		// creation_time datetime
		// created_by bigint
		// last_modiification_time datetime
		// last_modiified_by bigint
		// primary key id
		private long id;
		private String item;
		private long price;
		private RewardStatus status;
		private long createdBy;
		private LocalDateTime creationTime;
		private long lastModifiedBy;
		private LocalDateTime lastModificationTime;

		public Reward build() {
			return new Reward(this);
		}

		public Builder id(long id) {
			this.id = id;
			return this;
		}

		public Builder price(long price) {
			this.price = price;
			return this;
		}

		public Builder status(RewardStatus status) {
			this.status = status;
			return this;
		}

		public Builder item(String item) {
			this.item = item;
			return this;
		}

		public Builder lastModifiedBy(long lastModifiedBy) {
			this.lastModifiedBy = lastModifiedBy;
			return this;
		}

		public Builder lastModificationTime(LocalDateTime lastModificationTime) {
			this.lastModificationTime = lastModificationTime;
			return this;
		}

		public Builder createdBy(long createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public Builder creationTime(LocalDateTime creationTime) {
			this.creationTime = creationTime;
			return this;
		}
	}

	public long id() {
		return id;
	}

	public long price() {
		return price;
	}

	public LocalDateTime creationTime() {
		return creationTime;
	}

	public long createdBy() {
		return createdBy;
	}

	public LocalDateTime lastModificationTime() {
		return lastModificationTime;
	}

	public long lastModifiedBy() {
		return lastModifiedBy;
	}

	public String item() {
		return item;
	}

	public static class Deserializer extends JsonDeserializer<Reward> {
		@Override
		public Reward deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			p.nextToken();
			final Reward communicationLinks = new Reward();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				communicationLinks.populateFromJsonString(field, p);
			}
			return communicationLinks;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeNumberField("id", id);
		g.writeNumberField("price", price);
		g.writeStringField("item", item);
		g.writeNumberField("status", status.id());
		g.writeNumberField("createdBy", createdBy);
		g.writeObjectField("creationTime", creationTime);
		g.writeNumberField("lastModifiedBy", lastModifiedBy);
		g.writeObjectField("lastModificationTime", lastModificationTime);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("id".equals(field)) {
			p.nextToken();
			id = p.readValueAs(long.class);
		} else if ("status".equals(field)) {
			p.nextToken();
			status = RewardStatus.valueOf(p.readValueAs(int.class));
		} else if ("item".equals(field)) {
			p.nextToken();
			item = p.readValueAs(String.class);
		} else if ("price".equals(field)) {
			p.nextToken();
			price = p.readValueAs(long.class);
		} else if ("createdBy".equals(field)) {
			p.nextToken();
			createdBy = p.readValueAs(long.class);
		} else if ("creationTime".equals(field)) {
			p.nextToken();
			creationTime = p.readValueAs(LocalDateTime.class);
		} else if ("lastModifiedBy".equals(field)) {
			p.nextToken();
			lastModifiedBy = p.readValueAs(long.class);
		} else if ("lastModificationTime".equals(field)) {
			p.nextToken();
			lastModificationTime = p.readValueAs(LocalDateTime.class);
		}
	}

	@Override
	public Status status() {
		return status;
	}

	@Override
	public void setStatus(Status status) {
		this.status = (RewardStatus) status;
	}
}