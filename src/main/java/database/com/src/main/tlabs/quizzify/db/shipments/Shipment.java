package com.src.main.tlabs.quizzify.db.shipments;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import layer1.layer2.generics.QuizzifyEntity;
import layer1.layer2.generics.Status;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.src.main.tlabs.quizzify.db.statusfields.ShipmentStatus;

@Entity
@Table(name = "shipments", uniqueConstraints = @UniqueConstraint(columnNames = { "reward_stat_id", "user_id" }), indexes = { @Index(columnList = "reward_stat_id,user_id", name = "idx_reward_stat_id_user_id") })
public class Shipment implements QuizzifyEntity {

	public Shipment(Builder b) {
		id = b.id;
		rewardStatId = b.rewardStatId;
		userAddressId = b.userAddressId;
		userId = b.userId;
		status = b.status;
		creationTime = b.creationTime;
		createdBy = b.createdBy;
		lastModificationTime = b.lastModificationTime;
		lastModifiedBy = b.lastModifiedBy;
	}

	public Shipment() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "reward_stat_id", nullable = false)
	private long rewardStatId;

	@Column(name = "user_id", nullable = false)
	private long userId;

	@Column(name = "status", nullable = false)
	private ShipmentStatus status;

	@Column(name = "user_address_id")
	private int userAddressId;

	@Column(name = "courier_id", nullable = false)
	private long courierId;

	@Column(name = "created_by", nullable = false)
	private long createdBy;

	@Column(name = "creation_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime creationTime;

	@Column(name = "last_modified_by", nullable = false)
	private long lastModifiedBy;

	@Column(name = "last_modification_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime lastModificationTime;

	public static class Builder {
		public int userAddressId;
		private long id;
		private long rewardStatId;
		private long userId;
		private ShipmentStatus status;
		private long createdBy;
		private LocalDateTime creationTime;
		private long lastModifiedBy;
		private LocalDateTime lastModificationTime;

		public Shipment build() {
			return new Shipment(this);
		}

		public Builder id(long id) {
			this.id = id;
			return this;
		}

		public Builder userAddressId(int userAddressId) {
			this.userAddressId = userAddressId;
			return this;
		}

		public Builder rewardStatId(long rewardStatId) {
			this.rewardStatId = rewardStatId;
			return this;
		}

		public Builder userId(long userId) {
			this.userId = userId;
			return this;
		}

		public Builder status(ShipmentStatus status) {
			this.status = status;
			return this;
		}

		public Builder lastModifiedBy(long lastModifiedBy) {
			this.lastModifiedBy = lastModifiedBy;
			return this;
		}

		public Builder lastModificationTime(LocalDateTime lastModificationTime) {
			this.lastModificationTime = lastModificationTime;
			return this;
		}

		public Builder createdBy(long createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public Builder creationTime(LocalDateTime creationTime) {
			this.creationTime = creationTime;
			return this;
		}
	}

	public long id() {
		return id;
	}

	public long rewardStatId() {
		return rewardStatId;
	}

	public long userId() {
		return userId;
	}

	public int userAddressId() {
		return userAddressId;
	}

	public LocalDateTime creationTime() {
		return creationTime;
	}

	public long createdBy() {
		return createdBy;
	}

	public LocalDateTime lastModificationTime() {
		return lastModificationTime;
	}

	public long lastModifiedBy() {
		return lastModifiedBy;
	}

	public static class Deserializer extends JsonDeserializer<Shipment> {
		@Override
		public Shipment deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			p.nextToken();
			final Shipment shipments = new Shipment();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				shipments.populateFromJsonString(field, p);
			}
			return shipments;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeNumberField("id", id);
		g.writeNumberField("userId", userId);
		g.writeNumberField("status", status.id());
		g.writeNumberField("rewardStatId", rewardStatId);
		g.writeNumberField("userAddressId", userAddressId);
		g.writeNumberField("createdBy", createdBy);
		g.writeObjectField("creationTime", creationTime);
		g.writeNumberField("lastModifiedBy", lastModifiedBy);
		g.writeObjectField("lastModificationTime", lastModificationTime);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("id".equals(field)) {
			p.nextToken();
			id = p.readValueAs(long.class);
		} else if ("status".equals(field)) {
			p.nextToken();
			status = ShipmentStatus.valueOf(p.readValueAs(int.class));
		} else if ("requestId".equals(field)) {
			p.nextToken();
			rewardStatId = p.readValueAs(long.class);
		} else if ("userAddressId".equals(field)) {
			p.nextToken();
			userAddressId = p.readValueAs(int.class);
		} else if ("createdBy".equals(field)) {
			p.nextToken();
			createdBy = p.readValueAs(long.class);
		} else if ("creationTime".equals(field)) {
			p.nextToken();
			creationTime = p.readValueAs(LocalDateTime.class);
		} else if ("lastModifiedBy".equals(field)) {
			p.nextToken();
			lastModifiedBy = p.readValueAs(long.class);
		} else if ("lastModificationTime".equals(field)) {
			p.nextToken();
			lastModificationTime = p.readValueAs(LocalDateTime.class);
		}
	}

	@Override
	public Status status() {
		return status;
	}

	@Override
	public void setStatus(Status status) {
		this.status = (ShipmentStatus) status;
	}
}