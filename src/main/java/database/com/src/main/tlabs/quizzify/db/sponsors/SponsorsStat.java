package com.src.main.tlabs.quizzify.db.sponsors;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import layer1.layer2.generics.QuizzifyEntity;
import layer1.layer2.generics.Status;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.src.main.tlabs.quizzify.db.statusfields.SponsorStatus;

@Entity
@Table(name = "sponsors_stats", uniqueConstraints = @UniqueConstraint(columnNames = { "sponsor_id", "quiz_id" }), indexes = { @Index(columnList = "sponsor_id,quiz_id", name = "idx_sponsor_id_quiz_id") })
public class SponsorsStat implements QuizzifyEntity {

	public SponsorsStat(Builder b) {
		id = b.id;
		shareAmount = b.shareAmount;
		sponsorId = b.sponsorId;
		quizId = b.quizId;
		status = b.status;
		creationTime = b.creationTime;
		createdBy = b.createdBy;
		lastModificationTime = b.lastModificationTime;
		lastModifiedBy = b.lastModifiedBy;
	}

	public SponsorsStat() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "share_amt_tmp", nullable = false)
	private String shareAmount;

	@Column(name = "sponsor_id")
	private long sponsorId;

	@Column(name = "status")
	private SponsorStatus status;

	@Column(name = "quiz_id")
	private long quizId;

	@Column(name = "created_by", nullable = false)
	private long createdBy;

	@Column(name = "creation_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime creationTime;

	@Column(name = "last_modified_by", nullable = false)
	private long lastModifiedBy;

	@Column(name = "last_modification_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime lastModificationTime;

	public static class Builder {
		private long id;
		private String shareAmount;
		private long sponsorId;
		private SponsorStatus status;
		private long quizId;
		private long createdBy;
		private LocalDateTime creationTime;
		private long lastModifiedBy;
		private LocalDateTime lastModificationTime;

		public SponsorsStat build() {
			return new SponsorsStat(this);
		}

		public Builder id(long id) {
			this.id = id;
			return this;
		}

		public Builder shareAmount(String shareAmount) {
			this.shareAmount = shareAmount;
			return this;
		}

		public Builder sponsorId(long sponsorId) {
			this.sponsorId = sponsorId;
			return this;
		}

		public Builder status(SponsorStatus status) {
			this.status = status;
			return this;
		}

		public Builder quizId(long quizId) {
			this.quizId = quizId;
			return this;
		}

		public Builder lastModifiedBy(long lastModifiedBy) {
			this.lastModifiedBy = lastModifiedBy;
			return this;
		}

		public Builder lastModificationTime(LocalDateTime lastModificationTime) {
			this.lastModificationTime = lastModificationTime;
			return this;
		}

		public Builder createdBy(long createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public Builder creationTime(LocalDateTime creationTime) {
			this.creationTime = creationTime;
			return this;
		}
	}

	public long id() {
		return id;
	}

	public String shareAmount() {
		return shareAmount;
	}

	public long sponsorId() {
		return sponsorId;
	}

	public long quizId() {
		return quizId;
	}

	public LocalDateTime creationTime() {
		return creationTime;
	}

	public long createdBy() {
		return createdBy;
	}

	public LocalDateTime lastModificationTime() {
		return lastModificationTime;
	}

	public long lastModifiedBy() {
		return lastModifiedBy;
	}

	public static class Deserializer extends JsonDeserializer<SponsorsStat> {
		@Override
		public SponsorsStat deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			p.nextToken();
			final SponsorsStat sponsorsStats = new SponsorsStat();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				sponsorsStats.populateFromJsonString(field, p);
			}
			return sponsorsStats;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeNumberField("id", id);
		g.writeNumberField("sponsorId", sponsorId);
		g.writeStringField("shareAmount", shareAmount);
		g.writeNumberField("quizId", quizId);
		g.writeNumberField("status", status.id());
		g.writeNumberField("createdBy", createdBy);
		g.writeObjectField("creationTime", creationTime);
		g.writeNumberField("lastModifiedBy", lastModifiedBy);
		g.writeObjectField("lastModificationTime", lastModificationTime);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("id".equals(field)) {
			p.nextToken();
			id = p.readValueAs(long.class);
		} else if ("quizId".equals(field)) {
			p.nextToken();
			quizId = p.readValueAs(long.class);
		} else if ("sponsorId".equals(field)) {
			p.nextToken();
			sponsorId = p.readValueAs(long.class);
		} else if ("status".equals(field)) {
			p.nextToken();
			status = SponsorStatus.valueOf(p.readValueAs(int.class));
		} else if ("shareAmount".equals(field)) {
			p.nextToken();
			shareAmount = p.readValueAs(String.class);
		} else if ("createdBy".equals(field)) {
			p.nextToken();
			createdBy = p.readValueAs(long.class);
		} else if ("creationTime".equals(field)) {
			p.nextToken();
			creationTime = p.readValueAs(LocalDateTime.class);
		} else if ("lastModifiedBy".equals(field)) {
			p.nextToken();
			lastModifiedBy = p.readValueAs(long.class);
		} else if ("lastModificationTime".equals(field)) {
			p.nextToken();
			lastModificationTime = p.readValueAs(LocalDateTime.class);
		}
	}

	@Override
	public Status status() {
		return status;
	}

	@Override
	public void setStatus(Status status) {
		this.status = (SponsorStatus) status;
	}
}