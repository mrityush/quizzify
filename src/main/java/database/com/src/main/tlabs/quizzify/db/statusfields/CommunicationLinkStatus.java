package com.src.main.tlabs.quizzify.db.statusfields;

import java.util.HashMap;
import java.util.Map;

import layer1.layer2.generics.Status;

public enum CommunicationLinkStatus implements Status {
	ACTIVE(1, "active") {
	},
	INACTIVE(2, "inactive") {
	},
	NEW(3, "new") {
	},
	NOT_ACKNOWLEDGED(4, "Not Acknowledged") {
	},
	ACKNOWLEDGED(5, "Acknowledged") {
	};

	@Override
	public int id() {
		return id;
	}

	@Override
	public String description() {
		return description;
	}

	private int id;
	private String description;

	private CommunicationLinkStatus(int id, String description) {
		this.id = id;
		this.description = description;
	}

	private static final Map<Integer, CommunicationLinkStatus> statuses = new HashMap<Integer, CommunicationLinkStatus>();

	static {
		for (CommunicationLinkStatus status : CommunicationLinkStatus.values()) {
			if (statuses.get(status.id) == null) {
				statuses.put(status.id, status);
			} else {
				// throw new BadPracticeException("Duplicate id: " + status.id);
			}
		}
	}

	public static CommunicationLinkStatus valueOf(int id) {
		return statuses.get(id);
	}

}
