package com.src.main.tlabs.quizzify.db.statusfields;

import java.util.HashMap;
import java.util.Map;

import layer1.layer2.generics.Status;

public enum QuizStatus implements Status {
	ACTIVE(1, "active") {
	},
	INACTIVE(2, "inactive") {
	};

	@Override
	public int id() {
		return id;
	}

	@Override
	public String description() {
		return description;
	}

	private int id;
	private String description;

	private QuizStatus(int id, String description) {
		this.id = id;
		this.description = description;
	}

	private static final Map<Integer, QuizStatus> statuses = new HashMap<Integer, QuizStatus>();

	static {
		for (QuizStatus status : QuizStatus.values()) {
			if (statuses.get(status.id) == null) {
				statuses.put(status.id, status);
			} else {
				// throw new BadPracticeException("Duplicate id: " + status.id);
			}
		}
	}

	public static QuizStatus valueOf(int id) {
		return statuses.get(id);
	}

}
