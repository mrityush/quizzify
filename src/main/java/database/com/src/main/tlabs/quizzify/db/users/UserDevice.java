package com.src.main.tlabs.quizzify.db.users;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import layer1.layer2.generics.QuizzifyEntity;
import layer1.layer2.generics.Status;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.src.main.dtos.users.UsersDeviceDTO;
import com.src.main.tlabs.quizzify.dao.manager.DaoManager;
import com.src.main.tlabs.quizzify.db.statusfields.UserDeviceStatus;

@Entity
@Table(name = "users_device", uniqueConstraints = @UniqueConstraint(columnNames = { "user_id", "device_id" }), indexes = { @Index(columnList = "status", name = "idx_status") })
public class UserDevice implements QuizzifyEntity {

	public UserDevice(Builder b) {
		id = b.id;
		userId = b.userId;
		lastAccessTime = b.lastAccessTime;
		status = b.status;
		osType = b.osType;
		appVersion = b.appVersion;
		osVersion = b.osVersion;
		deviceId = b.deviceId;
		deviceImei = b.deviceImei;
		gcmRegistrationId = b.gcmRegistrationId;
		creationTime = b.creationTime;
		createdBy = b.createdBy;
		lastModificationTime = b.lastModificationTime;
		lastModifiedBy = b.lastModifiedBy;
	}

	public UserDevice() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "os_type", nullable = true)
	private int osType;

	@Column(name = "app_version", nullable = true)
	private String appVersion;

	@Column(name = "os_version", nullable = true)
	private String osVersion;

	@Column(name = "device_id", nullable = true)
	private String deviceId;

	@Column(name = "device_imei", nullable = true)
	private String deviceImei;

	@Column(name = "gcm_registration_id", nullable = true)
	private String gcmRegistrationId;

	@Column(name = "user_id")
	private long userId;

	@Column(name = "status")
	private UserDeviceStatus status;

	@Column(name = "last_access_time")
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime lastAccessTime;

	@Column(name = "created_by", nullable = false)
	private long createdBy;

	@Column(name = "creation_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime creationTime;

	@Column(name = "last_modified_by", nullable = false)
	private long lastModifiedBy;

	@Column(name = "last_modification_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime lastModificationTime;

	public static class Builder {
		private long id;
		private int osType;
		private String appVersion;
		private String osVersion;
		private UserDeviceStatus status;
		private String deviceId;
		private String deviceImei;
		private String gcmRegistrationId;
		private long userId;
		private LocalDateTime lastAccessTime;
		private long createdBy;
		private LocalDateTime creationTime;
		private long lastModifiedBy;
		private LocalDateTime lastModificationTime;

		public UserDevice build() {
			return new UserDevice(this);
		}

		public Builder id(long id) {
			this.id = id;
			return this;
		}

		public Builder userId(long userId) {
			this.userId = userId;
			return this;
		}

		public Builder status(UserDeviceStatus status) {
			this.status = status;
			return this;
		}

		public Builder lastRetryTime(LocalDateTime lastRetryTime) {
			this.lastAccessTime = lastRetryTime;
			return this;
		}

		public Builder lastModifiedBy(long lastModifiedBy) {
			this.lastModifiedBy = lastModifiedBy;
			return this;
		}

		public Builder lastModificationTime(LocalDateTime lastModificationTime) {
			this.lastModificationTime = lastModificationTime;
			return this;
		}

		public Builder createdBy(long createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public Builder creationTime(LocalDateTime creationTime) {
			this.creationTime = creationTime;
			return this;
		}

		public Builder withUserDeviceDTO(UsersDeviceDTO userDeviceDto, long userIdPassed, int idPassed) {
			List<UserDevice> userDevices = DaoManager.userDao().getUserDevicesForUser(userIdPassed,
					UserDeviceStatus.ACTIVE);
			for (UserDevice userDevice : userDevices) {
				if (userDevice.deviceId().equals(userDeviceDto.deviceId())) {
					this.id = userDevice.id();
					break;
				}
			}
			LocalDateTime now = LocalDateTime.now();
			if (idPassed > 0) {
				this.id = idPassed;
			}
			this.userId = userIdPassed;
			this.osType = userDeviceDto.osType();
			this.appVersion = userDeviceDto.appVersion();
			this.osVersion = userDeviceDto.osVersion();
			this.deviceId = userDeviceDto.deviceId();
			this.gcmRegistrationId = userDeviceDto.gcmRegistrationId();
			this.status = UserDeviceStatus.ACTIVE;
			this.lastAccessTime = now;
			this.createdBy = User.SYSTEM_USER_ID;
			this.creationTime = now;
			this.lastModifiedBy = User.SYSTEM_USER_ID;
			this.lastModificationTime = now;
			return this;
		}
	}

	public long id() {
		return id;
	}

	public long userId() {
		return userId;
	}

	public int osType() {
		return osType;
	}

	public String osVersion() {
		return osVersion;
	}

	public String appVersion() {
		return appVersion;
	}

	public String gcmRegistrationId() {
		return gcmRegistrationId;
	}

	public String deviceImei() {
		return deviceImei;
	}

	public String deviceId() {
		return deviceId;
	}

	public LocalDateTime lastAccessTime() {
		return lastAccessTime;
	}

	public LocalDateTime creationTime() {
		return creationTime;
	}

	public long createdBy() {
		return createdBy;
	}

	public LocalDateTime lastModificationTime() {
		return lastModificationTime;
	}

	public long lastModifiedBy() {
		return lastModifiedBy;
	}

	public static class Deserializer extends JsonDeserializer<UserDevice> {
		@Override
		public UserDevice deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			p.nextToken();
			final UserDevice userDevice = new UserDevice();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				userDevice.populateFromJsonString(field, p);
			}
			return userDevice;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeNumberField("id", id);
		g.writeNumberField("userId", userId);
		g.writeNumberField("status", (status != null) ? status.id() : UserDeviceStatus.ACTIVE.id());
		g.writeObjectField("lastAccessTime", lastAccessTime);
		g.writeNumberField("osType", osType);
		g.writeStringField("appVersion", appVersion);
		g.writeObjectField("lastAccessTime", lastAccessTime);
		g.writeStringField("osVersion", osVersion);
		g.writeStringField("deviceId", deviceId);
		g.writeStringField("deviceImei", deviceImei);
		g.writeStringField("gcmRegistrationId", gcmRegistrationId);
		g.writeNumberField("createdBy", createdBy);
		g.writeObjectField("creationTime", creationTime);
		g.writeNumberField("lastModifiedBy", lastModifiedBy);
		g.writeObjectField("lastModificationTime", lastModificationTime);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("id".equals(field)) {
			p.nextToken();
			id = p.readValueAs(long.class);
		} else if ("status".equals(field)) {
			p.nextToken();
			status = UserDeviceStatus.valueOf(p.readValueAs(int.class));
		} else if ("appVersion".equals(field)) {
			p.nextToken();
			appVersion = p.readValueAs(String.class);
		} else if ("osVersion".equals(field)) {
			p.nextToken();
			osVersion = p.readValueAs(String.class);
		} else if ("deviceId".equals(field)) {
			p.nextToken();
			deviceId = p.readValueAs(String.class);
		} else if ("gcmRegistrationId".equals(field)) {
			p.nextToken();
			gcmRegistrationId = p.readValueAs(String.class);
		} else if ("deviceImei".equals(field)) {
			p.nextToken();
			deviceImei = p.readValueAs(String.class);
		} else if ("osType".equals(field)) {
			p.nextToken();
			osType = p.readValueAs(int.class);
		} else if ("createdBy".equals(field)) {
			p.nextToken();
			createdBy = p.readValueAs(long.class);
		} else if ("lastAccessTime".equals(field)) {
			p.nextToken();
			lastAccessTime = p.readValueAs(LocalDateTime.class);
		} else if ("creationTime".equals(field)) {
			p.nextToken();
			creationTime = p.readValueAs(LocalDateTime.class);
		} else if ("lastModifiedBy".equals(field)) {
			p.nextToken();
			lastModifiedBy = p.readValueAs(long.class);
		} else if ("lastModificationTime".equals(field)) {
			p.nextToken();
			lastModificationTime = p.readValueAs(LocalDateTime.class);
		}
	}

	@Override
	public Status status() {
		return status;
	}

	@Override
	public void setStatus(Status status) {
		this.status = (UserDeviceStatus) status;
	}
}