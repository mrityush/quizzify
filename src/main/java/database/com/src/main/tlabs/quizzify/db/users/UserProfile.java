package com.src.main.tlabs.quizzify.db.users;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import layer1.layer2.generics.QuizzifyEntity;
import layer1.layer2.generics.Status;
import layer1.layer2.serialise.JsonUtil;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.src.main.tlabs.quizzify.db.statusfields.UserStatus;

@Entity
@Table(name = "users_profile", uniqueConstraints = @UniqueConstraint(columnNames = { "user_id", "quiz_ids" }), indexes = { @Index(columnList = "user_id", name = "idx_user_id") })
public class UserProfile implements QuizzifyEntity {
	public UserProfile(Builder b) {
		id = b.id;
		userId = b.userId;
		quizIds = b.quizIds;
		quizIdsJson = b.quizIdsJson;
		status = b.status;
		contactNumber = b.contactNumber;
		address = b.address;
		addressJson = b.addressJson;
		creationTime = b.creationTime;
		createdBy = b.createdBy;
		lastModificationTime = b.lastModificationTime;
		lastModifiedBy = b.lastModifiedBy;
	}

	public UserProfile() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "user_id")
	private long userId;

	@Column(name = "status")
	private UserStatus status;

	@Column(name = "quiz_ids")
	private String quizIdsJson;

	@Column(name = "address")
	private String addressJson;

	@Column(name = "contact_no")
	private String contactNumber;

	@Column(name = "created_by", nullable = false)
	private long createdBy;

	@Column(name = "creation_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime creationTime;

	@Column(name = "last_modified_by", nullable = false)
	private long lastModifiedBy;

	@Column(name = "last_modification_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime lastModificationTime;

	@Transient
	private List<Long> quizIds;
	@Transient
	private List<String> address;

	public static class Builder {
		public String addressJson;
		public String quizIdsJson;
		public List<String> address;
		public String contactNumber;
		public List<Long> quizIds;
		public UserStatus status;
		private long id;
		private long userId;
		private long createdBy;
		private LocalDateTime creationTime;
		private long lastModifiedBy;
		private LocalDateTime lastModificationTime;

		public UserProfile build() {
			return new UserProfile(this);
		}

		public Builder id(long id) {
			this.id = id;
			return this;
		}

		public Builder contactNumber(String contactNumber) {
			this.contactNumber = contactNumber;
			return this;
		}

		public Builder quizIds(List<Long> quizIds) {
			this.quizIds = quizIds;
			this.quizIdsJson = JsonUtil.toJson(quizIds).get();
			return this;
		}

		public Builder address(List<String> address) {
			this.address = address;
			this.addressJson = JsonUtil.toJson(address).get();
			return this;
		}

		public Builder userId(long userId) {
			this.userId = userId;
			return this;
		}

		public Builder status(UserStatus status) {
			this.status = status;
			return this;
		}

		public Builder lastModifiedBy(long lastModifiedBy) {
			this.lastModifiedBy = lastModifiedBy;
			return this;
		}

		public Builder lastModificationTime(LocalDateTime lastModificationTime) {
			this.lastModificationTime = lastModificationTime;
			return this;
		}

		public Builder createdBy(long createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public Builder creationTime(LocalDateTime creationTime) {
			this.creationTime = creationTime;
			return this;
		}
	}

	public long id() {
		return id;
	}

	public long userId() {
		return userId;
	}

	public LocalDateTime creationTime() {
		return creationTime;
	}

	public long createdBy() {
		return createdBy;
	}

	public LocalDateTime lastModificationTime() {
		return lastModificationTime;
	}

	public long lastModifiedBy() {
		return lastModifiedBy;
	}

	public String contactNumber() {
		return contactNumber;
	}

	public List<String> address() {
		return address;
	}

	public List<Long> quizIds() {
		return quizIds;
	}

	public static class Deserializer extends JsonDeserializer<UserProfile> {
		@Override
		public UserProfile deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			p.nextToken();
			final UserProfile communicationLinks = new UserProfile();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				communicationLinks.populateFromJsonString(field, p);
			}
			return communicationLinks;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeNumberField("id", id);
		g.writeNumberField("userId", userId);
		g.writeObjectField("quizIds", quizIds);
		g.writeStringField("contactNumber", contactNumber);
		g.writeNumberField("status", status.id());
		g.writeObjectField("address", address);
		g.writeNumberField("createdBy", createdBy);
		g.writeObjectField("creationTime", creationTime);
		g.writeNumberField("lastModifiedBy", lastModifiedBy);
		g.writeObjectField("lastModificationTime", lastModificationTime);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("id".equals(field)) {
			p.nextToken();
			id = p.readValueAs(long.class);
		} else if ("userId".equals(field)) {
			p.nextToken();
			userId = p.readValueAs(long.class);
		} else if ("quizIds".equals(field)) {
			p.nextToken();
			quizIds = p.readValueAs(new TypeReference<List<Long>>() {
			});
		} else if ("status".equals(field)) {
			p.nextToken();
			status = UserStatus.valueOf(p.readValueAs(int.class));
		} else if ("contactNumber".equals(field)) {
			p.nextToken();
			contactNumber = p.readValueAs(String.class);
		} else if ("address".equals(field)) {
			p.nextToken();
			address = p.readValueAs(new TypeReference<List<String>>() {
			});
		} else if ("createdBy".equals(field)) {
			p.nextToken();
			createdBy = p.readValueAs(long.class);
		} else if ("creationTime".equals(field)) {
			p.nextToken();
			creationTime = p.readValueAs(LocalDateTime.class);
		} else if ("lastModifiedBy".equals(field)) {
			p.nextToken();
			lastModifiedBy = p.readValueAs(long.class);
		} else if ("lastModificationTime".equals(field)) {
			p.nextToken();
			lastModificationTime = p.readValueAs(LocalDateTime.class);
		}
	}

	@Override
	public Status status() {
		return status;
	}

	@Override
	public void setStatus(Status status) {
		this.status = (UserStatus) status;
	}
}