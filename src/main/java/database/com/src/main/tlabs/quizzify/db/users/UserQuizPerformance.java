package com.src.main.tlabs.quizzify.db.users;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import layer1.layer2.generics.QuizzifyEntity;
import layer1.layer2.generics.Status;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.src.main.tlabs.quizzify.db.statusfields.UserStatus;

@Entity
@Table(name = "user_quiz_performance", uniqueConstraints = @UniqueConstraint(columnNames = { "user_id", "quiz_id" }), indexes = { @Index(columnList = "user_id,quiz_id", name = "idx_user_id_quiz_id") })
public class UserQuizPerformance implements QuizzifyEntity {

	public UserQuizPerformance(Builder b) {
		id = b.id;
		userId = b.userId;
		quizId = b.quizId;
		setId = b.setId;
		status = b.status;
		answerStats = b.answerStats;
		score = b.score;
		submitDuration = b.submitDuration;
		creationTime = b.creationTime;
		createdBy = b.createdBy;
		lastModificationTime = b.lastModificationTime;
		lastModifiedBy = b.lastModifiedBy;
	}

	public UserQuizPerformance() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private long id;

	@Column(name = "user_id")
	private long userId;

	@Column(name = "status")
	private UserStatus status;

	@Column(name = "quiz_id")
	private long quizId;

	@Column(name = "set_id")
	private int setId;

	@Column(name = "submit_duration")
	private BigDecimal submitDuration;

	@Column(name = "answer_stats")
	private String answerStats;

	@Column(name = "score")
	private double score;

	@Column(name = "created_by", nullable = false)
	private long createdBy;

	@Column(name = "creation_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime creationTime;

	@Column(name = "last_modified_by", nullable = false)
	private long lastModifiedBy;

	@Column(name = "last_modification_time", nullable = false)
	@Type(type = "layer1.layer2.time.LocalDateTimeUserType")
	private LocalDateTime lastModificationTime;

	public static class Builder {
		private long id;
		private long userId;
		public double score;
		public int setId;
		public BigDecimal submitDuration;
		public String answerStats;
		public long quizId;
		public UserStatus status;
		private long createdBy;
		private LocalDateTime creationTime;
		private long lastModifiedBy;
		private LocalDateTime lastModificationTime;

		public UserQuizPerformance build() {
			return new UserQuizPerformance(this);
		}

		public Builder id(long id) {
			this.id = id;
			return this;
		}

		public Builder contactNumber(String contactNumber) {
			this.answerStats = contactNumber;
			return this;
		}

		public Builder quizId(long quizId) {
			this.quizId = quizId;
			return this;
		}

		public Builder answerStats(String answerStats) {
			this.answerStats = answerStats;
			return this;
		}

		public Builder submitDuration(BigDecimal submitDuration) {
			this.submitDuration = submitDuration;
			return this;
		}

		public Builder userId(long userId) {
			this.userId = userId;
			return this;
		}

		public Builder score(double score) {
			this.score = score;
			return this;
		}

		public Builder setId(int setId) {
			this.setId = setId;
			return this;
		}

		public Builder status(UserStatus status) {
			this.status = status;
			return this;
		}

		public Builder lastModifiedBy(long lastModifiedBy) {
			this.lastModifiedBy = lastModifiedBy;
			return this;
		}

		public Builder lastModificationTime(LocalDateTime lastModificationTime) {
			this.lastModificationTime = lastModificationTime;
			return this;
		}

		public Builder createdBy(long createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public Builder creationTime(LocalDateTime creationTime) {
			this.creationTime = creationTime;
			return this;
		}
	}

	public long id() {
		return id;
	}

	public long userId() {
		return userId;
	}

	public int setId() {
		return setId;
	}

	public BigDecimal submitDuration() {
		return submitDuration;
	}

	public LocalDateTime creationTime() {
		return creationTime;
	}

	public long createdBy() {
		return createdBy;
	}

	public LocalDateTime lastModificationTime() {
		return lastModificationTime;
	}

	public long lastModifiedBy() {
		return lastModifiedBy;
	}

	public String answerStats() {
		return answerStats;
	}

	public long quizId() {
		return quizId;
	}

	public static class Deserializer extends JsonDeserializer<UserQuizPerformance> {
		@Override
		public UserQuizPerformance deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
				JsonProcessingException {
			p.nextToken();
			String field = p.getCurrentName();
			p.nextToken();
			final UserQuizPerformance usersQuizPerformance = new UserQuizPerformance();
			while (p.getCurrentToken() != JsonToken.END_OBJECT) {
				p.nextToken();
				field = p.getCurrentName();
				usersQuizPerformance.populateFromJsonString(field, p);
			}
			return usersQuizPerformance;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeNumberField("id", id);
		g.writeNumberField("userId", userId);
		g.writeObjectField("quizId", quizId);
		g.writeStringField("answerStats", answerStats);
		g.writeNumberField("status", status.id());
		g.writeNumberField("setId", setId);
		g.writeNumberField("score", score);
		g.writeNumberField("submitDuration", submitDuration);
		g.writeNumberField("createdBy", createdBy);
		g.writeObjectField("creationTime", creationTime);
		g.writeNumberField("lastModifiedBy", lastModifiedBy);
		g.writeObjectField("lastModificationTime", lastModificationTime);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		if ("id".equals(field)) {
			p.nextToken();
			id = p.readValueAs(long.class);
		} else if ("userId".equals(field)) {
			p.nextToken();
			userId = p.readValueAs(long.class);
		} else if ("quizId".equals(field)) {
			p.nextToken();
			quizId = p.readValueAs(long.class);
		} else if ("answerStats".equals(field)) {
			p.nextToken();
			answerStats = p.readValueAs(String.class);
		} else if ("score".equals(field)) {
			p.nextToken();
			score = p.readValueAs(double.class);
		} else if ("setId".equals(field)) {
			p.nextToken();
			setId = p.readValueAs(int.class);
		} else if ("status".equals(field)) {
			p.nextToken();
			status = UserStatus.valueOf(p.readValueAs(int.class));
		} else if ("submitDuration".equals(field)) {
			p.nextToken();
			submitDuration = p.readValueAs(BigDecimal.class);
		} else if ("createdBy".equals(field)) {
			p.nextToken();
			createdBy = p.readValueAs(long.class);
		} else if ("creationTime".equals(field)) {
			p.nextToken();
			creationTime = p.readValueAs(LocalDateTime.class);
		} else if ("lastModifiedBy".equals(field)) {
			p.nextToken();
			lastModifiedBy = p.readValueAs(long.class);
		} else if ("lastModificationTime".equals(field)) {
			p.nextToken();
			lastModificationTime = p.readValueAs(LocalDateTime.class);
		}
	}

	@Override
	public Status status() {
		return status;
	}

	@Override
	public void setStatus(Status status) {
		this.status = (UserStatus) status;
	}
}