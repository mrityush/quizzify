package com.src.main.tlabs.quizzify.dbops.manager;

import com.src.main.tlabs.quizzify.db.communications.ExternalCommunicationLink;

public interface CommunicationsService {

	void updateExternalCommunicationLinkAsDispatched(ExternalCommunicationLink link);

}
