package com.src.main.tlabs.quizzify.dbops.manager;

import java.util.ArrayList;
import java.util.List;

import layer1.layer2.generics.QuizzifyEntity;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface DatabaseOps<T extends QuizzifyEntity> {
	static Logger logger = LoggerFactory.getLogger(DatabaseOps.class);

	/**
	 * Adds item to database
	 * 
	 * @param entity
	 * @param session
	 * @return
	 */
	default boolean addItem(QuizzifyEntity entity, Session session) {
		boolean success = false;
		try {
			session.beginTransaction();
			session.save(entity);
			session.getTransaction().commit();
			success = true;
		} catch (Exception e) {
			success = false;
			logger.error("Error in adding Entity : {}", e.toString());
		}
		return success;
	}

	/**
	 * Hard deletes the entity.
	 * 
	 * @param entity
	 * @param session
	 * @return
	 */
	default boolean deleteItem(QuizzifyEntity entity, Session session) {
		boolean success = false;
		try {
			session.beginTransaction();
			session.delete(entity);
			session.getTransaction().commit();
			success = true;
		} catch (Exception e) {
			success = false;
			logger.error("Error in Deleting Entity : {}", e.toString());
		}
		return success;
	}

	@SuppressWarnings("unchecked")
	default T getFirstItemById(Class<T> passedClass, long id, Session session) {
		List<T> entities = new ArrayList<T>();
		T item = null;
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Criteria cr = session.createCriteria(passedClass);
			cr.add(Restrictions.eq("id", id));
			entities = (List<T>) cr.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error("Error in fetching entity: error = {}", e.toString());
		} finally {
			session.close();
		}
		if (entities.size() > 0) {
			item = entities.get(0);
		}
		return item;

	}

	default List<T> getItemsListById(Class<T> passedClass, long id, Session session) {
		// List<T> entities = new ArrayList<T>();
		List<T> entities = getNumberExactMatchFetchResults(passedClass, "id", id, session);
		// Transaction tx = null;
		// try {
		// tx = session.beginTransaction();
		// Criteria cr = session.createCriteria(passedClass);
		// cr.add(Restrictions.eq("id", id));
		// entities = (List<T>) cr.list();
		// tx.commit();
		// } catch (HibernateException e) {
		// if (tx != null)
		// tx.rollback();
		// logger.error("Error in fetching entity: error = {}", e.toString());
		// } finally {
		// session.close();
		// }
		return entities;

	}

	default List<T> getExactMatchingItemByProperty(Class<T> passedClass, String columnName, String value,
			Session session) {
		List<T> entities = getStringExactMatchFetchResults(passedClass, columnName, value, session);
		return entities;

	}

	@SuppressWarnings("unchecked")
	default List<T> getStringExactMatchFetchResults(Class<T> passedClass, String columnName, String value,
			Session session) {
		List<T> entities = new ArrayList<T>();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Criteria cr = session.createCriteria(passedClass);
			cr.add(Restrictions.eq(columnName, value));
			entities = (List<T>) cr.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error("Error in fetching entity: error = {}", e.toString());
		} finally {
			session.close();
		}
		return entities;
	}

	@SuppressWarnings("unchecked")
	default List<T> getNumberExactMatchFetchResults(Class<T> passedClass, String columnName, long value, Session session) {
		List<T> entities = new ArrayList<T>();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Criteria cr = session.createCriteria(passedClass);
			cr.add(Restrictions.eq(columnName, value));
			entities = (List<T>) cr.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.error("Error in fetching entity: error = {}", e.toString());
		} finally {
			session.close();
		}
		return entities;
	}

	/**
	 * Updates the entity in db
	 * 
	 * @param entity
	 * @param session
	 * @return
	 */
	default boolean updateItem(QuizzifyEntity entity, Session session) {
		boolean success = false;
		try {
			session.beginTransaction();
			session.saveOrUpdate(entity);
			session.getTransaction().commit();
			success = true;
		} catch (Exception e) {
			success = false;
			logger.error("Error in Updating Entity : {}", e.toString());
		}
		return success;
	}

}
