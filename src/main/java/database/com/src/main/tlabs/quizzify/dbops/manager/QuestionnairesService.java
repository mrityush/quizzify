package com.src.main.tlabs.quizzify.dbops.manager;

import com.src.main.tlabs.quizzify.db.questionnaires.Question;

public interface QuestionnairesService {

	public boolean addQuestion(Question question);
}
