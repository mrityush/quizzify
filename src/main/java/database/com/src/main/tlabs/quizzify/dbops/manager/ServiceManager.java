package com.src.main.tlabs.quizzify.dbops.manager;

import com.src.main.tlabs.quizzify.dbops.communications.CommunicationsDefaultService;
import com.src.main.tlabs.quizzify.dbops.emails.EmailsDefaultService;
import com.src.main.tlabs.quizzify.dbops.questionnaires.QuestionnairesDefaultService;
import com.src.main.tlabs.quizzify.dbops.rewards.RewardsDefaultService;
import com.src.main.tlabs.quizzify.dbops.shipments.ShipmentsDefaultService;
import com.src.main.tlabs.quizzify.dbops.sponsors.SponsorsDefaultService;
import com.src.main.tlabs.quizzify.dbops.users.UsersDefaultService;

public class ServiceManager {
	private static CommunicationsService communicationsService = new CommunicationsDefaultService();
	private static EmailsService emailsService = new EmailsDefaultService();
	private static QuestionnairesService questionnairesService = new QuestionnairesDefaultService();
	private static RewardsService rewardsService = new RewardsDefaultService();
	private static ShipmentsService shipmentsService = new ShipmentsDefaultService();
	private static SponsorsService sponsorsService = new SponsorsDefaultService();
	private static UsersService usersService = new UsersDefaultService();

	public static CommunicationsService communicationsService() {
		return communicationsService;
	}

	public static EmailsService emailsService() {
		return emailsService;
	}

	public static QuestionnairesService questionnairesService() {
		return questionnairesService;
	}

	public static RewardsService rewardsService() {
		return rewardsService;
	}

	public static ShipmentsService shipmentsService() {
		return shipmentsService;
	}

	public static SponsorsService sponsorsService() {
		return sponsorsService;
	}

	public static UsersService usersService() {
		return usersService;
	}
}
