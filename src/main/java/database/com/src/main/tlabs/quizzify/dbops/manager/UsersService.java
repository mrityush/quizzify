package com.src.main.tlabs.quizzify.dbops.manager;

import com.src.main.dtos.users.UsersDeviceDTO;
import com.src.main.tlabs.quizzify.db.users.User;

public interface UsersService {

	boolean generateAndSendOtp(User user, UsersDeviceDTO userDeviceDto);

}
