package com.src.main.tlabs.quizzify.dbops.questionnaires;

import com.src.main.tlabs.quizzify.dao.manager.DaoManager;
import com.src.main.tlabs.quizzify.db.questionnaires.Question;
import com.src.main.tlabs.quizzify.dbops.manager.QuestionnairesService;

public class QuestionnairesDefaultService implements QuestionnairesService {

	@Override
	public boolean addQuestion(Question question) {
		boolean result = DaoManager.questionnaireDao().addQuestion(question);
		return result;
	}

}
