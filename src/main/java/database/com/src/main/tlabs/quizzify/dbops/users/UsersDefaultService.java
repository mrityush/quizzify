package com.src.main.tlabs.quizzify.dbops.users;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import layer1.layer2.generics.CryptoUtil;
import layer1.layer2.serialise.JsonUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.src.main.comm.request.external.gcm.GcmCcsSendOTPRequest;
import com.src.main.dtos.users.UsersDeviceDTO;
import com.src.main.tlabs.quizzify.dao.manager.DaoManager;
import com.src.main.tlabs.quizzify.db.communications.ExternalCommunicationLink;
import com.src.main.tlabs.quizzify.db.statusfields.CommunicationLinkStatus;
import com.src.main.tlabs.quizzify.db.statusfields.UserDeviceStatus;
import com.src.main.tlabs.quizzify.db.statusfields.UserStatus;
import com.src.main.tlabs.quizzify.db.users.User;
import com.src.main.tlabs.quizzify.db.users.UserDevice;
import com.src.main.tlabs.quizzify.db.users.UserOTP;
import com.src.main.tlabs.quizzify.dbops.manager.DatabaseOps;
import com.src.main.tlabs.quizzify.dbops.manager.UsersService;

public class UsersDefaultService implements UsersService {
	static Logger logger = LoggerFactory.getLogger(DatabaseOps.class);

	@Override
	public boolean generateAndSendOtp(User user, UsersDeviceDTO userDeviceDto) {
		boolean success = true;
		String otp = CryptoUtil.generateRandomNumber() + "";
		// String userOtp = DaoManager.userDao().getUserOTPByUserId(user.id());
		UserOTP userOTPObj = DaoManager.userDao().getUserOTPByUserId(user.id());
		;
		LocalDateTime now = LocalDateTime.now();
		//
		if (userDeviceDto != null) {
			UserDevice userDevice = new UserDevice.Builder().withUserDeviceDTO(userDeviceDto, user.id(), 0).build();
			success = success && DaoManager.userDao().updateUsersDevice(userDevice);
			logger.debug("User Device updated? {}", success);
		}
		//
		if (userOTPObj.otp() == null) {
			UserOTP.Builder builder = new UserOTP.Builder();
			builder.userId(user.id()).otp(otp).status(UserStatus.ACTIVE).createdBy(User.SYSTEM_USER_ID)
					.creationTime(now).lastModifiedBy(User.SYSTEM_USER_ID).lastModificationTime(now);
			userOTPObj = builder.build();
			success = DaoManager.userDao().addUsersOtp(userOTPObj);
			logger.debug("Generated new OTP for userId: {} with success: {}", user.id(), success);
		} else if (now.isAfter(userOTPObj.lastModificationTime().plusMinutes(15))) {
			userOTPObj.setOTPPasscode(otp);
			userOTPObj.setLastModifiedBy(User.SYSTEM_USER_ID);
			success = DaoManager.userDao().updateUsersOtp(userOTPObj);
			logger.debug("Updated OTP for userId: {} with success: {}", user.id(), success);
		} else {
			otp = userOTPObj.otp();
		}
		//
		ExternalCommunicationLink.Builder linkBuilder = new ExternalCommunicationLink.Builder().maxAttempts(1)
				.attempts(0).createdBy(User.SYSTEM_USER_ID).lastModifiedBy(User.SYSTEM_USER_ID)
				.status(CommunicationLinkStatus.NEW).userId(user.id()).executionTime(now).lastRetryTime(now);
		GcmCcsSendOTPRequest.Builder requestBuilder = new GcmCcsSendOTPRequest.Builder().deliveryReceiptRequested(true)
				.otp(otp);
		UserDevice userDevice = getUserDevice(user.id(), userDeviceDto.deviceId());
		GcmCcsSendOTPRequest request = requestBuilder.requestId(UUID.randomUUID())
				.registrationId(userDevice.gcmRegistrationId()).build();
		linkBuilder.requestId(request.requestId().toString()).request(JsonUtil.toJson(request).get()).creationTime(now)
				.lastModificationTime(now);
		ExternalCommunicationLink link = linkBuilder.build();
		// link.setPersistInDB(true);
		boolean added = DaoManager.communicationDao().addExternalCommunicationLink(link);
		// if (added) {
		// try (Jedis connection = RedisDatabaseManager.JOB.getConnection()) {
		// ExternalCommunicationTask task = new ExternalCommunicationTask(link);
		// TaskExecutor.instance().submitForFuture(task, now);
		// }
		// }
		return success;
	}

	private UserDevice getUserDevice(long userId, String deviceId) {
		List<UserDevice> userDevices = DaoManager.userDao().getUserDevicesForUser(userId, UserDeviceStatus.ACTIVE);
		for (UserDevice userDevice : userDevices) {
			if (userDevice.deviceId().equals(deviceId)) {
				return userDevice;
			}
		}
		return null;
	}

}
