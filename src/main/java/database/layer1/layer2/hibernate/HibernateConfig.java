package layer1.layer2.hibernate;

import java.io.FileInputStream;
import java.util.Properties;

import layer1.layer2.config.BaseConfig;
import layer1.layer2.config.ConfigResources;

public class HibernateConfig {
	public static final String PROP_COMMUNICATIONS_CONFIG_FILE = "COMMUNICATIONS_CONFIG_FILE";
	public static final String PROP_EMAILS_CONFIG_FILE = "EMAILS_CONFIG_FILE";
	public static final String PROP_QUESTIONNAIRES_CONFIG_FILE = "QUESTIONNAIRES_CONFIG_FILE";
	public static final String PROP_REWARDS_CONFIG_FILE = "REWARDS_CONFIG_FILE";
	public static final String PROP_SHIPMENTS_CONFIG_FILE = "SHIPMENTS_CONFIG_FILE";
	public static final String PROP_SPONSORS_CONFIG_FILE = "SPONSORS_CONFIG_FILE";
	public static final String PROP_TEST_CONFIG_FILE = "TEST_CONFIG_FILE";
	public static final String PROP_USERS_CONFIG_FILE = "USERS_CONFIG_FILE";

	public static String COMMUNICATIONS_CONFIG_FILE;
	public static String EMAILS_CONFIG_FILE;
	public static String QUESTIONNAIRES_CONFIG_FILE;
	public static String REWARDS_CONFIG_FILE;
	public static String SHIPMENTS_CONFIG_FILE;
	public static String SPONSORS_CONFIG_FILE;
	public static String TEST_CONFIG_FILE;
	public static String USERS_CONFIG_FILE;

	public static synchronized boolean initialize() {
		try {
			FileInputStream fis = new FileInputStream(BaseConfig.resourcesBasePath()
					+ ConfigResources.hibernateXmlFileName());
			Properties prop = new Properties();
			prop.load(fis);
			COMMUNICATIONS_CONFIG_FILE = prop.getProperty(PROP_COMMUNICATIONS_CONFIG_FILE).trim().toLowerCase();
			EMAILS_CONFIG_FILE = prop.getProperty(PROP_EMAILS_CONFIG_FILE).trim().toLowerCase();
			QUESTIONNAIRES_CONFIG_FILE = prop.getProperty(PROP_QUESTIONNAIRES_CONFIG_FILE).trim().toLowerCase();
			REWARDS_CONFIG_FILE = prop.getProperty(PROP_REWARDS_CONFIG_FILE).trim().toLowerCase();
			SHIPMENTS_CONFIG_FILE = prop.getProperty(PROP_SHIPMENTS_CONFIG_FILE).trim().toLowerCase();
			SPONSORS_CONFIG_FILE = prop.getProperty(PROP_SPONSORS_CONFIG_FILE).trim().toLowerCase();
			TEST_CONFIG_FILE = prop.getProperty(PROP_TEST_CONFIG_FILE).trim().toLowerCase();
			USERS_CONFIG_FILE = prop.getProperty(PROP_USERS_CONFIG_FILE).trim().toLowerCase();
			return true;
		} catch (Exception e) {
			throw new RuntimeException("Error initializing System Config", e);
		}
	}

}
