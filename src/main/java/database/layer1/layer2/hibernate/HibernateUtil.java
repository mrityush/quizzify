package layer1.layer2.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.src.main.tlabs.quizzify.db.manager.DatabaseManager;

public class HibernateUtil {
	private static SessionFactory testSessionFactory;
	private static SessionFactory usersSessionFactory;
	private static SessionFactory questionnairesSessionFactory;
	private static SessionFactory rewardsSessionFactory;
	private static SessionFactory communicationsSessionFactory;
	private static SessionFactory sponsorsSessionFactory;
	private static SessionFactory shipmentsSessionFactory;
	private static SessionFactory emailsSessionFactory;

	static {
		for (DatabaseManager dbManager : DatabaseManager.values()) {
			dbManager.configureXml();
		}
		createTestSessionFactory();
		createUsersSessionFactory();
		createQuestionnairesSessionFactory();
		createRewardsSessionFactory();
		createCommunicationsSessionFactory();
		createSponsorsSessionFactory();
		createShipmentsSessionFactory();
		createEmailsSessionFactory();

	}

	public static SessionFactory getTestSessionFactory() {
		return testSessionFactory;
	}

	private static void createEmailsSessionFactory() {
		Configuration configuration = new Configuration().configure(DatabaseManager.EMAILS.configXml());
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration
				.getProperties());
		emailsSessionFactory = configuration.buildSessionFactory(builder.build());
	}

	private static void createShipmentsSessionFactory() {
		Configuration configuration = new Configuration().configure(DatabaseManager.SHIPMENTS.configXml());
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration
				.getProperties());
		shipmentsSessionFactory = configuration.buildSessionFactory(builder.build());
	}

	private static void createSponsorsSessionFactory() {
		Configuration configuration = new Configuration().configure(DatabaseManager.SPONSORS.configXml());
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration
				.getProperties());
		sponsorsSessionFactory = configuration.buildSessionFactory(builder.build());
	}

	private static void createCommunicationsSessionFactory() {
		Configuration configuration = new Configuration().configure(DatabaseManager.COMMUNICATIONS.configXml());
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration
				.getProperties());
		communicationsSessionFactory = configuration.buildSessionFactory(builder.build());
	}

	private static void createRewardsSessionFactory() {
		Configuration configuration = new Configuration().configure(DatabaseManager.REWARDS.configXml());
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration
				.getProperties());
		rewardsSessionFactory = configuration.buildSessionFactory(builder.build());
	}

	private static void createQuestionnairesSessionFactory() {
		Configuration configuration = new Configuration().configure(DatabaseManager.QUESTIONNAIRES.configXml());
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration
				.getProperties());
		questionnairesSessionFactory = configuration.buildSessionFactory(builder.build());
	}

	private static void createUsersSessionFactory() {
		Configuration configuration = new Configuration().configure(DatabaseManager.USERS.configXml());
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration
				.getProperties());
		usersSessionFactory = configuration.buildSessionFactory(builder.build());
	}

	private static void createTestSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration
				.getProperties());
		testSessionFactory = configuration.buildSessionFactory(builder.build());
	}

	public static SessionFactory getUsersSessionFactory() {
		return usersSessionFactory;
	}

	public static SessionFactory getQuestionnairesSessionFactory() {
		return questionnairesSessionFactory;
	}

	public static SessionFactory getRewardsSessionFactory() {
		return rewardsSessionFactory;
	}

	public static SessionFactory getCommunicationsSessionFactory() {
		return communicationsSessionFactory;
	}

	public static SessionFactory getSponsorsSessionFactory() {
		return sponsorsSessionFactory;
	}

	public static SessionFactory getShipmentsSessionFactory() {
		return shipmentsSessionFactory;
	}

	public static SessionFactory getEmailsSessionFactory() {
		return emailsSessionFactory;
	}
}