package com.src.main.dtos.users;

import java.io.IOException;

import layer1.layer2.serialise.JsonSerialisable;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class UsersDeviceDTO implements JsonSerialisable {
	private int osType;
	private String appVersion;
	private String osVersion;
	private String deviceId;
	private String deviceIMEI;
	private String gcmRegistrationId;
	private String androidAdId;
	private String channelId;

	public UsersDeviceDTO() {

	}

	public UsersDeviceDTO(int osType, String appVersion, String osVersion, String deviceId, String deviceIMEI,
			String gcmRegistrationId, String androidAdId, String channelId) {

		this.osType = osType;
		this.appVersion = appVersion;
		this.osVersion = osVersion;
		this.deviceId = deviceId;
		this.deviceIMEI = deviceIMEI;
		this.gcmRegistrationId = gcmRegistrationId;
		this.androidAdId = androidAdId;
		this.channelId = channelId;

	}

	public static class Deserializer extends JsonDeserializer<UsersDeviceDTO> {
		@Override
		public UsersDeviceDTO deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			final UsersDeviceDTO instance = new UsersDeviceDTO();
			while (p.nextToken() != JsonToken.END_OBJECT) {
				if (p.getCurrentToken() == JsonToken.FIELD_NAME) {
					String field = p.getCurrentName();
					instance.populateFromJsonString(field, p);
				}
			}
			return instance;
		}
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		g.writeNumberField("osType", osType);
		g.writeStringField("appVersion", appVersion);
		g.writeStringField("osVersion", osVersion);
		g.writeStringField("deviceId", deviceId);
		g.writeStringField("deviceIMEI", deviceIMEI);
		g.writeObjectField("gcmRegistrationId", gcmRegistrationId);
		g.writeStringField("androidAdId", androidAdId);
		g.writeStringField("channelId", channelId);
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		switch (field) {
		case "osType":
			p.nextToken();
			osType = p.readValueAs(int.class);
			break;
		case "appVersion":
			p.nextToken();
			appVersion = p.readValueAs(String.class);
			break;
		case "osVersion":
			p.nextToken();
			osVersion = p.readValueAs(String.class);
			break;
		case "deviceId":
			p.nextToken();
			deviceId = p.readValueAs(String.class);
			break;
		case "deviceIMEI":
			p.nextToken();
			deviceIMEI = p.readValueAs(String.class);
			break;
		case "gcmRegistrationId":
			p.nextToken();
			gcmRegistrationId = p.readValueAs(String.class);
			break;
		case "androidAdId":
			p.nextToken();
			androidAdId = p.readValueAs(String.class);
			break;
		case "channelId":
			p.nextToken();
			channelId = p.readValueAs(String.class);
			break;
		}
	}

	public int osType() {
		return osType;
	}

	// public MobileOsType osType() {
	// return osType;
	// }

	public String appVersion() {
		return appVersion;
	}

	public String osVersion() {
		return osVersion;
	}

	public String deviceId() {
		return deviceId;
	}

	public String deviceIMEI() {
		return deviceIMEI;
	}

	public String gcmRegistrationId() {
		return gcmRegistrationId;
	}

	public String androidAdId() {
		return androidAdId;
	}

	public String channelId() {
		return channelId;
	}

	@Override
	public String toString() {
		return serializeAsJson().orElse(super.toString());
	}
}
