package layer1.layer2.config;

import java.io.InputStream;
import java.util.Properties;

public class BaseConfig {
	private static final String PROP_RESOURCES_BASE_PATH = "RESOURCES_BASE_PATH";
	private static String resourcesBasePath;

	public static synchronized boolean initialize() {
		try (InputStream is = BaseConfig.class.getClassLoader()
				.getResourceAsStream(ConfigResources.basePropertiesFileName())) {
			Properties prop = new Properties();
			prop.load(is);
			resourcesBasePath = prop.getProperty(PROP_RESOURCES_BASE_PATH)
					.trim();
			return true;
		} catch (Exception e) {
			throw new RuntimeException("Error initializing Base Config", e);
		}
	}

	public static String resourcesBasePath() {
		return resourcesBasePath;
	}
}
