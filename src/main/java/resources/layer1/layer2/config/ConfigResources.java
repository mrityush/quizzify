package layer1.layer2.config;

/**
 * Configuration Object for the Application.
 * 
 * @author mrityunjya.shukla
 *
 */
public abstract class ConfigResources {
	private static final String BASE_PROPERTIES_FILE_NAME = "base.properties";
	private static final String SYSTEM_PROPERTIES_FILE_NAME = "system.properties";
	private static final String HIBERNATE_XML_FILE_NAME = "hibernatedb.properties";
	private static final String EXTERNAL_PROPERTIES_FILE_NAME = "external.properties";

	public static String hibernateXmlFileName() {
		return HIBERNATE_XML_FILE_NAME;
	}

	public static String basePropertiesFileName() {
		return BASE_PROPERTIES_FILE_NAME;
	}

	public static String systemPropertiesFileName() {
		return SYSTEM_PROPERTIES_FILE_NAME;
	}

	public static String externalPropertiesFileName() {
		return EXTERNAL_PROPERTIES_FILE_NAME;
	}

}
