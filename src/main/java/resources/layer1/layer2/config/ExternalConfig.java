package layer1.layer2.config;

import java.io.FileInputStream;
import java.util.Properties;

import layer1.layer2.string.Logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ExternalConfig {
	private static final Logger logger = LoggerFactory.getLogger(ExternalConfig.class);
	private static final String PROP_CLIENT_DEFAULT_TIMEOUT_MILLIS = "CLIENT_DEFAULT_TIMEOUT_MILLIS";
	private static final String PROP_FOURSQUARE_CLIENT_ID = "FOURSQUARE_CLIENT_ID";
	private static final String PROP_FOURSQUARE_CLIENT_SECRET = "FOURSQUARE_CLIENT_SECRET";
	private static final String PROP_FOURSQUARE_VERSION = "FOURSQUARE_VERSION";
	private static final String PROP_GOOGLE_PLACES_KEY = "GOOGLE_PLACES_KEY";
	private static final String PROP_COUPON_DUNIA_PARTNER_ID = "COUPON_DUNIA_PARTNER_ID";
	private static final String PROP_COUPON_DUNIA_API_KEY = "COUPON_DUNIA_API_KEY";
	private static final String PROP_GCM_SENDER_ID = "GCM_SENDER_ID";
	private static final String PROP_GCM_API_KEY = "GCM_API_KEY";
	private static final String PROP_GCM_CSS_PORT = "GCM_CSS_PORT";
	private static final String PROP_GCM_SERVER = "GCM_SERVER";
	private static final String PROP_AWS_ACCESS_KEY = "AWS_ACCESS_KEY";
	private static final String PROP_AWS_SECRET_KEY = "AWS_SECRET_KEY";
	private static final String PROP_UNWIRED_REQUEST_KEY = "UNWIRED_REQUEST_KEY";
	private static final String PROP_DEFAULT_DELHI_LAT = "DEFAULT_DELHI_LATITUTE";
	private static final String PROP_DEFAULT_DELHI_LON = "DEFAULT_DELHI_LONGITUDE";
	private static int clientDefaultTimeOutInMillis;
	private static String foursquareClientId;
	private static String foursquareClientSecret;
	private static String foursquareVersion;
	private static String googlePlacesKey;
	private static String couponDuniaPartnerId;
	private static String couponDuniaApiKey;
	private static String unwiredRequestKey;
	private static int gcmCssPort;
	private static String gcmServer;
	private static String gcmSenderId;
	private static String gcmApiKey;
	private static String awsAccessKey;
	private static String awsSecretKey;
	private static String defaultDelhiLat;
	private static String defaultDelhiLon;

	public static synchronized boolean initialize() {
		try {
			FileInputStream fis = new FileInputStream(BaseConfig.resourcesBasePath()
					+ ConfigResources.externalPropertiesFileName());
			logger.info("Start: Initializing External Config");
			Properties prop = new Properties();
			prop.load(fis);
			clientDefaultTimeOutInMillis = Integer
					.parseInt(prop.getProperty(PROP_CLIENT_DEFAULT_TIMEOUT_MILLIS).trim());
			foursquareClientId = prop.getProperty(PROP_FOURSQUARE_CLIENT_ID).trim();
			foursquareClientSecret = prop.getProperty(PROP_FOURSQUARE_CLIENT_SECRET).trim();
			foursquareVersion = prop.getProperty(PROP_FOURSQUARE_VERSION).trim();
			googlePlacesKey = prop.getProperty(PROP_GOOGLE_PLACES_KEY).trim();
			couponDuniaPartnerId = prop.getProperty(PROP_COUPON_DUNIA_PARTNER_ID).trim();
			couponDuniaApiKey = prop.getProperty(PROP_COUPON_DUNIA_API_KEY).trim();
			unwiredRequestKey = prop.getProperty(PROP_UNWIRED_REQUEST_KEY).trim();
			gcmSenderId = prop.getProperty(PROP_GCM_SENDER_ID).trim();
			gcmApiKey = prop.getProperty(PROP_GCM_API_KEY).trim();
			gcmCssPort = Integer.parseInt(prop.getProperty(PROP_GCM_CSS_PORT).trim());
			gcmServer = prop.getProperty(PROP_GCM_SERVER).trim();
			awsAccessKey = prop.getProperty(PROP_AWS_ACCESS_KEY).trim();
			awsSecretKey = prop.getProperty(PROP_AWS_SECRET_KEY).trim();
			defaultDelhiLat = prop.getProperty(PROP_DEFAULT_DELHI_LAT).trim();
			defaultDelhiLon = prop.getProperty(PROP_DEFAULT_DELHI_LON).trim();
			logger.info("End: Initializing External Config");
			return true;
		} catch (Exception e) {
			logger.error(Logging.FATAL, "Error loading External configuration from properties file.", e);
			throw new RuntimeException("Error loading External configuration", e);
		}
	}

	public static int clientDefaultTimeOutInMillis() {
		return clientDefaultTimeOutInMillis;
	}

	public static String foursquareClientId() {
		return foursquareClientId;
	}

	public static String foursquareClientSecret() {
		return foursquareClientSecret;
	}

	public static String foursquareVersion() {
		return foursquareVersion;
	}

	public static String googlePlacesKey() {
		return googlePlacesKey;
	}

	public static String couponDuniaPartnerId() {
		return couponDuniaPartnerId;
	}

	public static String couponDuniaApiKey() {
		return couponDuniaApiKey;
	}

	public static String unwiredRequestKey() {
		return unwiredRequestKey;
	}

	public static String gcmSenderId() {
		return gcmSenderId;
	}

	public static String gcmApiKey() {
		return gcmApiKey;
	}

	public static int gcmCssPort() {
		return gcmCssPort;
	}

	public static String gcmServer() {
		return gcmServer;
	}

	public static String awsAccessKey() {
		return awsAccessKey;
	}

	public static String awsSecretKey() {
		return awsSecretKey;
	}

	public static String defaultDelhiLat() {
		return defaultDelhiLat;
	}

	public static String defaultDelhiLon() {
		return defaultDelhiLon;
	}
}
