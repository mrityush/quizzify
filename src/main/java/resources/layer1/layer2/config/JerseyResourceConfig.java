package layer1.layer2.config;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.src.main.tlabs.quizzify.rest.controller.Resource;

public class JerseyResourceConfig extends ResourceConfig {
	private static final Logger logger = LoggerFactory.getLogger(JerseyResourceConfig.class);

	public JerseyResourceConfig() {
		logger.info("Configuring ResourceConfig");
		packages(Resource.class.getPackage().getName());
		register(JacksonFeature.class);
		logger.info("Configured ResourceConfig");
	}
}
