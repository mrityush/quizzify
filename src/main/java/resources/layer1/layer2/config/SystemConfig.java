package layer1.layer2.config;

import java.io.FileInputStream;
import java.util.Properties;

public class SystemConfig {
	private static final String PROP_SUPPORTED_VERSIONS = "SUPPORTED_VERSIONS";
	private static final String PROP_APP_PUBLIC_KEY = "APP_PUBLIC_KEY";
	private static final String PROP_PORT = "PORT";
	private static String supportedVersions;
	private static String appPublicKey;
	private static int port;

	public static synchronized boolean initialize() {
		// try (InputStream is =
		// SystemConfig.class.getClassLoader().getResourceAsStream(
		// ConfigResources.systemPropertiesFileName())) {
		try {
			FileInputStream fis = new FileInputStream(BaseConfig.resourcesBasePath()
					+ ConfigResources.systemPropertiesFileName());
			Properties prop = new Properties();
			prop.load(fis);
			// Properties prop = new Properties();
			// prop.load(is);
			supportedVersions = prop.getProperty(PROP_SUPPORTED_VERSIONS).trim();
			appPublicKey = prop.getProperty(PROP_APP_PUBLIC_KEY).trim();
			port = Integer.parseInt(prop.getProperty(PROP_PORT).trim());
			return true;
		} catch (Exception e) {
			throw new RuntimeException("Error initializing Base Config", e);
		}
	}

	public static String supportedVersions() {
		return supportedVersions;
	}

	public static String appPublicKey() {
		return appPublicKey;
	}

	public static int port() {
		return port;
	}
}
