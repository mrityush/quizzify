package layer1.layer2.constants;

public class UrlConstants {
	public static final String ADMIN_SEPARATOR = "admin/";
	public static final String COMMUNICATION_SEPARATOR = "comunications/";
	public static final String EMAIL_SEPARATOR = "emails/";
	public static final String LOGGING_SEPARATOR = "logs/";
	public static final String QUESTION_SEPARATOR = "questions/";
	public static final String QUIZ_SEPARATOR = "quizzes/";
	public static final String RATE_SEPARATOR = "rating/";
	public static final String REWARD_SEPARATOR = "rewards/";
	public static final String SEPARATOR = "/";
	public static final String SHIPMENT_SEPARATOR = "shipments/";
	public static final String SPONSOR_SEPARATOR = "sponsors/";
	public static final String USER_SEPARATOR = "users/";
}
