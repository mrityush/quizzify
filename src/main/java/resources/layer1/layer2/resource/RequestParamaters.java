package layer1.layer2.resource;

import javax.servlet.http.HttpServletRequest;

public class RequestParamaters {
	private HttpServletRequest req;
	private String payload;
	private String requestMessage;
	private String authParam;

	public RequestParamaters(Builder b) {
		req = b.req;
		payload = b.payload;
		requestMessage = b.requestMessage;
		authParam = b.authParam;
	}

	public static class Builder {
		private HttpServletRequest req;
		private String payload;
		private String requestMessage;
		private String authParam;

		public Builder req(HttpServletRequest req) {
			this.req = req;
			return this;
		}

		public Builder payload(String payload) {
			this.payload = payload;
			return this;
		}

		public Builder requestMessage(String requestMessage) {
			this.requestMessage = requestMessage;
			return this;
		}

		public Builder authParam(String authParam) {
			this.authParam = authParam;
			return this;
		}

		public RequestParamaters build() {
			return new RequestParamaters(this);
		}

	}

	public HttpServletRequest req() {
		return req;
	}

	public String payload() {
		return payload;
	}

	public String requestMessage() {
		return requestMessage;
	}

	public String authParam() {
		return authParam;
	}
}
