package com.src.main.tlabs.quizzify.rest.controller;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import layer1.layer2.constants.UrlConstants;
import layer1.layer2.resource.RequestParamaters;

@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Path(UrlConstants.SEPARATOR + UrlConstants.ADMIN_SEPARATOR)
@Produces(MediaType.APPLICATION_JSON)
public class AdminController implements Resource {
	private String requestMessage = null;

	@POST
	@Path(UrlConstants.QUESTION_SEPARATOR)
	public String addQuestion(@Context HttpServletRequest req, @FormParam("payload") String payload,
			@HeaderParam("authorization") String appPublicKey) {
		requestMessage = "Add Question";
		RequestParamaters requestParameters = new RequestParamaters.Builder().req(req).payload(
				payload).requestMessage(requestMessage).authParam(appPublicKey).build();
		return Resource.handleRequestResponse(requestParameters);
	}
}
