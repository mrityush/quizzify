package com.src.main.tlabs.quizzify.rest.controller;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.src.main.tlabs.quizzify.db.communications.CommunicationLink;

import layer1.layer2.link.CommLink;
import layer1.layer2.resource.RequestParamaters;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
public interface Resource {
	static final Logger logger = LoggerFactory.getLogger(Resource.class);

	static String handleRequestResponse(RequestParamaters requestParams) {
		// HttpServletRequest req = requestParams.req();
		String payload = requestParams.payload();
		String requestName = requestParams.requestMessage();
		String authParams = requestParams.authParam();
		logger.debug("payload = {}, request Name = {}", payload, requestName);

		CommunicationLink.Builder b = new CommunicationLink.Builder();
		LocalDateTime now = LocalDateTime.now();
		CommLink link = b.createdBy(-1).creationTime(now).lastModifiedBy(-1).lastModificationTime(
				now).build();
		link.listen(payload);
		link.process(authParams);
		logger.debug("END: {} request.", requestName);
		return link.reply();
	}

}
