package com.src.main.tlabs.quizzify.rest.controller;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import layer1.layer2.constants.UrlConstants;
import layer1.layer2.resource.RequestParamaters;

/**
 * @author mrityunjya.shukla
 */
@Path(UrlConstants.USER_SEPARATOR)
@Produces(MediaType.APPLICATION_JSON)
public class UserController implements Resource {
    private String requestMessage = null;
    // User/{ID}/rate/user/{user_Id}//not needed actually as user will be rated
    // by quiz
    // User/{ID}/rate/quiz/{quiz_Id}
    // user/{id}/quiz/section/add
    // user/{id}/quiz/section/edit
    // user/{id}/quiz/section/fetch
    // user/{id}/quiz/section/delete

    @Path("{id}" + UrlConstants.SEPARATOR + UrlConstants.RATE_SEPARATOR + UrlConstants.QUIZ_SEPARATOR + "{quizId}")
    @POST
    public String rateQuiz(@Context HttpServletRequest req, @PathParam("id") long id,
                           @FormParam("payload") String payload, @HeaderParam("authorization") String appPublicKey) {
        requestMessage = "Rate Quiz";
        RequestParamaters requestParameters = new RequestParamaters.Builder().req(req).payload(payload)
                .requestMessage(requestMessage).authParam(appPublicKey).build();
        return Resource.handleRequestResponse(requestParameters);
    }


}