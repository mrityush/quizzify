package layer1.layer2.link;

import java.io.IOException;

import layer1.layer2.comm.request.Request;
import layer1.layer2.comm.response.Response;
import layer1.layer2.resource.AuthenticationException;
import layer1.layer2.resource.ErrorType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;

public abstract class AbstractCommunicationLink implements CommLink {
	private static Logger logger = LoggerFactory.getLogger(AbstractCommunicationLink.class);
	protected Request request;
	protected Response response;

	public AbstractCommunicationLink(Builder<?, ?> b) {
		request = b.request;
		response = b.response;
	}

	public AbstractCommunicationLink() {
		// TODO Auto-generated constructor stub
	}

	public static abstract class Builder<E extends CommLink, T extends Builder<E, T>> {
		protected Response response;
		protected Request request;

		public abstract T self();

		public T request(Request request) {
			this.request = request;
			return self();
		}

		public T response(Response response) {
			this.response = response;
			return self();
		}
	}

	@Override
	public Request request() {
		return request;
	}

	@Override
	public Response response() {
		return response;
	}

	@Override
	public void process(String... authParams) {
		logger.trace("Processing Request: ({})", request);
		try {
			request.authenticate(authParams);
		} catch (Exception e) {
			logger.error("Authentication Exception.", e);
			if (e == AuthenticationException.INVALID_CREDENTIALS) {
				throw ErrorType.WRONG_CREDENTIALS.exception();
			} else if (e == AuthenticationException.UNSUPPORTED_SOURCE) {
				throw ErrorType.NOT_SUPPORTED_ANY_MORE.exception();
			}
		}
		if (!request.validate()) {
			logger.error("Invalid Request. ({})", request);
		}
		try {
			response = request.process();
		} catch (Exception e) {
			logger.error("Error processing request: ({})", request, e);
		}
		logger.trace("Constructed Response: ({})", response);
		response.process();
	}

	@Override
	public void inJsonString(JsonGenerator g) throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void populateFromJsonString(String field, JsonParser p) throws IOException {
		throw new UnsupportedOperationException();
	}

}
