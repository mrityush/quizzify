package layer1.layer2.link;

import layer1.layer2.comm.request.Request;
import layer1.layer2.comm.response.Response;
import layer1.layer2.serialise.JsonSerialisable;

public interface CommLink extends JsonSerialisable {
	Request request();

	Response response();

	void process(String... authParams);

	void listen(String input);

	String reply();

}
