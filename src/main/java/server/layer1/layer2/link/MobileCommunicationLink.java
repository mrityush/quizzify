package layer1.layer2.link;

import java.io.IOException;

import layer1.layer2.comm.request.Request;
import layer1.layer2.serialise.JacksonUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;

public class MobileCommunicationLink extends AbstractCommunicationLink {
	private static Logger logger = LoggerFactory.getLogger(MobileCommunicationLink.class);

	public MobileCommunicationLink(Builder builder) {
		super(builder);
	}

	@Override
	public void listen(String input) {
		try {
			request = JacksonUtil.DEFAULT.mapper().readValue(input, new TypeReference<Request>() {
			});
		} catch (IOException e) {
			logger.error("Error parsing input: ({})", input, e);
		}
	}

	public static class Builder extends AbstractCommunicationLink.Builder<MobileCommunicationLink, Builder> {
		public MobileCommunicationLink build() {
			return new MobileCommunicationLink(this);
		}

		public Builder() {
			super();
		}

		@Override
		public Builder self() {
			return this;
		}

	}

	@Override
	public String reply() {
		long start = System.currentTimeMillis();
		String reply = response.replyInJson().orElse("Could not convert to JSON! STOP");
		long end = System.currentTimeMillis();
		if (end - start > 100) {
			logger.warn("{} ms taken to create reply for request of class ({}) ", end - start, request.getClass()
					.getSimpleName());
		}
		logger.info("Sending Response: ({})", reply);
		return reply;
	}

}
