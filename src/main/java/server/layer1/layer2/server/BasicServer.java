package layer1.layer2.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface BasicServer extends Runnable {
	static final Logger logger = LoggerFactory.getLogger(BasicServer.class);

	@Override
	default void run() {
		try {
			start();
		} catch (Exception e) {
			logger.debug("Error Starting server ", e);
		}
	}

	void start() throws Exception;

	void stop() throws Exception;

	boolean isRunning();

	int port();

}
