package layer1.layer2.server;

import java.util.Arrays;

import layer1.layer2.config.JerseyResourceConfig;
import layer1.layer2.enums.EnumOps;
import layer1.layer2.server.handlers.TestRequestHandler;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JettyRestServer implements RestServer {
	private static final Logger logger = LoggerFactory.getLogger(JettyRestServer.class);
	private final int port;
	private Server server;

	public JettyRestServer(int port) {
		this.port = port;
	}

	@Override
	public void start() throws Exception {
		ServletContainer container = new ServletContainer(new JerseyResourceConfig());
		ServletHolder sh = new ServletHolder(container);
		ServletContextHandler sch = new ServletContextHandler(ServletContextHandler.NO_SECURITY
				| ServletContextHandler.NO_SESSIONS);
		sch.addServlet(DefaultServlet.class, "/");
		sch.addServlet(TestRequestHandler.class, "/test/*");// TODO remove it
		sch.addServlet(sh, "/*");
		sch.setContextPath("/");
		server = new Server(port);
		server.setHandler(sch);
		server.start();
		System.out.println("Started Server in port : " + port);
		logger.debug("Started Server on port : {}", port);
		logger.debug("Enum Values :{} ", Arrays.asList(EnumOps.values()));
		logger.debug("Enum Values :{} ");
		server.join();
	}

	@Override
	public void stop() throws Exception {
		server.destroy();
	}

	@Override
	public boolean isRunning() {
		return server.isRunning();
	}

	@Override
	public int port() {
		return port;
	}

}
