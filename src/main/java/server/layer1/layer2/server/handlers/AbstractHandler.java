package layer1.layer2.server.handlers;

import javax.servlet.http.HttpServlet;

import layer1.layer2.comm.request.Request;

public abstract class AbstractHandler extends HttpServlet {
	public abstract void handle(Request request);
}
