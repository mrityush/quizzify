package layer1.layer2.server.handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import layer1.layer2.comm.request.Request;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
public class TestRequestHandler extends AbstractHandler {
	private static Logger logger = LoggerFactory.getLogger(TestRequestHandler.class);

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.debug("Comes Here with request!");
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	public void handle(Request request) {
		// TODO Auto-generated method stub

	}

}
