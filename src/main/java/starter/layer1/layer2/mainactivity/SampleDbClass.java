package layer1.layer2.mainactivity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.src.main.tlabs.quizzify.dao.manager.DaoManager;
import com.src.main.tlabs.quizzify.db.questionnaires.QuizSet;
import com.src.main.tlabs.quizzify.db.statusfields.QuizStatus;

import layer1.layer2.config.BaseConfig;
import layer1.layer2.hibernate.HibernateConfig;
import layer1.layer2.serialise.JsonUtil;

public class SampleDbClass {
	private static Logger logger = LoggerFactory.getLogger(SampleDbClass.class);

	public static void main(String[] args) {

		BaseConfig.initialize();
		HibernateConfig.initialize();
		// ////////////////////////////////////////////
		// Person person = new Person();
		// person.setId(2603);
		// person.setName("CheckingMerchant2");
		// Session session =
		// HibernateUtil.getEmailsSessionFactory().openSession();
		// ////////////////////////////////////////////
		// Users person = DaoManager.userDao().getUserById(1).get(0);
		// person.setStatus(UserStatus.ACTIVE);
		// Session session =
		// HibernateUtil.getUsersSessionFactory().openSession();
		// boolean success = DaoManager.userDao().updateUser(person, session);
		// String json = JsonUtil.toJson(person).get();
		// logger.debug("Success:{} in adding person {}", success, person);
		// logger.debug(" Json is {}", json);
		// ////////////////////////////////////////////
		// Users person = new
		// Users.Builder().createdBy(-1).creationTime(LocalDateTime.now()).email("admin@quizzify.com")
		// .lastModificationTime(LocalDateTime.now()).lastModifiedBy(-1).status(UserStatus.ACTIVE).type(1).id(1)
		// .userUuid(UUID.randomUUID()).build();
		// Session session =
		// HibernateUtil.getUsersSessionFactory().openSession();
		// boolean success = DaoManager.userDao().updateUser(person, session);
		// String json = JsonUtil.toJson(person).get();
		// logger.debug("Success:{} in adding person {}", success, person);
		// logger.debug(" Json is {}", json);
		// // ////////////////////////////////////////////
		// CommunicationLinks cLinks =
		// DaoManager.communicationDao().getCommunicationLinkById(2).get(0);
		// logger.debug("Extracted Links object is {}",
		// JsonUtil.toJson(cLinks).get());
		// ////////////////////////////////////////////
		List<Integer> ls = new ArrayList<Integer>();
		ls.add(1);
		ls.add(2);
		ls.add(3);
		ls.add(4);
		QuizSet email = new QuizSet.Builder().createdBy(-1).creationTime(LocalDateTime.now())
				.lastModificationTime(LocalDateTime.now()).lastModifiedBy(-1).status(QuizStatus.ACTIVE).questionSequence(ls)
				.quizId(1).build();
		boolean successOld = DaoManager.questionnaireDao().addQuizSet(email);
		String jsonBefore = JsonUtil.toJson(email).get();
		logger.debug("Success:{} in adding email {}", successOld, email);
		logger.debug("Email Json is {}", jsonBefore);
		QuizSet fetchedQS = DaoManager.questionnaireDao().getQuizSetById(1).get(0);
		String json = JsonUtil.toJson(fetchedQS).get();
		logger.debug("Success:{} in adding person {}", successOld, fetchedQS);
		logger.debug(" Json is {}", jsonBefore);
		// ////////////////////////////////////////////
		// Emails email = new
		// Emails.Builder().createdBy(-1).creationTime(LocalDateTime.now())
		// .lastModificationTime(LocalDateTime.now()).lastModifiedBy(-1).status(EmailStatus.ACTIVE).templateId(-1)
		// .userId(-1).valueVariable("TestValueVariable8").lastModificationTimes(DateTime.now()).build();
		// boolean success = DaoManager.emailsDao().addEmail(email);
		// String json = JsonUtil.toJson(email).get();
		// logger.debug("Success:{} in adding email {}", success, email);
		// logger.debug("Email Json is {}", json);
		// ////////////////////////////////////////////
		// CommunicationLinks cLink = new
		// CommunicationLinks.Builder().createdBy(-1).creationTime(LocalDateTime.now())
		// .lastModificationTime(LocalDateTime.now()).lastModifiedBy(-1).status(CommunicationLinkStatus.ACTIVE)
		// .request("DummyRequest2").response("DummyResponse2").id(-1).type(-1).build();
		// boolean success =
		// DaoManager.communicationDao().addCommunicationLink(cLink);
		// logger.debug("Success:{} in adding communication Link {}", success,
		// cLink);
		// System.out.println("Success:" + success +
		// " in adding communication Link {}" + cLink);
		// //////////////////////////////////////////////
		// Session session =
		// HibernateUtil.getTestSessionFactory().openSession();
		// Session session =
		// HibernateUtil.getTestSessionFactory().openSession();
		// session.beginTransaction();
		// session.save(person);
		// session.getTransaction().commit();
		// // session.close();
		// System.err.println("Done");

		/*--------------------------------*/
		// Animal person = new Animal.Builder().id(1).name("lakshmi").build();
		// Emails email = new
		// Emails.Builder().createdBy(-1).creationTime(LocalDateTime.now())
		// .lastModificationTime(LocalDateTime.now()).lastModifiedBy(-1).status(EmailStatus.ACTIVE).templateId(-1)
		// .userId(-1).valueVariable("TestValueVariable5").lastModificationTimes(DateTime.now()).build();
		// // person.setId(2603);
		// // person.setName("CheckingMerchant2");
		// // Session session =
		// // HibernateUtil.getTestSessionFactory().openSession();
		// session.beginTransaction();
		// // session.save(person);
		// session.save(email);
		// session.getTransaction().commit();
		// session.close();
		// System.err.println("Done");

		/*-----------------------------*/

		// Animal person2 = (Animal) session.get(Animal.class, 2600);
		// System.out.println("Person is " + person2);
		// // session.getTransaction().commit();
		// person2.setId(2600);
		// person2.setName("CheckinggMerchant");
		// session.beginTransaction();
		// session.save(person2);
		// session.saveOrUpdate(person2);
		// session.getTransaction().commit();
		// session.close();
		// System.err.println("Done");

		/*-----------------------------*/
		// Person person2 = (Person) session.get(Person.class, 2600);
		// System.out.println("Person is " + person2);
		// // session.getTransaction().commit();
		// person2.setId(2600);
		// person2.setName("CheckinggMerchant");
		// session.beginTransaction();
		// session.save(person2);
		// session.saveOrUpdate(person2);
		// session.getTransaction().commit();
		// session.close();
		// System.err.println("Done");
	}
}