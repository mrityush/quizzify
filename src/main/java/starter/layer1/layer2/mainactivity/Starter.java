package layer1.layer2.mainactivity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import layer1.layer2.config.BaseConfig;
import layer1.layer2.config.SystemConfig;
import layer1.layer2.hibernate.HibernateConfig;
import layer1.layer2.server.BasicServer;
import layer1.layer2.server.JettyRestServer;

public class Starter {

	private static volatile BasicServer server;
	private static int port;
	private static Logger logger = LoggerFactory.getLogger(Starter.class);

	public static void main(String[] args) {
		BaseConfig.initialize();
		HibernateConfig.initialize();
		SystemConfig.initialize();
		port = SystemConfig.port();

		try {
			server = new JettyRestServer(port);
			server.start();
			logger.debug("Started Server on port : {}", port);
			System.out.println("Started Server on port --> " + port);
		} catch (Exception e) {
			logger.debug("Error in starting server ", e);
			System.exit(-1);
		}
	}

	public static Optional<List<String>> keys() {
		return Optional.of(Arrays.asList(new String[] { "cache:entity:messagedata:{type}",
				"cache:entity:messagedatas", "cache:entity:merchant:{type}:messagedatas",
				"cache:entity:merchant:name" }));
	}
}
