package layer1.layer2.dbthings;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "animals")
public class Animal {
	@Id
	@Column(name = "id")
	private int id;
	@Column(name = "name")
	private String name;

	@Transient
	private List<String> name2;

	public Animal(Builder b) {
		id = b.id;
		name = b.name;
	}

	public Animal() {
		// TODO Auto-generated constructor stub
	}

	public static class Builder {

		private int id;
		private String name;

		public Animal build() {
			return new Animal(this);
		}

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}