package layer1.layer2.enums;

import java.io.IOException;

import layer1.layer2.serialise.JsonSerialisable;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;

public enum EnumOps implements JsonSerialisable {
	ITEM1 {

		@Override
		public void inJsonString(JsonGenerator g) throws IOException {

		}

		@Override
		public void populateFromJsonString(String field, JsonParser p)
				throws IOException {

		}

		@Override
		public void showAll() {

		}

	},
	ITEM2 {

		@Override
		public void inJsonString(JsonGenerator g) throws IOException {
			// TODO Auto-generated method stub

		}

		@Override
		public void populateFromJsonString(String field, JsonParser p)
				throws IOException {
			// TODO Auto-generated method stub

		}

		@Override
		public void showAll() {
			// TODO Auto-generated method stub

		}

	};

	public abstract void showAll();

}
