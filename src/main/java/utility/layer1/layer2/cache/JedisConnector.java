package layer1.layer2.cache;

import layer1.layer2.generics.Connector;
import redis.clients.jedis.Jedis;

/**
 * 
 * @author Mrityunjya.Shukla
 *
 */
public class JedisConnector implements Connector {
	private Jedis jedis;

	@Override
	public boolean connect() {
		jedis = new Jedis("localhost");
		System.out.println("Server is running: " + jedis.ping());
		return jedis.isConnected();
	}

	@Override
	public boolean disconnect() {
		jedis.close();
		if (jedis.isConnected()) {
			return false;
		} else {
			return true;
		}
	}

	public Jedis jedis() {
		return jedis;
	}

}
