package layer1.layer2.fileio;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class FileIOOperations {
	public static List<String> getFileList(String passedFilePath) {
		File file = new File(passedFilePath);
		List<String> fileList = Arrays.asList(file.list());
		return fileList;
	}
}
