package layer1.layer2.generics;

public interface Connector {
	public boolean connect();

	public boolean disconnect();
}
