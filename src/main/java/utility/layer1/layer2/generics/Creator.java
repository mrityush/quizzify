package layer1.layer2.generics;

public interface Creator<T> {
	public T configureXml();
}
