package layer1.layer2.generics;

import java.security.MessageDigest;
import java.util.Base64;
import java.util.Random;
import java.util.UUID;

import layer1.layer2.config.SystemConfig;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
public abstract class CryptoUtil {
	private static Random random = new Random(System.currentTimeMillis());

	public static String generateAccessToken() {
		return UUID.randomUUID().toString();
	}

	public static UUID generateUUID() {
		return UUID.randomUUID();
	}

	public static boolean matchesAppPublicKey(String encryptedKey) {
		return encryptedKey.equals(SystemConfig.appPublicKey());
	}

	public static String generateCheckSum(String checkSumString) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(checkSumString.getBytes("UTF-8"));
			byte raw[] = md.digest();
			return Base64.getEncoder().encodeToString(raw);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String getEncryptedPassword(String password, String salt) {
		String encryptedValue = null;
		try {
			StringBuffer sb = new StringBuffer();
			sb.append("-").append(password).append("-").append(salt);
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(sb.toString().getBytes("UTF-8"));
			byte raw[] = md.digest();
			encryptedValue = Base64.getEncoder().encodeToString(raw);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return encryptedValue;
	}

	public static int generateRandomNumber() {
		return (int) (100000 + random.nextFloat() * 900000);
	}

}
