package layer1.layer2.generics;

public class Pair<X, Y> {
	X left;
	Y right;

	public Pair(X left, Y right) {
		this.left = left;
		this.right = right;
	}

	public X left() {
		return left;
	}

	public Y right() {
		return right;
	}

	public void setLeft(X leftItem) {
		this.left = leftItem;
	}

	public void setRight(Y rightItem) {
		this.right = rightItem;
	}
}
