package layer1.layer2.generics;

import java.time.LocalDateTime;

import layer1.layer2.serialise.JsonSerialisable;

public interface QuizzifyEntity extends JsonSerialisable {

	public void setStatus(Status status);

	public Status status();

	public LocalDateTime lastModificationTime();

	public LocalDateTime creationTime();

	public long createdBy();

	public long lastModifiedBy();

}
