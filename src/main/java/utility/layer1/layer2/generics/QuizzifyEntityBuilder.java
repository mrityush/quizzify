package layer1.layer2.generics;

public interface QuizzifyEntityBuilder<E extends QuizzifyEntity> {
	public E build();
}
