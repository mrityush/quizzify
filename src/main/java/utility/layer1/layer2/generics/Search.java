package layer1.layer2.generics;

public interface Search<T> {
	public String getSearchResult(T i);

}
