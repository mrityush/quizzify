package layer1.layer2.generics;

import java.util.List;

public interface Sort<T> {
	public List<T> sort();
}
