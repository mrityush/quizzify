package layer1.layer2.generics;

public interface Status {

	int id();

	String name();

	String description();
}
