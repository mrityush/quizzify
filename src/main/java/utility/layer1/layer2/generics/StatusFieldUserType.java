package layer1.layer2.generics;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.usertype.EnhancedUserType;

import com.src.main.tlabs.quizzify.db.statusfields.CommunicationLinkStatus;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
@SuppressWarnings("serial")
public class StatusFieldUserType implements EnhancedUserType, Serializable {

	private static final int[] SQL_TYPES = new int[] { Types.INTEGER };

	@Override
	public int[] sqlTypes() {
		return SQL_TYPES;
	}

	@SuppressWarnings("rawtypes")
	// TODO check generic
	@Override
	public Class returnedClass() {
		return Status.class;
	}

	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == y) {
			return true;
		}
		if (x == null || y == null) {
			return false;
		}
		Status dtx = (Status) x;
		Status dty = (Status) y;
		return dtx.equals(dty);
	}

	@Override
	public int hashCode(Object object) throws HibernateException {
		return object.hashCode();
	}

	@Override
	public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index, SessionImplementor session)
			throws HibernateException, SQLException {
		if (value == null) {
			StandardBasicTypes.INTEGER.nullSafeSet(preparedStatement, -1, index, session);
		} else {
			Status status = (Status) value;
			StandardBasicTypes.INTEGER.nullSafeSet(preparedStatement, status.id(), index, session);
		}
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}

	@Override
	public boolean isMutable() {
		return false;
	}

	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) value;
	}

	@Override
	public Object assemble(Serializable cached, Object value) throws HibernateException {
		return cached;
	}

	@Override
	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return original;
	}

	@Override
	public String objectToSQLString(Object object) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toXMLString(Object object) {
		return object.toString();
	}

	@Override
	public Object fromXMLString(String string) {
		return string;
	}

	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner)
			throws HibernateException, SQLException {
		int enumId = (int) StandardBasicTypes.INTEGER.nullSafeGet(rs, names, session, owner);
		if (enumId == 0) {
			return null;
		}
		// writing communication link status here because it has maximum enum
		// values
		return (Status) CommunicationLinkStatus.valueOf(enumId);
	}

}