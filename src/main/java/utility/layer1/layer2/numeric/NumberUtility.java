package layer1.layer2.numeric;

import java.util.ArrayList;
import java.util.List;

public class NumberUtility {
	public static int[][] getRandom2DArray(int rows, int cols) {
		int[][] random2DArray = new int[rows][cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				random2DArray[i][j] = (int) (10 * Math.random());
				// System.out.print(random2DArray[i][j] + ",");
			}
		}
		return random2DArray;
	}

	public static List<Integer> getRandomIntegerList(int d) {
		List<Integer> intList = new ArrayList<Integer>();
		for (int i = 0; i < d; i++) {
			intList.add((int) (Math.random() * 10));
		}
		return intList;
	}
}
