package layer1.layer2.serialise;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * 
 * @author Abhinav.Dwivedi
 *
 */
public class CustomDateDeserializer extends JsonDeserializer<LocalDate> {
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	@Override
	public LocalDate deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
		p.nextToken();
		p.nextToken();
		LocalDate time = LocalDate.parse(p.readValueAs(String.class), formatter);
		while (p.getCurrentToken() != JsonToken.END_OBJECT) {
			p.nextToken();
		}
		return time;
	}
}
