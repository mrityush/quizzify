package layer1.layer2.serialise;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * 
 * @author Abhinav.Dwivedi
 *
 */
public class CustomDateSerializer extends JsonSerializer<LocalDate> {
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	@Override
	public void serialize(final LocalDate time, JsonGenerator g, SerializerProvider arg2) throws IOException,
			JsonProcessingException {
		g.writeStartObject();
		g.writeStringField("value", formatter.format(time));
		g.writeEndObject();
	}
}
