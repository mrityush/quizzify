package layer1.layer2.serialise;

import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
public class CustomDateTimeDeserializer extends JsonDeserializer<DateTime> {
	private static DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");

	@Override
	public DateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		p.nextToken();
		p.nextToken();
		DateTime time = formatter.parseDateTime(p.readValueAs(String.class));
		while (p.getCurrentToken() != JsonToken.END_OBJECT) {
			p.nextToken();
		}
		return time;
	}
}
