package layer1.layer2.serialise;

import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * 
 * @author Abhinav.Dwivedi
 *
 */
public class CustomDateTimeSerializer extends JsonSerializer<DateTime> {
	DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");

	@Override
	public void serialize(final DateTime time, JsonGenerator g, SerializerProvider arg2) throws IOException,
			JsonProcessingException {
		g.writeStartObject();
		String dtStr = fmt.print(time);
		g.writeStringField("value", dtStr);
		g.writeEndObject();
	}
}
