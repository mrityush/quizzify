package layer1.layer2.serialise;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * 
 * @author Abhinav.Dwivedi
 *
 */
public class CustomLocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

	@Override
	public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
			JsonProcessingException {
		p.nextToken();
		p.nextToken();
		LocalDateTime time = LocalDateTime.parse(p.readValueAs(String.class), formatter);
		while (p.getCurrentToken() != JsonToken.END_OBJECT) {
			p.nextToken();
		}
		return time;
	}
}
