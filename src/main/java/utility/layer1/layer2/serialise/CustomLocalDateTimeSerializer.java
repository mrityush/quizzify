package layer1.layer2.serialise;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * 
 * @author Abhinav.Dwivedi
 *
 */
public class CustomLocalDateTimeSerializer extends JsonSerializer<LocalDateTime> {
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

	@Override
	public void serialize(final LocalDateTime time, JsonGenerator g, SerializerProvider arg2) throws IOException,
			JsonProcessingException {
		g.writeStartObject();
		g.writeStringField("value", FORMATTER.format(time));
		g.writeEndObject();
	}
}
