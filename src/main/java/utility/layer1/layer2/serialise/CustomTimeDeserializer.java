package layer1.layer2.serialise;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * 
 * @author Mrityunjya.Shukla
 *
 */
public class CustomTimeDeserializer extends JsonDeserializer<LocalTime> {
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

	@Override
	public LocalTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
		p.nextToken();
		p.nextToken();
		LocalTime time = LocalTime.parse(p.readValueAs(String.class), formatter);
		while (p.getCurrentToken() != JsonToken.END_OBJECT) {
			p.nextToken();
		}
		return time;
	}
}
