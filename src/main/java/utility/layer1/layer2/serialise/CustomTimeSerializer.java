package layer1.layer2.serialise;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * 
 * @author Mrityunjya.Shukla
 *
 */
public class CustomTimeSerializer extends JsonSerializer<LocalTime> {
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

	@Override
	public void serialize(final LocalTime time, JsonGenerator g, SerializerProvider arg2) throws IOException,
			JsonProcessingException {
		g.writeStartObject();
		g.writeStringField("value", formatter.format(time));
		g.writeEndObject();
	}
}
