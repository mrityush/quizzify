package layer1.layer2.serialise;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * 
 * @author Abhinav.Dwivedi
 *
 */
public abstract class DateUtility {
	public static LocalTime DAY_START_TIME = LocalTime.of(00, 01);
	public static LocalTime DAY_END_TIME = LocalTime.of(23, 59);
	public static final DateTimeFormatter FORMATTER_DD_MM_YYYY_HH_MM_SS = DateTimeFormatter
			.ofPattern("dd/MM/yyyy HH:mm:ss");
	public static final DateTimeFormatter FORMATTER_DD_MM_YYYY_HH_MM_SS_MS = DateTimeFormatter
			.ofPattern("dd/MM/yyyy HH:mm:ss.SSS");

	public static final DateTimeFormatter FORMATTER_EMAIL_DATETIME = DateTimeFormatter.ofPattern("d-MMM-yyyy h:mm a");

	public static LocalDateTime parseDateTime(String dateTime, String pattern) {
		Objects.requireNonNull(dateTime);
		Objects.requireNonNull(pattern);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		return LocalDateTime.parse(dateTime, formatter);
	}

	public static LocalDate parseDate(String date, String pattern) {
		Objects.requireNonNull(date);
		Objects.requireNonNull(pattern);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		return LocalDate.parse(date, formatter);
	}

	public static LocalDateTime correctYear(LocalDateTime smsDateTime, LocalDateTime dateTime) {
		Objects.requireNonNull(smsDateTime);
		Objects.requireNonNull(dateTime);
		LocalDate smsDate = smsDateTime.toLocalDate();
		LocalDate date = dateTime.toLocalDate();
		LocalTime time = dateTime.toLocalTime();
		if (smsDate.getMonthValue() < 3 && date.getMonthValue() > 10) {
			date = LocalDate.of(smsDate.getYear() - 1, date.getMonth(), date.getDayOfMonth());
		} else if (smsDate.getMonthValue() > 10 && date.getMonthValue() < 3) {
			date = LocalDate.of(smsDate.getYear() + 1, date.getMonth(), date.getDayOfMonth());
		} else {
			date = LocalDate.of(smsDate.getYear(), date.getMonth(), date.getDayOfMonth());
		}
		return LocalDateTime.of(date, time);
	}

	public static LocalDateTime dateTimeFrom(Calendar calendar) {
		Objects.requireNonNull(calendar);
		return dateTimeFrom(calendar.getTime());
	}

	public static LocalDate dateFrom(Calendar calendar) {
		Objects.requireNonNull(calendar);
		return dateFrom(calendar.getTime());
	}

	public static LocalDateTime dateTimeFrom(Date date) {
		Objects.requireNonNull(date);
		return LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneId.systemDefault());
	}

	public static LocalDate dateFrom(Date date) {
		Objects.requireNonNull(date);
		return dateTimeFrom(date).toLocalDate();
	}

	public static Date fromLocalDateTime(LocalDateTime time) {
		Objects.requireNonNull(time);
		return Date.from(time.atZone(ZoneId.systemDefault()).toInstant());
	}

	public static Date fromLocalDate(LocalDate date) {
		Objects.requireNonNull(date);
		return Date.from(date.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	public static long secondsInLocalDateTime(LocalDateTime time) {
		Objects.requireNonNull(time);
		return time.atZone(ZoneId.systemDefault()).toEpochSecond();
	}

	public static boolean isSameTillSeconds(LocalDateTime first, LocalDateTime second) {
		Objects.requireNonNull(first);
		Objects.requireNonNull(second);
		return first.format(FORMATTER_DD_MM_YYYY_HH_MM_SS).equals(second.format(FORMATTER_DD_MM_YYYY_HH_MM_SS));
	}

	public static boolean isGreaterTillSeconds(LocalDateTime first, LocalDateTime second) {
		Objects.requireNonNull(first);
		Objects.requireNonNull(second);
		return parseDateTime(first.format(FORMATTER_DD_MM_YYYY_HH_MM_SS), "dd/MM/yyyy HH:mm:ss").isAfter(
				parseDateTime(second.format(FORMATTER_DD_MM_YYYY_HH_MM_SS), "dd/MM/yyyy HH:mm:ss"));
	}
}
