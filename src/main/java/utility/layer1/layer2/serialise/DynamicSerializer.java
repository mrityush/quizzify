package layer1.layer2.serialise;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * 
 * @author mrityunjya.shukla
 *
 * @param <T>
 */
public class DynamicSerializer<T extends JsonSerialisable> extends JsonSerializer<T> {
	@Override
	public void serialize(final T t, JsonGenerator g, SerializerProvider serializers) throws IOException {
		g.writeStartObject();
		t.inJsonString(g);
		g.writeEndObject();
	}
}
