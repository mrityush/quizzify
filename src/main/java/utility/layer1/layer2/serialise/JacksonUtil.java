package layer1.layer2.serialise;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.LinkedHashMap;
import java.util.Map;

import org.joda.time.DateTime;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.src.main.comm.request.external.AbstractExternalRequest;
import com.src.main.comm.request.external.ExternalRequest;
import com.src.main.dtos.users.UsersDeviceDTO;
import com.src.main.tlabs.quizzify.db.communications.CommunicationLink;
import com.src.main.tlabs.quizzify.db.communications.ExternalCommunicationLink;
import com.src.main.tlabs.quizzify.db.emails.Email;
import com.src.main.tlabs.quizzify.db.emails.Template;
import com.src.main.tlabs.quizzify.db.questionnaires.AnswerSet;
import com.src.main.tlabs.quizzify.db.questionnaires.Category;
import com.src.main.tlabs.quizzify.db.questionnaires.Question;
import com.src.main.tlabs.quizzify.db.questionnaires.Quiz;
import com.src.main.tlabs.quizzify.db.questionnaires.QuizSet;
import com.src.main.tlabs.quizzify.db.questionnaires.Set;
import com.src.main.tlabs.quizzify.db.rewards.Reward;
import com.src.main.tlabs.quizzify.db.rewards.RewardsStat;
import com.src.main.tlabs.quizzify.db.shipments.Courier;
import com.src.main.tlabs.quizzify.db.shipments.Shipment;
import com.src.main.tlabs.quizzify.db.sponsors.Sponsor;
import com.src.main.tlabs.quizzify.db.sponsors.SponsorsStat;
import com.src.main.tlabs.quizzify.db.users.User;
import com.src.main.tlabs.quizzify.db.users.UserDevice;
import com.src.main.tlabs.quizzify.db.users.UserProfile;
import com.src.main.tlabs.quizzify.db.users.UserQuestionProfile;
import com.src.main.tlabs.quizzify.db.users.UserQuizPerformance;
import com.src.main.tlabs.quizzify.db.users.UserQuizProfile;

import layer1.layer2.comm.request.AbstractRequest;
import layer1.layer2.comm.request.Request;
import layer1.layer2.comm.response.AbstractResponse;
import layer1.layer2.comm.response.Response;
import layer1.layer2.comm.response.ResponseError;
import layer1.layer2.resource.Result;
import net.greghaines.jesque.json.ObjectMapperFactory;

public enum JacksonUtil implements JsonUtil {
	DEFAULT {
		@Override
		protected void addDeserializers(SimpleModule module) {
			addCommonDeserializers(module);
		}

		@Override
		protected void addSerializers(SimpleModule module) {
			addCommonSerializers(module);
		}
	};
	public static void addCommonDeserializers(final SimpleModule module) {

		module.addDeserializer(AnswerSet.class, new AnswerSet.Deserializer());
		module.addDeserializer(Category.class, new Category.Deserializer());
		module.addDeserializer(CommunicationLink.class, new CommunicationLink.Deserializer());
		module.addDeserializer(Courier.class, new Courier.Deserializer());
		module.addDeserializer(DateTime.class, new CustomDateTimeDeserializer());
		module.addDeserializer(Email.class, new Email.Deserializer());
		module.addDeserializer(ExternalCommunicationLink.class,
				new ExternalCommunicationLink.Deserializer());
		module.addDeserializer(ExternalRequest.class, new AbstractExternalRequest.Deserializer());
		module.addDeserializer(LocalDate.class, new CustomDateDeserializer());
		module.addDeserializer(LocalDateTime.class, new CustomLocalDateTimeDeserializer());
		module.addDeserializer(LocalTime.class, new CustomTimeDeserializer());
		module.addDeserializer(Question.class, new Question.Deserializer());
		module.addDeserializer(QuizSet.class, new QuizSet.Deserializer());
		module.addDeserializer(Quiz.class, new Quiz.Deserializer());
		module.addDeserializer(Request.class, new AbstractRequest.Deserializer());
		module.addDeserializer(ResponseError.class, new ResponseError.Deserializer());
		module.addDeserializer(Response.class, new AbstractResponse.Deserializer());
		module.addDeserializer(Result.class, new Result.Deserializer());
		module.addDeserializer(Reward.class, new Reward.Deserializer());
		module.addDeserializer(RewardsStat.class, new RewardsStat.Deserializer());
		module.addDeserializer(Set.class, new Set.Deserializer());
		module.addDeserializer(Shipment.class, new Shipment.Deserializer());
		module.addDeserializer(Sponsor.class, new Sponsor.Deserializer());
		module.addDeserializer(SponsorsStat.class, new SponsorsStat.Deserializer());
		module.addDeserializer(Template.class, new Template.Deserializer());
		module.addDeserializer(User.class, new User.Deserializer());
		module.addDeserializer(UserDevice.class, new UserDevice.Deserializer());
		module.addDeserializer(UsersDeviceDTO.class, new UsersDeviceDTO.Deserializer());
		module.addDeserializer(UserProfile.class, new UserProfile.Deserializer());
		module.addDeserializer(UserQuestionProfile.class, new UserQuestionProfile.Deserializer());
		module.addDeserializer(UserQuizPerformance.class, new UserQuizPerformance.Deserializer());
		module.addDeserializer(UserQuizProfile.class, new UserQuizProfile.Deserializer());
	}

	public static void addCommonSerializers(final SimpleModule module) {
		module.addSerializer(AnswerSet.class, new DynamicSerializer<AnswerSet>());
		module.addSerializer(Category.class, new DynamicSerializer<Category>());
		module.addSerializer(CommunicationLink.class, new DynamicSerializer<CommunicationLink>());
		module.addSerializer(Courier.class, new DynamicSerializer<Courier>());
		module.addSerializer(DateTime.class, new CustomDateTimeSerializer());
		module.addSerializer(Email.class, new DynamicSerializer<Email>());
		module.addSerializer(ExternalCommunicationLink.class,
				new DynamicSerializer<ExternalCommunicationLink>());
		module.addSerializer(ExternalRequest.class, new DynamicSerializer<ExternalRequest>());
		module.addSerializer(LocalDateTime.class, new CustomLocalDateTimeSerializer());
		module.addSerializer(LocalDate.class, new CustomDateSerializer());
		module.addSerializer(LocalTime.class, new CustomTimeSerializer());
		module.addSerializer(Question.class, new DynamicSerializer<Question>());
		module.addSerializer(QuizSet.class, new DynamicSerializer<QuizSet>());
		module.addSerializer(Quiz.class, new DynamicSerializer<Quiz>());
		module.addSerializer(Request.class, new DynamicSerializer<Request>());
		module.addSerializer(ResponseError.class, new DynamicSerializer<ResponseError>());
		module.addSerializer(Response.class, new DynamicSerializer<Response>());
		module.addSerializer(Result.class, new DynamicSerializer<Result>());
		module.addSerializer(Reward.class, new DynamicSerializer<Reward>());
		module.addSerializer(RewardsStat.class, new DynamicSerializer<RewardsStat>());
		module.addSerializer(Set.class, new DynamicSerializer<Set>());
		module.addSerializer(Shipment.class, new DynamicSerializer<Shipment>());
		module.addSerializer(Sponsor.class, new DynamicSerializer<Sponsor>());
		module.addSerializer(SponsorsStat.class, new DynamicSerializer<SponsorsStat>());
		module.addSerializer(Template.class, new DynamicSerializer<Template>());
		module.addSerializer(User.class, new DynamicSerializer<User>());
		module.addSerializer(UserDevice.class, new DynamicSerializer<UserDevice>());
		module.addSerializer(UsersDeviceDTO.class, new DynamicSerializer<UsersDeviceDTO>());
		module.addSerializer(UserProfile.class, new DynamicSerializer<UserProfile>());
		module.addSerializer(UserQuestionProfile.class,
				new DynamicSerializer<UserQuestionProfile>());
		module.addSerializer(UserQuizPerformance.class,
				new DynamicSerializer<UserQuizPerformance>());
		module.addSerializer(UserQuizProfile.class, new DynamicSerializer<UserQuizProfile>());
	}

	private final ObjectMapper mapper;

	private JacksonUtil() {
		mapper = new ObjectMapper();
		setup();
	}

	protected abstract void addDeserializers(final SimpleModule module);

	protected abstract void addSerializers(final SimpleModule module);

	@Override
	public Map<String, String> attributes(String json) throws IOException {
		// TODO: Check if this works when a field has array value
		LinkedHashMap<String, String> attributes = DEFAULT.mapper.readValue(json,
				new TypeReference<LinkedHashMap<String, String>>() {
				});
		return attributes;
	}

	public ObjectMapper mapper() {
		return mapper;
	}

	@Override
	public synchronized void setup() {
		logger.info("Start: Setting up Jackson " + name());
		// mapper.registerModule(new JSR310Module());
		final SimpleModule module = new SimpleModule("ETMoney");
		addSerializers(module);
		addDeserializers(module);
		// mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.enable(JsonGenerator.Feature.ESCAPE_NON_ASCII);
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		mapper.registerModule(module);
		logger.info("End: Setting up Jackson " + name());
		logger.info("Start: Setting up jackson for jesque.");
		ObjectMapperFactory.get().registerModule(module);
		logger.info("End: Setting up jackson for jesque.");
	}
}
