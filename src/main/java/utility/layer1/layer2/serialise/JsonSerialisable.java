package layer1.layer2.serialise;

import java.io.IOException;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;

public interface JsonSerialisable {
	default Optional<String> serializeAsJson() {
		return JsonUtil.toJson(this);
	}

	void inJsonString(JsonGenerator g) throws IOException;

	void populateFromJsonString(String field, JsonParser p) throws IOException;
}
