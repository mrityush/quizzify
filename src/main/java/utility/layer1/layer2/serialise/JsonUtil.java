package layer1.layer2.serialise;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonSerializable;

public interface JsonUtil {
	static final Logger logger = LoggerFactory.getLogger(JsonUtil.class);

	static JsonUtil defaultProvider() {
		return JacksonUtil.DEFAULT;
	}

	Map<String, String> attributes(String json) throws Exception;

	void setup();

	/**
	 * Generate the json serialized string for the passed object.
	 * 
	 * @param o
	 * @return Json representation or empty optional if object is null
	 */
	static Optional<String> toJson(Object o) {
		if (o == null) {
			return Optional.empty();
		}
		try {
			long start = System.currentTimeMillis();
			Optional<String> result = Optional.of(JacksonUtil.DEFAULT.mapper().writeValueAsString(o));
			long end = System.currentTimeMillis();
			if (end - start > 100) {
				logger.warn("{} ms taken to serialize object of class ({}) ", end - start, o.getClass().getSimpleName());
			}
			return result;
		} catch (Exception e) {
			logger.error("Error converting object of class ({}) to JSON", o.getClass().getSimpleName(), e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * 
	 * Usage JsonUtil.<User>fromJson(jsonString,new TypeReference<User>(){})
	 * 
	 * @param json
	 * @return Object represented by this json or empty optional is some error
	 *         occurs.
	 */
	static <T> Optional<T> fromJson(String json, TypeReference<T> reference) {
		if (json == null) {
			return Optional.empty();
		}
		Objects.requireNonNull(reference);
		try {
			T t = JacksonUtil.DEFAULT.mapper().readValue(json, reference);
			return Optional.of(t);
		} catch (Exception e) {
			logger.error("Error converting Json {} to Object", json, e);
		}
		return Optional.empty();
	}

	/**
	 * 
	 * Usage JsonUtil.<User>fromJson(jsonString,new TypeReference<User>(){})
	 * 
	 * @param json
	 * @return Object represented by this json or empty optional is some error
	 *         occurs.
	 */
	static <T> Optional<T> fromJson(String json, Class<T> reference) {
		if (json == null) {
			return Optional.empty();
		}
		Objects.requireNonNull(reference);
		try {
			T t = JacksonUtil.DEFAULT.mapper().readValue(json, reference);
			return Optional.of(t);
		} catch (Exception e) {
			logger.error("Error converting Json {} to Object", json, e);
		}
		return Optional.empty();
	}

	static <T extends JsonSerializable> Optional<T> clone(T t, TypeReference<T> reference) {
		return JsonUtil.<T> fromJson(toJson(t).get(), reference);
	}
}
