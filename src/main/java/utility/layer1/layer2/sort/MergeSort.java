package layer1.layer2.sort;

import java.util.List;

import layer1.layer2.generics.Sort;

@SuppressWarnings("hiding")
public class MergeSort<Integer> implements Sort<Integer> {

	private List<Integer> unsortedList;
	// private List<Integer> tempMergArr;
	private int length;

	public MergeSort(List<Integer> passedList) {
		this.unsortedList = passedList;
		this.length = passedList.size();
	}

	@Override
	public List<Integer> sort() {
		mergeSort(0, length - 1);
		return unsortedList;
	}

	private void mergeSort(int lowerIndex, int higherIndex) {
		if (lowerIndex < higherIndex) {
			int middleIndex = lowerIndex + (higherIndex - lowerIndex) / 2;
			mergeSort(lowerIndex, middleIndex);
			mergeSort(middleIndex + 1, higherIndex);
			mergeParts(lowerIndex, middleIndex, higherIndex);
		}
	}

	private void mergeParts(int lowerIndex, int middleIndex, int higherIndex) {
		// for (int i = lowerIndex; i <= higherIndex; i++) {
		// tempMergArr[i] = array[i];
		// }
		// int i = lowerIndex;
		// int j = middle + 1;
		// int k = lowerIndex;
		// while (i <= middle && j <= higherIndex) {
		// if (tempMergArr[i] <= tempMergArr[j]) {
		// array[k] = tempMergArr[i];
		// i++;
		// } else {
		// array[k] = tempMergArr[j];
		// j++;
		// }
		// k++;
		// }
		// while (i <= middle) {
		// array[k] = tempMergArr[i];
		// k++;
		// i++;
		// }

	}

}
