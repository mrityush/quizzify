package layer1.layer2.sort;

import java.util.List;

import layer1.layer2.generics.Sort;

@SuppressWarnings("hiding")
public class QuickSort<Integer> implements Sort<Integer> {

	List<Integer> unsortedList;
	int length;

	public QuickSort(List<Integer> passedList) {
		this.unsortedList = passedList;
		this.length = passedList.size();
	}

	@Override
	public List<Integer> sort() {
		quickSort(0, length - 1);
		return unsortedList;
	}

	private void quickSort(int lowerIndex, int higherIndex) {
		int i = lowerIndex;
		int j = higherIndex;
		int pivot = (int) unsortedList.get(lowerIndex
				+ (higherIndex - lowerIndex) / 2);
		while (i < j) {
			while ((int) unsortedList.get(i) < pivot) {
				i++;
			}
			while ((int) unsortedList.get(j) > pivot) {
				j--;
			}
			if (i <= j) {
				exchangeNumbers(i, j);
				i++;
				j--;
			}
		}
		if (lowerIndex < j) {
			quickSort(lowerIndex, j);
		}
		if (higherIndex > i) {
			quickSort(i, higherIndex);
		}
		// return unsortedList;
	}

	private void exchangeNumbers(int i, int j) {
		Integer tempNo = unsortedList.get(i);
		unsortedList.add(i, unsortedList.get(j));
		unsortedList.add(j, tempNo);
	}
}
