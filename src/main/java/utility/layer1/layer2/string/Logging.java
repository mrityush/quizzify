package layer1.layer2.string;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * 
 * @author mrityunjya.shukla
 *
 */
public interface Logging {
	static final Marker FATAL = MarkerFactory.getMarker("FATAL");
}
