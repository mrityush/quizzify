-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `animals`
--

DROP TABLE IF EXISTS `animals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `animals` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `answer_sets`
--

DROP TABLE IF EXISTS `answer_sets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer_sets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `quiz_set_id` int(11) NOT NULL,
  `solution_key` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_aht9720i91t8vf0be58r8ef6k` (`quiz_set_id`),
  KEY `idx_solution_key_quiz_set_id` (`solution_key`,`quiz_set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `parent_category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_41g4n0emuvcm3qyf1f6cn43c0` (`category_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `communication_links`
--

DROP TABLE IF EXISTS `communication_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communication_links` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `request` varchar(255) DEFAULT NULL,
  `response` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_dfltxm7r6awajfo4n6co4xukc` (`id`,`request`),
  KEY `comm_links_type_id_idx` (`id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `couriers`
--

DROP TABLE IF EXISTS `couriers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `couriers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `courier_details` varchar(255) DEFAULT NULL,
  `courier_name` varchar(255) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jffnjl3rlqve9bobbb7mcqxof` (`courier_name`,`courier_details`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `template_id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `value_variable` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ftioq64tstbeopodpjwkdixru` (`user_id`,`template_id`,`value_variable`),
  KEY `idx_emails_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `external_communication_links`
--

DROP TABLE IF EXISTS `external_communication_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_communication_links` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attempts` int(11) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `execution_time` tinyblob,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `last_retry_time` tinyblob,
  `max_attempts` int(11) DEFAULT NULL,
  `request` longtext,
  `request_id` varchar(255) NOT NULL,
  `response` longtext,
  `response_text` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_msvwytru25y7tsw7paqkf1st` (`id`,`request_id`),
  KEY `idx_external_communications_request_id` (`request_id`),
  KEY `idx_external_communications_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `merchants`
--

DROP TABLE IF EXISTS `merchants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merchants` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `encrypted_answer` varchar(255) NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `question` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_nv168ew28d2nc8efey0cs2613` (`question`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quiz_sets`
--

DROP TABLE IF EXISTS `quiz_sets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quiz_sets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `q_sequence` varchar(255) DEFAULT NULL,
  `quiz_id` bigint(20) DEFAULT NULL,
  `set_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_mltb3t4269kb3qkkjkkrwynop` (`quiz_id`,`set_id`),
  KEY `idx_quiz_id_set_id` (`quiz_id`,`set_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quizzes`
--

DROP TABLE IF EXISTS `quizzes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quizzes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `launch_time` datetime DEFAULT NULL,
  `question_ids` varchar(255) NOT NULL,
  `quiz_name` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_qjbn4o73c460wu8bcoxswtqkb` (`quiz_name`,`launch_time`,`question_ids`),
  KEY `idx_launch_time_duration_category` (`launch_time`,`duration`,`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reward_stats`
--

DROP TABLE IF EXISTS `reward_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reward_stats` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `quiz_id` bigint(20) DEFAULT NULL,
  `reward_id` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_3eow88saq7dqjvw3db8vfa7x5` (`reward_id`,`quiz_id`,`user_id`),
  KEY `idx_reward_id_quiz_id_user_id` (`reward_id`,`quiz_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rewards`
--

DROP TABLE IF EXISTS `rewards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rewards` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `item` varchar(255) NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `price` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jr270qq6pld9gy0p8rl5spa93` (`item`,`price`),
  KEY `idx_price` (`price`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sets`
--

DROP TABLE IF EXISTS `sets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_raobjah9l9ggye9ks4jfaytma` (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shipments`
--

DROP TABLE IF EXISTS `shipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `courier_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `reward_stat_id` bigint(20) NOT NULL,
  `status` int(11) NOT NULL,
  `user_address_id` int(11) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_mlfvbhxhlagclkgq2hbmlyebg` (`reward_stat_id`,`user_id`),
  KEY `idx_reward_stat_id_user_id` (`reward_stat_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sponsors`
--

DROP TABLE IF EXISTS `sponsors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `location_etc` varchar(255) DEFAULT NULL,
  `sponsor_name` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_mi05r8yf810f87qnfl9r73s9t` (`sponsor_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sponsors_stats`
--

DROP TABLE IF EXISTS `sponsors_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsors_stats` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `quiz_id` bigint(20) DEFAULT NULL,
  `share_amt_tmp` varchar(255) NOT NULL,
  `sponsor_id` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_p0cpa0f8bsvpk2dt46vbe389q` (`sponsor_id`,`quiz_id`),
  KEY `idx_sponsor_id_quiz_id` (`sponsor_id`,`quiz_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `templates`
--

DROP TABLE IF EXISTS `templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `template` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_2ldlr6je2h25vint4h03hqcbq` (`template`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_quiz_performance`
--

DROP TABLE IF EXISTS `user_quiz_performance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_quiz_performance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `answer_stats` varchar(255) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `quiz_id` bigint(20) DEFAULT NULL,
  `score` double DEFAULT NULL,
  `set_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `submit_duration` decimal(19,2) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ggwb04y371cphr28rxt03huu1` (`user_id`,`quiz_id`),
  KEY `idx_user_id_quiz_id` (`user_id`,`quiz_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_quiz_profile`
--

DROP TABLE IF EXISTS `user_quiz_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_quiz_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `item_won` varchar(255) DEFAULT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `percentile` decimal(19,2) DEFAULT NULL,
  `quiz_id` bigint(20) DEFAULT NULL,
  `rank` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_rd41ew1yootj1nr7dpakwwdpr` (`user_id`,`quiz_id`),
  KEY `idx_user_id_quiz_id` (`user_id`,`quiz_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `email` varchar(255) NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_6dotkott2kjsp8vw4d0m25fb7` (`email`),
  KEY `idx_email_status` (`email`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_device`
--

DROP TABLE IF EXISTS `users_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_device` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_version` varchar(255) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `device_imei` varchar(255) DEFAULT NULL,
  `gcm_registration_id` varchar(255) DEFAULT NULL,
  `last_access_time` datetime DEFAULT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `os_type` int(11) DEFAULT NULL,
  `os_version` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jmh821d71lh2pui2iyja458xb` (`user_id`,`device_id`),
  KEY `idx_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_otp`
--

DROP TABLE IF EXISTS `users_otp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_otp` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `otp` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_eh0y3lru2ahovadnj8p0t0jg0` (`user_id`,`otp`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_profile`
--

DROP TABLE IF EXISTS `users_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `quiz_ids` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_gma5dkqp1h4b0f9mj6nygluvo` (`user_id`,`quiz_ids`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_question_profile`
--

DROP TABLE IF EXISTS `users_question_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_question_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `creation_time` datetime NOT NULL,
  `last_modification_time` datetime NOT NULL,
  `last_modified_by` bigint(20) NOT NULL,
  `question_ids` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5ya9l0ygc8l3ciahtjeod6knx` (`user_id`,`question_ids`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-26  7:05:31
